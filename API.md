# API Documentation

Following endpoints are used between the ends.

Notes:

-   When an endpoint is not public, this means it shall provide its token, in
    HTTP `uthorization` header, as follows: `Bearer {token}`.
-   `STR_DATETIME` means [ISO8601] format, i.e. something like
    `2020-11-24T08:25:00+01:00`. Timezone setting is not required, but greatly
    encouraged!
-   `STR_TIMEDELTA` expresses a duration, as follows: `HH:MM:SS`, with leading
    zeroes if necessary. Example: `02:30:00`.

[iso8601]: https://en.wikipedia.org/wiki/ISO_8601#Time_offsets_from_UTC

## Application endpoints

| Endpoint      | Method | Parameters | Public | Result structure    | Comment                  |
| ------------- | :----: | ---------- | :----: | ------------------- | ------------------------ |
| `/api/status` | `GET`  | none       |  yes   | `{"healthy": BOOL}` | Provides back-end status |

## Users endpoints

### Registration, Login, Logout

| Endpoint              | Method | Parameters                        | Public | Result structure                                                          | Comment                      |
| --------------------- | :----: | --------------------------------- | :----: | ------------------------------------------------------------------------- | ---------------------------- |
| `/api/users/register` | `POST` | `{"email": STR, "password": STR}` |  yes   | `{"message": STR, "user": {"user_id": INT, "link": URL}, "token": Token}` | To create a new user         |
| `/api/users/login`    | `POST` | `{"email": STR, "password": STR}` |  yes   | `{"message": STR, "user": {"user_id": INT, "link": URL}, "token": Token}` | To retrieve a new token      |
| `/api/users/logout`   | `POST` | none                              |   no   | `{"message": STR}`                                                        | Blacklist the provided Token |

### Users listing and details

| Endpoint                   | Method | Parameters | Public | Result structure                                                              | Comment                         |
| -------------------------- | :----: | ---------- | :----: | ----------------------------------------------------------------------------- | ------------------------------- |
| `/api/users/{int:user_id}` | `GET`  | none       |  yes   | `{"message": STR, "user": {"user_id": INT, "email": STR}}`                    | To show user public details     |
| `/api/users/me`            | `GET`  | none       |   no   | `{"message": STR, "user": {"user_id": INT, "email": STR}}`                    | To show user private details    |
| `/api/users/all`           | `GET`  | none       |   no   | `{"message": STR, "users": [ {"user_id": INT, "email": STR, "link": URL} ] }` | Retrieve list of existing users |

## Groups endpoints

### Creation, Update, Deletion

| Endpoint                     |  Method  | Parameters                         | Public | Result structure                                            | Comment                                                                            |
| ---------------------------- | :------: | ---------------------------------- | :----: | ----------------------------------------------------------- | ---------------------------------------------------------------------------------- |
| `/api/groups/new`            |  `POST`  | `{"name": STR, "is_public": BOOL}` |   no   | `{"message": STR, "group": {"group_id": INT, "link": STR}}` | Create a new group, using the authenticated user as creator and first coordinator. |
| `/api/groups/{int:group_id}` |  `PUT`   | `{"name": STR, "is_public": BOOL}` |   no   | `{"message": STR}`                                          | Update the group for the given fields; Token bearer shall be a coordinator.        |
| `/api/groups/{int:group_id}` | `DELETE` | none                               |   no   | `{"message": STR}`                                          | Delete the group; Token bearer shall be a coordinator.                             |

### Groups listing and details

| Endpoint                     | Method | Parameters | Public | Result structure                                                                                                                                                                                                                                                                   | Comment                                                                                                                                                                                                   |
| ---------------------------- | :----: | ---------- | :----: | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/api/groups/{int:group_id}` | `GET`  | none       | yes/no | Public: `{"message": STR, "group": {"name": STR, "is_public": BOOL, "members_nb": INT, "coordinator_nb": INT}}`<br>With token: `{"message": STR, "group": {"name": STR, "is_public": BOOL, "is_member": BOOL", "is_coordinator": BOOL, "members_nb": INT, "coordinator_nb": INT}}` | Return overall information from a group, including indication whether authenticated user is a member, and whether they is a coordinator.<br>If the group is not public, it is not visible to non-members. |
| `/api/groups/all`            | `GET`  | none       | yes/no | `{"message": STR, "groups": [{"group_id": INT, "name": STR, "link": URL, "is_public": BOOL}]}`                                                                                                                                                                                     | Return the list of all visible groups, including those visible by authenticated user (if any).                                                                                                            |
| `/api/groups/mine`           | `GET`  | none       |   no   | `{"message": STR, "groups": [{"group_id": INT, "name": STR, "link": URL, "is_public": BOOL}]}`                                                                                                                                                                                     | Return the list of groups for which authenticated user is a member.                                                                                                                                       |

### Memberships, Coordination

| Endpoint                                           |  Method  | Parameters         | Public | Result structure                                                                        | Comment                                                                                                                                                                                |
| -------------------------------------------------- | :------: | ------------------ | :----: | --------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/api/groups/{int:group_id}/members`               |  `GET`   | none               |   no   | `{"message": STR, "members": [{"user_id": INT, "link": STR, "is_coordinator": BOOL}],}` | Retrieve group members, and indicates which are coordinators; visible only by group members.                                                                                           |
| `/api/groups/{int:group_id}/members`               |  `POST`  | `{"user_id": INT}` |   no   | `{"message": STR}`                                                                      | Set user identified by `user_id` as a member of the group identified by `group_id`.{br}The authenticated user shall be a coordinator of the group.                                     |
| `/api/groups/{int:group_id}/members/{int:user_id}` | `DELETE` | none               |   no   | `{"message": STR}`                                                                      | Set user identified by `user_id` as a member of the group identified by `group_id`.{br}The authenticated user shall be this user, or a coordinator of the group.                       |
| `/api/groups/{int:group_id}/coordinators`          |  `GET`   | none               |   no   | `{"message": STR, "coordinators": [{"user_id": INT, "link": STR}]}`                     | Retrieve group coordinators; visible only by group members.                                                                                                                            |
| `/api/groups/{int:group_id}/coordinators`          |  `POST`  | `{"user_id": INT}` |   no   | `{"message": STR}`                                                                      | Set user identified by `user_id` as a coordinator of the group identified by `group_id` (and as a member if necessary).{br}The authenticated user shall be a coordinator of the group. |

### Linked Events

| Endpoint                            | Method | Parameters | Public | Result structure                                                  | Comment                                                                                               |
| ----------------------------------- | :----: | ---------- | :----: | ----------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------- |
| `/api/groups/{int:group_id}/events` | `GET`  | none       | yes/no | `{"message": STR, "events": [ {"event_id": INT, "link": URL} ] }` | Return list of events in the group.<br>If the group is private, token bearer shall be a group member. |

## Events endpoints

### Creation, update, deletion

| Endpoint                           |  Method  | Parameters                                                                                                            | Public | Result structure                                            | Comment                                                                                                                                                       |
| ---------------------------------- | :------: | --------------------------------------------------------------------------------------------------------------------- | :----: | ----------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/api/events/new`                  |  `POST`  | `{"title": STR, "group_id": INT, "start": STR_DATETIME, "duration": STR_TIMEDELTA, "is_public": BOOL, "status": STR}` |   no   | `{"message": STR, "event": {"event_id": INT, "link": URL}}` | Create a new event. `is_public` and `status` may be omitted, and defaults to `is_public: True` and `status: active`.<br>Token bearer shall be a group member. |
| `/api/events/{int:event_id}`       |  `PUT`   | `{"title": STR, "start": STR_DATETIME, "duration": STR_TIMEDELTA, "is_public": BOOL, "status": STR}`                  |   no   | `{"message": STR}`                                          | Update the event. All data shall be provided; token bearer shall be a group member.                                                                           |
| `/api/events/{int:event_id}/group` |  `PUT`   | `{"group_id": INT}`                                                                                                   |   no   | `{"message": STR}`                                          | Update the event group; Token bearer shall be a member of both origin and destination groups.                                                                 |
| `/api/events/{int:event_id}`       | `DELETE` | none                                                                                                                  |   no   | `{"message": STR}`                                          | Delete the event; Token bearer shall be a group coordinator.                                                                                                  |

### Events listing and details

| Endpoint                     | Method | Parameters | Public | Result structure                                                                                                                                                                                                      | Comment                                                                                                                      |
| ---------------------------- | :----: | ---------- | :----: | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| `/api/events/{int:event_id}` | `GET`  | none       | yes/no | `{"message": STR, "event": { "event_id": INT, "title": STR, "creator_id": INT, "creator_link": URL, "group_id": INT, "group_link": URL, "is_public": BOOL, "start_date": DATE, "start_time": TIME, "duration": TIME}` | Return the details of identified event.<br>If the event is private or in a private group, caller shall be a group member.    |
| `/api/events/all`            | `GET`  | none       | yes/no | `{"message": STR, [ {"event_id": INT, "link": URL, "group_id": INT, "group_link": URL, "is_public": BOOL, "start_date": DATE, "start_time": TIME, "duration": TIME} ] }`                                              | Return the list of all visible events. If a user is authenticated, this contains the events in their private groups as well. |

### Participation management

| Endpoint                                     | Method | Parameters | Public | Result structure                                                          | Comment                                                                                                                                                                                                                                               |
| -------------------------------------------- | :----: | ---------- | :----: | ------------------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `/api/events/{int:event_id}/{participation}` | `GET`  | none       | yes/no | `{"message": STR, "{participation}": [ {"user_id": INT, "link": URL} ] }` | Return list of members participating as `{participation}` for the event.<br>`participation` can be either `volunteers`, `back-ups`, `absents` or `no-answers`.<br>Token bearer shall be a group member if the event or its group is not public.       |
| `/api/events/{int:event_id}/{participation}` | `POST` | none       |   no   | optional: `{"message": STR, "user_id": INT}`                              | Set user identified by `user_id` participation to `participation`, being either `volunteers`, `back-ups`, `absents` or `no-answers`. If no payload is provided, token bearer is considered.<br>Token bearer shall be a group member or a coordinator. |
