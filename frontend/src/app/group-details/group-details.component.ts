import { Component, Input, OnInit } from '@angular/core';
import { GroupDetails } from '../domain';

@Component({
    selector: 'app-group-details',
    templateUrl: './group-details.component.html',
    styleUrls: ['./group-details.component.scss'],
})
export class GroupDetailsComponent implements OnInit {
    @Input() group: GroupDetails;

    public membersCountMapping: { [k: string]: string } = {
        '=0': 'pas de membre',
        '=1': 'un membre',
        other: '# membres',
    };

    public coordinatorsCountMapping: { [k: string]: string } = {
        '=0': 'pas de coordinateur',
        '=1': 'un coordinateur',
        other: '# coordinateurs',
    };

    constructor() {}

    ngOnInit(): void {}
}
