import { GroupDetails, Group, User } from './domain';

// requests
export interface Credentials {
    email: string;
    password: string;
}

// responses
export interface MessageResponse {
    message: string;
}

export interface BackendStatusResponse {
    healthy: boolean;
}

export interface RegisterResponse {
    token: string;
    message: string;
    user: { user_id: number; link: string };
}
export interface UserDetailsResponse {
    message: string;
    user: User;
}
export interface GroupDetailsResponse {
    message: string;
    group: GroupDetails;
}
export interface GroupsResponse {
    message: string;
    groups: Group[];
}
export interface MembersResponse {
    message: string;
    members: User[];
}
export interface CoordinatorsResponse {
    message: string;
    coordinators: User[];
}
