import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { AuthService } from '../auth.service';
import { Group } from '../domain';
import { GroupsService } from '../groups.service';

@Component({
    selector: 'app-group-list',
    templateUrl: './group-list.component.html',
    styleUrls: ['./group-list.component.scss'],
})
export class GroupListComponent implements OnInit {
    public groups$: Observable<Group[]>;

    constructor(public auth: AuthService, private groups: GroupsService) {}

    ngOnInit(): void {
        this.groups$ = this.groups.getGroups();
    }
}
