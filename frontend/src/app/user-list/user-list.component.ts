import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from '../domain';
import { UsersService } from '../users.service';

@Component({
    selector: 'app-user-list',
    templateUrl: './user-list.component.html',
    styleUrls: ['./user-list.component.scss'],
})
export class UserListComponent implements OnInit {
    public users$: Observable<User[]>;

    constructor(private userservice: UsersService) {}

    ngOnInit(): void {
        this.users$ = this.userservice.getUsers();
    }
}
