import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';

import { AppRoutingModule } from './app-routing.module';
import { httpInterceptorProviders } from './http-interceptors';

import { AppComponent } from './app.component';
import { BackendMonitorComponent } from './backend-monitor/backend-monitor.component';
import { UserPanelComponent } from './user-panel/user-panel.component';
import { UserLogInComponent } from './user-log-in/user-log-in.component';
import { UserRegisterComponent } from './user-register/user-register.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AboutComponent } from './about/about.component';
import { GroupComponent } from './group/group.component';

import { GroupFormComponent } from './group-form/group-form.component';
import { GroupListComponent } from './group-list/group-list.component';
import { EventListComponent } from './event-list/event-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { GroupMembershipComponent } from './group-membership/group-membership.component';
import { GroupDetailsComponent } from './group-details/group-details.component';
import { GroupCoordinatorsComponent } from './group-coordinators/group-coordinators.component';

const routes: Routes = [
    {
        path: '',
        component: HomeComponent,
    },
    { path: 'login', component: UserLogInComponent },
    { path: 'register', component: UserRegisterComponent },
    { path: 'users', component: UserListComponent },
    { path: 'events', component: EventListComponent },
    {
        path: 'groups',
        children: [
            { path: '', component: GroupListComponent },
            {
                path: ':group_id',
                children: [
                    { path: '', component: GroupComponent },
                    {
                        path: 'coordinators',
                        component: GroupCoordinatorsComponent,
                    },
                    { path: 'members', component: GroupMembershipComponent },
                    { path: ':mode', component: GroupComponent },
                ],
            },
        ],
    },
    { path: 'about', component: AboutComponent },
    { path: '**', component: PageNotFoundComponent },
];

@NgModule({
    declarations: [
        AppComponent,
        BackendMonitorComponent,
        UserPanelComponent,
        UserLogInComponent,
        UserRegisterComponent,
        FooterComponent,
        HomeComponent,
        NavBarComponent,
        PageNotFoundComponent,
        AboutComponent,
        GroupComponent,
        GroupFormComponent,
        GroupListComponent,
        EventListComponent,
        UserListComponent,
        GroupMembershipComponent,
        GroupDetailsComponent,
        GroupCoordinatorsComponent,
    ],
    imports: [
        AppRoutingModule,
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterModule.forRoot(routes),
    ],
    providers: [httpInterceptorProviders],
    bootstrap: [AppComponent],
    schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class AppModule {}
