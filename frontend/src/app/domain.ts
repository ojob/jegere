export interface BackendStatus {
    updated: boolean;
    connected: boolean;
    healthy: boolean;
}

export interface Group {
    group_id?: number;
    name: string;
    members_nb?: number;
}
export interface GroupDetails {
    group_id: number;
    name: string;
    members_nb?: number;
    coordinators_nb: number;
    is_public?: boolean;
    is_member?: boolean;
    is_coordinator?: boolean;
}
export const VOID_GROUP: GroupDetails = {
    group_id: 0,
    name: '',
    members_nb: 0,
    coordinators_nb: 0,
    is_public: true,
    is_member: false,
    is_coordinator: false,
};

export interface User {
    user_id: number;
    email: string;
    is_coordinator?: boolean;
}

export const VOID_USER: User = {
    user_id: 0,
    email: '',
};
