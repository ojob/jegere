import { Component, OnDestroy, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

import { AuthService } from '../auth.service';
import { Credentials } from '../api';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-user-log-in',
    templateUrl: './user-log-in.component.html',
    styleUrls: ['./user-log-in.component.scss'],
})
export class UserLogInComponent implements OnInit, OnDestroy {
    logInForm: any;
    message: string;
    private subs: Subscription[] = [];

    constructor(
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private router: Router
    ) {
        this.logInForm = this.formBuilder.group({
            email: '',
            password: '',
        });
    }

    ngOnInit(): void {}

    onLogIn() {
        const credentials: Credentials = this.logInForm.value;
        this.logInForm.reset();
        this.subs.push(
            this.auth.logUserIn(credentials).subscribe({
                next: (connected: boolean) => {
                    if (connected) this.router.navigate(['']);
                },
                error: (error) => {
                    this.message = error.error.message;
                },
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }
}
