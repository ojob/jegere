import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { pluck } from 'rxjs/operators';
import { backendUrl } from './backend.service';
import { User } from './domain';

interface UsersResponse {
    message: string;
    users: User[];
}

@Injectable({
    providedIn: 'root',
})
export class UsersService {
    constructor(private http: HttpClient) {}

    public getUsers(): Observable<User[]> {
        return this.http
            .get<UsersResponse>(`${backendUrl}/users/all`)
            .pipe(pluck('users'));
    }
}
