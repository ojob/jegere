import { Injectable } from '@angular/core';
import {
    HttpEvent,
    HttpInterceptor,
    HttpHandler,
    HttpRequest,
    HttpErrorResponse,
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(private auth: AuthService, private router: Router) {}

    private handleAuthError(err: HttpErrorResponse): Observable<any> {
        // TODO: err.status can be checked against 401 or 403, and trigger redirect
        return throwError(err);
    }

    intercept(
        req: HttpRequest<any>,
        next: HttpHandler
    ): Observable<HttpEvent<any>> {
        // send authenticated request, if there is a user token
        if (this.auth.userToken) {
            const authReq = this.addToken(req, this.auth.userToken);
            return next.handle(authReq).pipe(
                catchError((err) => {
                    return this.handleAuthError(err);
                })
            );
        } else {
            return next.handle(req);
        }
    }

    private addToken(req: HttpRequest<any>, token: string): HttpRequest<any> {
        return req.clone({
            setHeaders: { Authorization: `Bearer ${token}` },
        });
    }
}
