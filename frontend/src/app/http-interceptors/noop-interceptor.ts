import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest,
} from '@angular/common/http';

import { Observable } from 'rxjs';

/* forward the request, untouched, to next handler. */
@Injectable()
export class NoopInterceptor implements HttpInterceptor {

  intercept(
      req: HttpRequest<any>, next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    // just pass the request to next interceptor
    return next.handle(req);
  }

}
