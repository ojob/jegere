import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { distinct, map, pluck, switchMap, tap } from 'rxjs/operators';
import { backendUrl } from './backend.service';
import { VOID_GROUP, GroupDetails, Group, User } from './domain';
import {
    GroupsResponse,
    MembersResponse,
    CoordinatorsResponse,
    MessageResponse,
    GroupDetailsResponse,
} from './api';

@Injectable({
    providedIn: 'root',
})
export class GroupsService {
    currentGroupId$ = new BehaviorSubject<number | string | null>(-1);

    currentGroup$: Observable<GroupDetails> = this.currentGroupId$.pipe(
        tap((group_id) => console.log(`group id updated to ${group_id}`)),
        distinct(),
        switchMap((group_id) => this.getGroup(group_id))
    );

    constructor(private http: HttpClient) {}

    set(group_id: string | number | null): void {
        this.currentGroupId$.next(group_id);
    }

    getGroups(): Observable<Group[]> {
        return this.http
            .get<GroupsResponse>(`${backendUrl}/groups/all`)
            .pipe(pluck('groups'));
    }

    private getGroup(
        group_id: string | number | null
    ): Observable<GroupDetails> {
        return group_id !== null && group_id >= 0
            ? this.http
                  .get<GroupDetailsResponse>(`${backendUrl}/groups/${group_id}`)
                  .pipe(map((resp) => resp.group))
            : of({ ...VOID_GROUP });
    }

    getMembers(): Observable<User[]> {
        return this.http
            .get<MembersResponse>(
                `${backendUrl}/groups/${this.currentGroupId$.getValue()}/members`
            )
            .pipe(pluck('members'));
    }

    getCoordinators(): Observable<User[]> {
        return this.http
            .get<CoordinatorsResponse>(
                `${backendUrl}/groups/${this.currentGroupId$.getValue()}/coordinators`
            )
            .pipe(pluck('coordinators'));
    }

    addMember(user: User, group: GroupDetails): Observable<string> {
        console.log(
            `adding user ${user.email} (id:${user.user_id}) as member of group ${group.name} (id:{group.group_id})`
        );
        return this.http
            .post<MessageResponse>(
                `${backendUrl}/groups/${group.group_id}/members`,
                { user_id: user.user_id }
            )
            .pipe(pluck('message'));
    }

    addCoordinator(user: User, group: GroupDetails): Observable<string> {
        console.log(
            `adding user ${user.email} (id:${user.user_id}) as coordinator of group ${group.name} (id:{group.group_id})`
        );
        return this.http
            .post<MessageResponse>(
                `${backendUrl}/groups/${group.group_id}/coordinators`,
                { user_id: user.user_id }
            )
            .pipe(pluck('message'));
    }

    removeMember(user: User, group: GroupDetails): Observable<string> {
        console.log(
            `removing user ${user.email} (id:${user.user_id}) as member of group ${group.name} (id:{group.group_id})`
        );
        return this.http
            .delete<MessageResponse>(
                `${backendUrl}/groups/${group.group_id}/members/${user.user_id}`
            )
            .pipe(pluck('message'));
    }

    removeCoordinator(user: User, group: GroupDetails): Observable<string> {
        console.log(
            `removing user ${user.email} (id:${user.user_id}) as coordinator of group ${group.name} (id:{group.group_id})`
        );
        return this.http
            .delete<MessageResponse>(
                `${backendUrl}/groups/${group.group_id}/coordinators/${user.user_id}`
            )
            .pipe(pluck('message'));
    }

    store(group: GroupDetails): Observable<number> {
        return this.http
            .post<GroupDetailsResponse>(`${backendUrl}/groups/new`, group)
            .pipe(
                tap((resp) => {
                    console.log(resp.message);
                }),
                pluck('group'),
                map((group) => {
                    let group_id = Number(group.group_id);
                    console.log(
                        `new group stored: ${group.name} (id: ${group_id})`
                    );
                    return group_id;
                })
            );
    }

    update(group: GroupDetails): Observable<string> {
        return this.http
            .put<MessageResponse>(
                `${backendUrl}/groups/${group.group_id}`,
                group
            )
            .pipe(
                pluck('message'),
                tap((message) => {
                    console.log(message);
                })
            );
    }

    delete(group_id: number): Observable<string> {
        return this.http
            .delete<MessageResponse>(`${backendUrl}/groups/${group_id}`)
            .pipe(
                pluck('message'),
                tap((message) => {
                    console.log(message);
                    this.currentGroupId$.next(null);
                })
            );
    }
}
