import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, pluck } from 'rxjs/operators';

import { BackendStatusResponse } from './api';
import { BackendStatus } from './domain';

export const backendUrl = '/api';

@Injectable({
    providedIn: 'root',
})
export class BackendService {
    constructor(private http: HttpClient) {}

    public getStatus(): Observable<BackendStatus> {
        return this.http
            .get<BackendStatusResponse>(`${backendUrl}/status`)
            .pipe(
                pluck('status'),
                map((status: boolean) => {
                    return {
                        updated: true,
                        connected: true,
                        healthy: status,
                    };
                })
            );
    }
}
