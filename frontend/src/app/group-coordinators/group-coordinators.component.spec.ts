import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCoordinatorsComponent } from './group-coordinators.component';

describe('GroupCoordinatorsComponent', () => {
  let component: GroupCoordinatorsComponent;
  let fixture: ComponentFixture<GroupCoordinatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCoordinatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCoordinatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
