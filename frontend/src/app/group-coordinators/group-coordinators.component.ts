import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BehaviorSubject, forkJoin, Observable, of, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { GroupDetails, User } from '../domain';
import { GroupsService } from '../groups.service';

@Component({
    selector: 'app-group-coordinators',
    templateUrl: './group-coordinators.component.html',
    styleUrls: ['./group-coordinators.component.scss'],
})
export class GroupCoordinatorsComponent implements OnInit, OnDestroy {
    coordinators$: Observable<User[]>;
    currentUser$: Observable<User>;
    message$ = new BehaviorSubject<string>('');
    mode: string = 'show';
    nonCoordinators$: Observable<User[]>;
    private subs: Subscription[] = [];

    constructor(
        private auth: AuthService,
        private route: ActivatedRoute,
        public groups: GroupsService
    ) {}

    ngOnInit(): void {
        this.subs.push(
            this.route.paramMap.subscribe((params: ParamMap) =>
                this.handleParams(params)
            )
        );
        this.currentUser$ = this.auth.fetchCurrentUser();
        this.coordinators$ = this.groups.getCoordinators();
        this.nonCoordinators$ = this.nonCoordinators();
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }

    private handleParams(params: ParamMap): void {
        this.groups.set(params.get('group_id'));
    }

    edit(): void {
        this.mode = 'edit';
    }

    endEdit(): void {
        this.mode = 'show';
    }

    nonCoordinators(): Observable<User[]> {
        return forkJoin({
            coordinators: this.groups.getCoordinators(),
            members: this.groups.getMembers(),
        }).pipe(
            map((stuff: { coordinators: User[]; members: User[] }) => {
                let coordinatorsId: number[] = stuff.coordinators.map(
                    (user: User) => user.user_id
                );
                return stuff.members.filter(
                    (user) => !coordinatorsId.includes(user.user_id)
                );
            })
        );
    }

    add(user: User, group: GroupDetails): void {
        this.subs.push(
            this.groups.addCoordinator(user, group).subscribe({
                next: (message) => {
                    this.message$.next(message);
                    this.ngOnInit();
                },
            })
        );
    }

    remove(user: User, group: GroupDetails): void {
        this.subs.push(
            this.groups.removeCoordinator(user, group).subscribe({
                next: (message) => {
                    this.message$.next(message);
                    this.ngOnInit();
                },
            })
        );
    }
}
