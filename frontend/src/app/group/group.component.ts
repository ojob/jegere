import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { BehaviorSubject, Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { backendUrl } from '../backend.service';

import { GroupDetails, User, VOID_GROUP } from '../domain';
import { GroupsService } from '../groups.service';

@Component({
    selector: 'app-groups',
    templateUrl: './group.component.html',
    styleUrls: ['./group.component.scss'],
})
export class GroupComponent implements OnInit {
    currentUser$: Observable<User>;
    error: string | null = null;
    group$: Observable<GroupDetails>;
    groupId$: BehaviorSubject<string | number | null>;
    mayQuit$: Observable<boolean>;
    mode: string = 'show'; // new, edit, show
    private subs: Subscription[] = [];

    message$ = new BehaviorSubject<string>('');

    constructor(
        public auth: AuthService,
        public groups: GroupsService,
        private http: HttpClient,
        private route: ActivatedRoute,
        private router: Router
    ) {
        this.group$ = this.groups.currentGroup$;
        this.groupId$ = this.groups.currentGroupId$;
    }

    ngOnInit(): void {
        this.subs.push(
            this.route.paramMap.subscribe((params: ParamMap) =>
                this.handleParams(params)
            )
        );
        this.currentUser$ = this.auth.fetchCurrentUser();
        this.mayQuit$ = this.mayQuit();
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }

    private handleParams(params: ParamMap): void {
        let param0 = params.get('group_id');
        if (param0 === 'new') {
            this.groups.set(null);
            this.setMode(param0);
        } else {
            this.groups.set(param0);
            this.setMode(params.get('mode'));
        }
    }

    private setMode(mode: string | null): void {
        if (mode === null) {
            this.mode = 'show';
        } else {
            this.mode = mode;
        }
    }

    edit(group_id: number): void {
        this.router.navigate(['/groups', group_id, 'edit']);
    }

    cancelEdit(group_id: number | string | null): void {
        group_id !== null
            ? this.router.navigate(['/groups', group_id])
            : this.router.navigate(['/groups']);
    }

    store(group: GroupDetails): void {
        this.groups.store(group).subscribe({
            next: (group_id: number) => {
                this.mode = 'show';
                this.router.navigate(['/groups', group_id]);
            },
        });
    }

    update(group: GroupDetails): void {
        this.groups.update(group).subscribe({
            next: () => {
                this.mode = 'show';
                this.router.navigate(['/groups', group.group_id]);
            },
            error: (err) => {
                this.message$.next(err.message);
            },
        });
    }

    remove(group_id: number): void {
        this.groups.delete(group_id).subscribe({
            next: () => {
                this.router.navigate(['/groups']);
            },
        });
    }

    mayQuit(): Observable<boolean> {
        return this.group$.pipe(
            map(
                (group: GroupDetails) =>
                    !!group.is_member &&
                    (group.is_coordinator ? group.coordinators_nb > 1 : true)
            )
        );
    }

    quit(user: User, group: GroupDetails): void {
        console.log(
            `user ${user.user_id} requested to leave group ${group.group_id}`
        );
        this.subs.push(
            this.http
                .delete<{ message: string }>(
                    `${backendUrl}/groups/${group.group_id}/members/${user.user_id}`
                )
                .subscribe({
                    next: (resp) => {
                        this.router.navigate(['/groups']);
                    },
                })
        );
    }
}
