import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from '../auth.service';
import { Credentials } from '../api';

@Component({
    selector: 'app-user-register',
    templateUrl: './user-register.component.html',
    styleUrls: ['./user-register.component.scss'],
})
export class UserRegisterComponent implements OnInit, OnDestroy {
    registerForm: any;
    message: string;
    private subs: Subscription[] = [];

    constructor(
        public auth: AuthService,
        private formBuilder: FormBuilder,
        private router: Router
    ) {
        this.registerForm = this.formBuilder.group({
            email: '',
            password: '',
        });
    }

    ngOnInit(): void {}

    onRegister() {
        const credentials: Credentials = this.registerForm.value;
        this.subs.push(
            this.auth.registerUser(credentials).subscribe({
                next: (connected: boolean) => {
                    this.registerForm.reset();
                    if (connected) this.router.navigate(['']);
                },
                error: (error) => {
                    this.message = error.error.message;
                },
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }
}
