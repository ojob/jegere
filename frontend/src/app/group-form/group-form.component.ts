import { Component, Input, OnInit } from '@angular/core';
import { GroupDetails } from '../domain';

@Component({
    selector: 'app-group-form',
    templateUrl: './group-form.component.html',
    styleUrls: ['./group-form.component.scss'],
})
export class GroupFormComponent implements OnInit {
    @Input() group: GroupDetails;

    constructor() {}

    ngOnInit(): void {}
}
