import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

import { AuthService } from '../auth.service';
import { User } from '../domain';

@Component({
    selector: 'app-user-panel',
    templateUrl: './user-panel.component.html',
    styleUrls: ['./user-panel.component.scss'],
})
export class UserPanelComponent implements OnInit, OnDestroy {
    currentUser$: Observable<User>;
    private subs: Subscription[] = [];

    constructor(public auth: AuthService, private router: Router) {}

    ngOnInit(): void {
        // trigger refresh from local storage once the component is ready
        this.auth.initializeFromStorage();
        this.currentUser$ = this.auth.fetchCurrentUser();
    }

    onLogOut() {
        this.subs.push(
            this.auth.logUserOut().subscribe({
                next: (disconnected) => {
                    if (disconnected) {
                        this.router.navigate(['']);
                    }
                },
                error: (error) => {},
            })
        );
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }
}
