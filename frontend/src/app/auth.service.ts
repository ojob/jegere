import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

import { HttpClient } from '@angular/common/http';

import { backendUrl } from './backend.service';
import { catchError, map, pluck, tap, timeout } from 'rxjs/operators';
import { User } from './domain';
import {
    Credentials,
    RegisterResponse,
    UserDetailsResponse,
    MessageResponse,
} from './api';

const DEFAULT_MESSAGE = '';

const timeoutDuration = 700; // milliseconds

@Injectable({
    providedIn: 'root',
})
export class AuthService {
    isUserConnected$ = new BehaviorSubject<boolean>(false);
    userToken: string | null;
    message$ = new BehaviorSubject<string>(DEFAULT_MESSAGE);

    constructor(private http: HttpClient) {}

    initializeFromStorage(): void {
        this.retrieveUserTokenFromStorage();
        if (this.userToken) {
            // FIXME: the token shall first be tested: it may well be blacklisted,
            // if copied from a previous session, or the user id may have been lost
            // by a backend update.
            this.isUserConnected$.next(true);
        } else {
            this.isUserConnected$.next(false);
        }
    }

    registerUser(credentials: Credentials): Observable<boolean> {
        return this.http
            .post<RegisterResponse>(`${backendUrl}/users/register`, credentials)
            .pipe(
                timeout(timeoutDuration),
                tap((resp) => {
                    console.log(resp.message);
                }),
                map((resp) => {
                    this.storeUserToken(resp.token);
                    this.message$.next('Nouveau compte créé');
                    this.isUserConnected$.next(true);
                    this.message$.next('Nouveau compte créé');
                    return true;
                })
            );
    }

    logUserIn(credentials: Credentials): Observable<boolean> {
        return this.http
            .post<RegisterResponse>(`${backendUrl}/users/login`, credentials)
            .pipe(
                timeout(timeoutDuration),
                tap((resp) => {
                    console.log(resp.message);
                }),
                map((resp) => {
                    this.storeUserToken(resp.token);
                    this.message$.next('Nouveau compte créé');
                    this.isUserConnected$.next(true);
                    return true;
                })
            );
    }

    logUserOut(): Observable<boolean> {
        return this.http
            .post<MessageResponse>(`${backendUrl}/users/logout`, null)
            .pipe(
                timeout(timeoutDuration),
                tap((resp) => {
                    console.log(resp.message);
                }),
                map(() => {
                    this.message$.next('Déconnecté');
                    this.removeUserToken();
                    this.isUserConnected$.next(false);
                    return true;
                })
            );
    }

    fetchCurrentUser(): Observable<User> {
        return this.http
            .get<UserDetailsResponse>(`${backendUrl}/users/me`)
            .pipe(
                timeout(timeoutDuration),
                tap((resp) => {
                    console.log(resp.message);
                }),
                pluck('user'),
                catchError((error) => {
                    let message: string;
                    if (error.error !== undefined) {
                        message = error.error.message;
                    } else {
                        message = error.message;
                    }
                    this.isUserConnected$.next(false);
                    if (message.includes('no user with user_id=')) {
                        this.removeUserToken();
                        message += ', removed stored token';
                    }
                    throw `Failed to get user details: ${message}`;
                })
            );
    }

    // methods to parse HTTP responses

    // error message handling
    private extractErrorMessage(error: any): string {
        let msg = '';
        console.log(error);
        if (error.name === 'TimeoutError') {
            msg = 'pas de réponse du serveur';
        } else if (error.error instanceof ErrorEvent) {
            // error in Angular app
            msg = error.message;
        } else {
            // error in discussion with server
            msg = error.error.message;
        }
        return msg;
    }

    // methods to deal with storage and attributes updates
    private retrieveUserTokenFromStorage(): void {
        // use this when reloading the frontend
        let bareToken = localStorage.getItem('UserToken');
        if (bareToken) {
            console.log('retrieved user token from local storage');
            this.userToken = JSON.parse(bareToken);
        } else {
            console.log('no user token found in local storage');
        }
    }

    private storeUserToken(token: string): void {
        this.userToken = token;
        localStorage.setItem('UserToken', JSON.stringify(token));
        console.log(`new token: ${token}`);
    }

    private removeUserToken(): void {
        this.userToken = null;
        localStorage.removeItem('UserToken');
        console.log('token removed');
    }
}
