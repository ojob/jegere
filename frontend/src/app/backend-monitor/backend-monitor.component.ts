import { Component, OnInit, OnDestroy } from '@angular/core';
import { timeout } from 'rxjs/operators';

import { BackendStatus } from '../domain';
import { BackendService } from '../backend.service';

const refreshInterval: number = 30 * 1000; // milliseconds
const timeoutDuration: number = 1 * 1000; // milliseconds

const defaultStatusStr: string = 'vérification...';

@Component({
    selector: 'app-backend-monitor',
    templateUrl: './backend-monitor.component.html',
    styleUrls: ['./backend-monitor.component.scss'],
})
export class BackendMonitorComponent implements OnInit {
    public status_str: string;
    private interval: any;

    constructor(public backend: BackendService) {}

    ngOnInit(): void {
        this.updateStatus();
        this.interval = setInterval(() => {
            this.updateStatus();
        }, refreshInterval);
    }

    ngOnDestroy(): void {
        this.interval = null;
    }

    private updateStatus(): void {
        this.backend
            .getStatus()
            .pipe(timeout(timeoutDuration))
            .subscribe(
                (res) => {
                    this.status_str = this.buildStatusStr(res);
                },
                (err) => {
                    this.status_str = defaultStatusStr;
                }
            );
    }

    private buildStatusStr(status: BackendStatus): string {
        if (!status.updated) {
            return defaultStatusStr;
        } else if (!status.connected) {
            return 'déconnecté';
        } else {
            return status.healthy ? 'OK' : 'pas OK';
        }
    }
}
