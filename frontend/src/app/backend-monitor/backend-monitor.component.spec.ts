import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackendMonitorComponent } from './backend-monitor.component';

describe('BackendMonitorComponent', () => {
  let component: BackendMonitorComponent;
  let fixture: ComponentFixture<BackendMonitorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackendMonitorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackendMonitorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
