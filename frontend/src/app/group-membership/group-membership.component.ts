import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { BehaviorSubject, forkJoin, Observable, of, Subscription } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { AuthService } from '../auth.service';
import { GroupDetails, User } from '../domain';
import { GroupsService } from '../groups.service';
import { UsersService } from '../users.service';

@Component({
    selector: 'app-group-membership',
    templateUrl: './group-membership.component.html',
    styleUrls: ['./group-membership.component.scss'],
})
export class GroupMembershipComponent implements OnInit {
    mode: string = 'show';
    currentUser$: Observable<User>;
    members$: Observable<User[]>;
    nonMembers$: Observable<User[]>;
    users$: Observable<User[]>;
    message$ = new BehaviorSubject<string>('');
    private subs: Subscription[] = [];

    constructor(
        public auth: AuthService,
        public groups: GroupsService,
        private route: ActivatedRoute,
        private userservice: UsersService
    ) {}

    ngOnInit(): void {
        this.subs.push(
            this.route.paramMap.subscribe((params: ParamMap) =>
                this.handleParams(params)
            )
        );
        this.currentUser$ = this.auth.fetchCurrentUser();
        this.members$ = this.groups.getMembers();
        this.nonMembers$ = this.nonMembers();
        this.users$ = this.userservice.getUsers();
    }

    ngOnDestroy(): void {
        this.subs.forEach((sub) => sub.unsubscribe());
    }

    private handleParams(params: ParamMap): void {
        this.groups.set(params.get('group_id'));
    }

    edit(): void {
        this.mode = 'edit';
    }

    endEdit(): void {
        this.mode = 'show';
    }

    nonMembers(): Observable<User[]> {
        return forkJoin({
            members: this.groups.getMembers(),
            users: this.userservice.getUsers(),
        }).pipe(
            map((stuff: { members: User[]; users: User[] }) => {
                let membersId: number[] = stuff.members.map(
                    (user: User) => user.user_id
                );
                return stuff.users.filter(
                    (user) => !membersId.includes(user.user_id)
                );
            }),
            tap((nonMembers: User[]) =>
                console.log(
                    `non-members ids: ${nonMembers.map((user) => user.user_id)}`
                )
            )
        );
    }

    add(user: User, group: GroupDetails): void {
        this.subs.push(
            this.groups.addMember(user, group).subscribe({
                next: (message) => {
                    this.message$.next(message);
                    this.ngOnInit();
                },
            })
        );
    }

    remove(user: User, group: GroupDetails): void {
        this.subs.push(
            this.groups.removeMember(user, group).subscribe({
                next: (message) => {
                    this.message$.next(message);
                    this.ngOnInit();
                },
            })
        );
    }
}
