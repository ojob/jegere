import pytest

from jegere.domain import exc, model
from jegere.service_layer import app_events, commands, message_bus as bus
from jegere.service_layer.unit_of_work import UnitOfWork
from jegere.domain.model import UserData

from ..conftest import SampleUow


def test_uow(void_uow: SampleUow, users_dataset: list[model.UserData]):
    uow: UnitOfWork = void_uow.uow
    user_data: UserData = users_dataset[0]
    create_cmd = commands.CreateUser(data=user_data)

    [ev], trace = bus.handle(create_cmd, uow)
    assert [type(event) for event in trace] == \
        [commands.CreateUser, app_events.UserCreated]

    with pytest.raises(exc.EmailAlreadyRegistered):
        bus.handle(create_cmd, uow)

    search_cmd = commands.GetUserPublicDetails(email=user_data.email)
    search_res = bus.handle(search_cmd, uow).results[0]
    assert search_res['user_id'] == ev.user_id


def test_uow_events_cascade(sample_uow):
    # given a repo containing a group with an event
    actor = sample_uow.users[0]
    group = sample_uow.groups[0]
    event = sample_uow.events[0]
    assert event.group is group
    # and a clean trace in the repository
    sample_uow.uow.store.app_events = []

    # when group deletion is requested
    delete_cmd = commands.RemoveGroup(actor_id=actor.id, group_id=group.id)
    results, trace = bus.handle(delete_cmd, sample_uow.uow)

    # then group is not in the repo anymore
    assert sample_uow.uow.store.find_group(group_id=group.id) is None
    # and the expected events are identified in the execution trace
    assert [type(ev) for ev in trace] == \
        [commands.RemoveGroup, app_events.GroupRemoved, app_events.EventRemoved]
    # and related events are not in the repo anymore
    with pytest.raises(exc.EventNotFound):
        sample_uow.uow.store.get_event(event_id=event.id)
