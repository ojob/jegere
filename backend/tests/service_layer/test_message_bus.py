import pytest
from jegere.domain import model, exc
from jegere.service_layer import \
    app_events, commands, handlers, message_bus as bus
from jegere.service_layer.unit_of_work import UnitOfWork

from ..conftest import SampleUow


def test_message_bus_with_multiple_commands(sample_uow):
    # given a sequence of commands
    coord_id = sample_uow.users[0].id
    group = sample_uow.groups[0]
    new_members = sample_uow.users[1:]
    cmds = [commands.AddMember(
                actor_id=coord_id, group_id=group.id, user_id=user.id)
            for user in new_members]

    # when the bus is filled with these commands
    results, trace = bus.handle(cmds, sample_uow.uow)
    # then the expected effect has been performed
    assert all(user in group.members for user in new_members)
    exp_events = [
        app_events.MemberAdded(
            actor_id=coord_id, group=group, user=user,
            msg="user added as member")
        for user in new_members]
    assert all(ev in trace for ev in exp_events)


def test_message_bus_stops_on_command_error(sample_uow):
    # given a sample uow, an actor
    actor = sample_uow.users[0]
    # and a group not stored in the repository
    other_group = model.Group(name='grp0', creator=actor)

    # when removal of a non-existing group is requested
    cmd = commands.RemoveGroup(actor_id=actor.id, group_id=other_group.id)
    # then execution stops with expected exception
    results, trace = [], []  # to ensure existence despite of exception below
    with pytest.raises(exc.GroupNotFound):
        results, trace = bus.handle(cmd, sample_uow.uow)
    assert not results
    assert not trace


def test_message_bus_does_not_stop_on_app_event_error(sample_uow: SampleUow):
    # when one of the events handlers raises an exception
    handlers.EVENTS_HANDLERS[app_events.GroupRemoved].insert(0, failing_handler)

    # and given a sample uow, actor, group
    actor = sample_uow.users[0]
    group = sample_uow.groups[0]
    event = sample_uow.events[0]

    # when removal of a group is requested
    cmd = commands.RemoveGroup(actor_id=actor.id, group_id=group.id)
    _, trace = bus.handle(cmd, sample_uow.uow)

    # first let's not forget to remove the failing handler, in case one of the
    # following checks fails
    del handlers.EVENTS_HANDLERS[app_events.GroupRemoved][0]

    # then group is removed
    assert sample_uow.uow.store.find_group(group_id=group.id) is None
    # and trace does correctly show that linked event was removed
    exp_ev = app_events.EventRemoved(actor_id=actor.id, event=event, msg=None)
    assert exp_ev in trace
    # and trace shows that failed handler message is caught
    exp_trace = app_events.EventHandlerFailure(
        actor_id=actor.id,
        msg=f"app event {trace[1]} handler {failing_handler} failed: failing!")
    assert exp_trace in trace


# ---- helpers ----
def failing_handler(_0: app_events.AppEvent, _1: UnitOfWork) -> None:
    """A function that fails running."""
    raise TypeError('failing!')
