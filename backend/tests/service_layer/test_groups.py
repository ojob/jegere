import pytest

from jegere.domain import exc, model
from jegere.service_layer import commands, message_bus as bus

from ..conftest import SampleUow


def test_group_creation(sample_uow: SampleUow):
    # given a repo containing a user and some group data
    user = sample_uow.users[0]
    group_data = model.GroupData(name='New gathering')

    # when user requests creation of a new group
    create = commands.CreateGroup(actor_id=user.id, data=group_data)
    [ev], _ = bus.handle(create, sample_uow.uow)
    [group], _ = \
        bus.handle(commands.FindGroup(group_id=ev.group_id), sample_uow.uow)

    # then group is created with appropriate characteristics
    assert group.name == group_data.name, "name is the provided one"
    assert user is group.creator, "actor is the group creator"
    assert user in group.members, "creator is part of group members"
    assert user in group.coordinators, "creator is part of group coordinators"
    assert group in user.groups, "created group is part of user groups"

    assert sample_uow.uow.store.find_group(name=group_data.name) is group, \
        "group is stored in the repository"


def test_group_find(sample_uow: SampleUow):
    # given a repo with one group
    group = sample_uow.groups[1]

    # when request is made to find the group by its name
    find = commands.FindGroup(name=group.name)
    [found_group], _ = bus.handle(find, sample_uow.uow)
    # then expected group is returned
    assert found_group == group

    # when request is made with some unexisting name
    wrong_name: str = group.name + '-whaaatever'
    find_wrong = commands.FindGroup(name=wrong_name)
    res: list = bus.handle(find_wrong, sample_uow.uow).results
    # then no group is returned
    assert res == [None]

    # when request is made with no criterion
    find_void = commands.FindGroup()
    # then failure is raised
    with pytest.raises(TypeError):
        bus.handle(find_void, uow=sample_uow.uow)


def test_group_creation_robustness(sample_uow: SampleUow):
    # given a core with one user and one group
    user: model.User = sample_uow.users[0]
    group: model.Group = sample_uow.groups[0]
    group_data: model.GroupData = sample_uow.groups_data[0]

    # when creation of a new group with same name is tried
    cmd_create = commands.CreateGroup(actor_id=user.id, data=group_data)
    # then error is returned
    with pytest.raises(exc.GroupNameAlreadyTaken):
        bus.handle(cmd_create, sample_uow.uow)

    # when trying to access group by another id or name
    # then error is raised
    cmd_get_by_id = commands.GetGroupDetails(group_id=group.id + 19897)
    with pytest.raises(exc.GroupNotFound):
        bus.handle(cmd_get_by_id, sample_uow.uow)
    cmd_get_by_wrong_name = commands.GetGroupDetails(name=group.name + '--fake')
    with pytest.raises(exc.GroupNotFound):
        bus.handle(cmd_get_by_wrong_name, sample_uow.uow)

    # and at least one criterion is necessary returned
    cmd_no_criterion = commands.GetGroupDetails()
    with pytest.raises(TypeError):
        bus.handle(cmd_no_criterion, sample_uow.uow)


def test_group_member_addition(sample_uow: SampleUow):
    # given several users and a group
    group = sample_uow.groups[0]
    coordinator = sample_uow.users[0]
    user_1 = sample_uow.users[1]

    # when a coordinator requests addition of a user as a member
    member_cmd = commands.AddMember(
        actor_id=coordinator.id, group_id=group.id, user_id=user_1.id)
    bus.handle(member_cmd, sample_uow.uow)

    # then user is a group member
    assert user_1 in group.members
    assert group in user_1.groups


def test_groups_privacy_setting(sample_uow: SampleUow):
    # given several users and groups
    group = sample_uow.groups[0]
    coordinator = sample_uow.users[0]

    # when its coordinator requests visibility change
    cmd_visibility = commands.SetGroupVisibility(
        actor_id=coordinator.id, group_id=group.id, public=False)
    bus.handle(cmd_visibility, sample_uow.uow)

    # then unauthorized users cannot see it
    cmd_get_by_unauth = commands.GetGroupDetails(group_id=group.id)
    with pytest.raises(exc.NotAuthorized):
        bus.handle(cmd_get_by_unauth, sample_uow.uow)

    # and other users cannot see it
    cmd_get_by_other = commands.GetGroupDetails(
        actor_id=sample_uow.users[2].id, group_id=group.id)
    with pytest.raises(exc.NotAuthorized):
        bus.handle(cmd_get_by_other, sample_uow.uow)

    # and coordinator can still see it
    cmd_get_by_coord = commands.GetGroupDetails(
        actor_id=coordinator.id, group_id=group.id)
    res_group_details = \
        bus.handle(cmd_get_by_coord, sample_uow.uow).results[0]
    assert res_group_details == group.public_details()

    # and members can still see it
