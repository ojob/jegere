from itertools import combinations

import pytest

from jegere.domain.model import Event, EventData, Group, GroupData,\
    Participation, User, UserData
from jegere.domain import ops


# ---- fixtures ----
@pytest.fixture
def sample_user(user_data: UserData) -> User:
    return ops.create_user(user_data)


@pytest.fixture
def sample_user2(user2_data: UserData) -> User:
    return ops.create_user(user2_data)


@pytest.fixture
def sample_group(sample_user: User, group1_data: GroupData) -> Group:
    return ops.create_group(creator=sample_user, data=group1_data)


@pytest.fixture
def sample_event(
        sample_user: User, sample_group: Group, event_data: EventData) -> Event:
    return ops.create_event(
        creator=sample_user, group=sample_group, data=event_data)


# ---- tests ----
def test_user_comparisons(user_data: UserData, user2_data: UserData):
    # given some users data
    # when User instances are created
    user1 = ops.create_user(user_data)
    user2 = ops.create_user(user2_data)

    # then they compare as expected
    assert (user1 < user2) == (user1.email < user2.email), \
        'users are sorted by email'

    assert ops.create_user(user_data) != user1, \
        'two new user instances cannot be equal'
    other_user = User(email=user_data.email, password='x', id=user1.id)
    assert other_user == user1, \
        'user with same id and email compare equal'

    # and user instances can be used in sets or as dict keys
    users = {user1: user1, user2: user2}
    assert user1 in users


def test_group_comparisons(
        sample_user: User, group1_data: GroupData, group2_data: GroupData):
    # given some group data
    # when Group instances are created
    group1 = ops.create_group(creator=sample_user, data=group1_data)
    group2 = ops.create_group(creator=sample_user, data=group2_data)

    # then they compare as expected
    assert ops.create_group(creator=sample_user, data=group1_data) != group1, \
        'two new group instances cannot be equal'
    other_group = Group(creator=sample_user, id=group1.id, name=group1.name)
    assert other_group == group1, \
        'groups with same id and name compare equal'

    assert (group1 < group2) == (group1.name < group2.name), \
        'groups are sorted by name'

    # and group instances can be used in sets or as dict keys
    groups = {group1: group1, group2: group2}
    assert group2 in groups


def test_event_participation(
        sample_user: User, sample_event: Event, sample_user2: User):
    # given an event and another user

    # when no user volunteered but at lest one is expected
    assert sample_event.participants == {}
    assert sample_event.volunteering == 1

    # then participation is not satisfied
    assert not sample_event.is_participation_enough

    # when one user indicates as back-un
    sample_event.participants[sample_user] = Participation.BACK_UP
    # then participation is still not satisfied
    assert not sample_event.is_participation_enough

    # when another user volunteers
    sample_event.participants[sample_user2] = Participation.VOLUNTEER
    # then participation is now satisfied
    assert sample_event.is_participation_enough


def test_event_comparisons(
        sample_user: User,
        sample_group: Group,
        event_data: EventData,
        event2_data: EventData
):
    # given some event data
    # when event instances are created
    event1 = ops.create_event(
        creator=sample_user, group=sample_group, data=event_data)
    event2 = ops.create_event(
        creator=sample_user, group=sample_group, data=event2_data)

    # then they compare as expected
    other_event = ops.create_event(
        creator=sample_user, group=sample_group, data=event_data)
    assert other_event != event1  # same id would be needed

    assert (event1 < event2) == (event1.start < event2.start), \
        'event instances are sorted by start datetime'

    # event can be used in sets or as dictionary keys
    events = {event1: 1, event2: 2}
    assert event2 in events


def tests_inequalities(
        sample_user: User, sample_group: Group, sample_event: Event):
    # model instances cannot be compared to other objects, both directions
    combs = combinations([sample_user, sample_group, sample_event], 2)
    for obj0, obj1 in combs:
        with pytest.raises(TypeError):
            _ = obj0 < obj1  # type: ignore
        with pytest.raises(TypeError):
            _ = obj1 < obj0  # type: ignore

        assert obj0 != obj1
        assert obj1 != obj0
