import os

import pytest

from jegere.config import get_env

DEFAULT: str = "some very unlikely value for an environment variable"


def test_get_env_var_existing():
    # given a variable that exists
    key = list(os.environ.keys())[0]

    # when is it requested with no converter
    # then the bare value is returned
    assert get_env(key=key) == os.environ[key]

    # when it is requested with a converter
    # then this converter is used
    assert get_env(key=key, converter=lambda x: DEFAULT) == DEFAULT


def test_get_env_var_not_existing():
    # given a variable name not likely to exist
    key: str = "it_is_very_unlikely_that_this_is_an_environment_variable_name"

    # when it is requested with a default
    # then the default is returned
    assert get_env(key=key, default=DEFAULT) == DEFAULT

    # when it is requested with no default
    # then an exception is raised
    with pytest.raises(KeyError):
        get_env(key=key, default=None)
