import pytest

from jegere.domain.crypto import BcryptPasswordManager
from jegere.domain.exc import WrongPassword

pw_manager = BcryptPasswordManager(bcrypt_rounds=5)  # make it fast


@pytest.mark.parametrize(
    ('password', 'candidates'),
    [
        ('something IMPOSSIble tO gu3ss!!', ['test&', 'something very close']),
    ]
)
def test_password_manager_success(password: str, candidates: list[str]):
    # given some password
    # when it is hashed
    pw_hash: str = pw_manager.hash(password)

    # then
    assert pw_hash != password,\
        "hashed password is never equal to the password"
    # password check always succeed with source password
    pw_manager.check(password, pw_hash)
    # and no candidate matches the password
    for candidate in candidates:
        if candidate != password:
            with pytest.raises(WrongPassword):
                pw_manager.check(candidate, pw_hash)
