import time
from secrets import compare_digest

import pytest
from hypothesis import given, settings, strategies as st, HealthCheck

from jegere.domain.crypto import Token
from jegere.domain.model import User, UserData

from .conftest import FlaskTestBackend, request


def test_user_register(test_client):
    # given some user data
    user_data = UserData(email='user1@some.where', password='1234')

    # when registration is requested
    response = test_client.post(
        '/api/users/register',
        json=user_data._asdict(),
    )

    # then request is accepted
    assert response.status_code == 201  # Created
    assert response.json is not None
    assert response.json['message'] == "new user created"

    # and id of the created user is returned
    assert 'user_id' in response.json, "new user id is returned"

    # when provided link is used to request to retrieval of the created user
    link: str = response.json['link']
    assert link.startswith('/api/users/')
    user_id = int(link[len('/api/users/'):])
    link_response = test_client.get(link)
    # then retrieved information is the expected one
    assert link_response.status_code == 200
    assert response.json is not None
    assert link_response.json['user']['email'] == user_data.email

    # and a token is returned, allowing retrieval of user information
    assert 'token' in response.json
    token = response.json['token']
    assert isinstance(token, str)
    me_response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=f'Bearer {token}')
    )
    assert me_response.status_code == 200
    assert response.json is not None
    assert me_response.json['user']['user_id'] == user_id
    assert me_response.json['user']['email'] == user_data.email


def test_user_duplicate_registration(test_client_with_one_user):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user
    user_email = user_data.email

    # when attempting to register with same email
    new_user_data = UserData(email=user_email, password='someOther')
    response = test_client.post(
        '/api/users/register',
        json=new_user_data._asdict(),
    )

    # then registration is refused
    assert response.status_code == 409  # Conflict
    assert response.json is not None
    assert response.json['message'] == \
           f"email already registered: '{user_email}'"


@pytest.mark.parametrize(
    ('incomplete_data', 'exp_msg'),
    [
        (dict(), "missing field in request: 'email'"),
        (dict(password='bob'), "missing field in request: 'email'"),
        (dict(email='bob'), "missing field in request: 'password'"),
    ]
)
def test_user_register_robustness(test_client, incomplete_data, exp_msg):
    # given a test client
    # when a request to create a new user is provided with a missing field
    response = test_client.post(
        '/api/users/register',
        json=incomplete_data)

    # then request fails
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == exp_msg


def test_user_login_with_correct_credentials(test_client_with_one_user):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user

    # when this user logs in, using their credentials
    response = test_client.post('/api/users/login', json=user_data._asdict())

    # then a new token is returned
    assert response.status_code == 200
    assert response.json is not None
    # TODO: following assertion is weird: why should the tokens be equal?
    # If we expect to get a new token, it seems the test passes because the
    # token generator takes time into account, but has not a clock precise
    # enough to generate a different token between fixture run and test run.
    assert ('token' in response.json
            and compare_digest(response.json['token'], token))
    assert response.json['user']['user_id'] == user_id


def test_user_login_with_wrong_credentials(test_client_with_one_user):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user

    # when this user logs in, using their credentials
    response = test_client.post(
        '/api/users/login',
        json=dict(email=user_data.email, password='wroNGG!!'))

    # then
    assert response.status_code == 401
    assert response.json is not None
    assert 'token' not in response.json, "no token is returned"
    assert response.json['message'] == "unknown user or wrong password"


@pytest.mark.parametrize(
    ('route', 'credentials'),
    [
        ('/api/users/login', dict(email='', password='')),  # both empty
        ('/api/users/login', dict(email='', password='oéeou')),  # email empty
        ('/api/users/login', dict(email='od', password='')),  # password empty
    ]
)
def test_empty_credentials(test_client, route, credentials):
    # given some wrong or empty credentials
    # when user login is requested with these credentials
    response = test_client.post(route, json=credentials)

    # then request fails
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == \
        'email and password shall be non-empty'


@pytest.mark.parametrize(
    ('route', 'credentials'),
    [
        ('/api/users/login', dict(email='')),  # password missing
        ('/api/users/login', dict(password='')),  # email missing
    ]
)
def test_missing_credentials(test_client, route, credentials):
    # given some wrong or empty credentials
    # when user login is requested with these credentials
    response = test_client.post(route, json=credentials)

    # then request fails
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == \
        'email and/or password missing in request'


def test_user_logout(test_client_with_one_user):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user
    # and logs again to request a new token, waiting a little so that the
    # generated token is different
    time.sleep(0.1)
    other_token = token
    while compare_digest(other_token, token):
        other_token = test_client.post(
            '/api/users/login',
            json=user_data._asdict(),
        ).json['token']
        time.sleep(0.1)

    # when user requests to log out with a given token
    response = test_client.post(
        '/api/users/logout',
        headers=dict(Authorization=f'Bearer {token}')
    )

    # then
    assert response.status_code == 200, "request is accepted"
    assert response.json is not None
    assert response.json['message'] == "user logged out for this token"

    usage_response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=f'Bearer {token}')
    )
    assert usage_response.status_code == 401, \
        "the same token cannot be used anymore"
    assert response.json is not None
    assert usage_response.json['message'] == "blacklisted token, log in again"
    assert usage_response.json['link'] == '/api/users/login'

    # but other tokens still live
    other_response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=f'Bearer {other_token}')
    )
    assert other_response.status_code == 200, "other token can still be used"


def test_user_access(sample_backend: FlaskTestBackend):
    # given a sample app
    test_client = sample_backend.test_client
    user: User = sample_backend.users[1]
    token: Token = sample_backend.tokens[1]

    # when details of a user are requested
    response = request(
        test_client.get,
        f'/api/users/{user.id}',
        token=token)

    # then public details are retrieved
    assert response.status_code == 200
    assert response.json is not None
    assert all(field in response.json['user'] for field in ('user_id', 'email'))


# This test does not mutate the test_client, so test_client can be created by
# the fixture only once at function call, with no need to recreate it between
# two Hypothesis @given calls.
# Therefore, the Hypothesis health check can be disabled -- see
# https://hypothesis.readthedocs.io/en/latest/healthchecks.html#hypothesis.HealthCheck.function_scoped_fixture
@settings(suppress_health_check=[HealthCheck.function_scoped_fixture])
@given(user_id=st.integers(min_value=0))
def test_user_access_robustness(test_client, user_id: int):
    # given no user exists
    # when any user is requested
    response = test_client.get(f'/api/users/{user_id}')

    # then response indicates that user does not exist
    assert response.status_code == 404
    assert response.json is not None
    assert response.json['message'] == f"no user with user_id={user_id}"


INVALID_TOKEN = (
    'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCIsImtpZCI6IjIzMDQ5ODE1MWMyMTRiNzg4ZGQ5N'
    '2YyMmI4NTQxMGE1In0.eyJzb21lIjoicGF5bG9hZCJ9.DogbDGmMHgA_bU05TAB-R6geQ2nMU'
    '2BRM-LnYEtefwg')


def test_user_access_by_wrong_token(test_client_with_one_user):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user

    # when a different token is used to log in
    response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=f'Bearer {INVALID_TOKEN}'),
    )

    # then
    assert response.status_code == 401, "authentication error is returned"
    assert response.json is not None
    assert response.json['message'] == 'Token is invalid and cannot be used'
    assert response.json['link'] == '/api/users/login'


def test_user_access_with_no_token(test_client_with_one_user):
    # given a user exists
    test_client, *_ = test_client_with_one_user

    # when a different token is used to log in
    response = test_client.get(
        '/api/users/me',
        headers=dict(),
    )

    # then
    assert response.status_code == 401, "authentication error is returned"
    assert response.json is not None
    assert response.json['message'].startswith('found no Authorization header')


def test_unknown_user_access_by_token(web_root, test_client_with_one_user):
    # given a user has been created, but is removed from repository
    test_client, user_data, user_id, token = test_client_with_one_user
    web_root.uow.store.del_user(user_id)

    # when the token is used to request user details
    response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=f'Bearer {token}')
    )

    # then the request is refused with relevant message
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'no user matching provided token'
    # and link to login is provided
    assert response.json['link'] == '/api/users/login'


def test_users_listing(sample_backend: FlaskTestBackend):
    # given an app with several users
    test_client = sample_backend.test_client
    users: list[User] = sample_backend.users
    member_token = sample_backend.tokens[0]

    # when a user requests list of users
    response = request(
        test_client.get,
        '/api/users/all',
        token=member_token)
    # then a list of all users is returned
    assert response.status_code == 200
    assert response.json is not None
    assert len(response.json['users']) == len(users)
    assert set(user['user_id'] for user in response.json['users']) == \
        set(user.id for user in users)
    assert set(response.json['users'][0].keys()) == {'user_id', 'email', 'link'}
