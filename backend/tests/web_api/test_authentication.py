"""This test module provides the checks for authentication."""

from typing import Optional

import pytest

from .conftest import FlaskTestBackend


@pytest.mark.parametrize(
    ('endpoint', 'method', 'payload'),
    [
        ('/api/users/me', 'get', None),
        ('/api/users/logout', 'post', None),
        ('/api/users/all', 'get', None),
        ('/api/groups/new', 'post', dict(name='grp')),
        ('/api/groups/mine', 'get', None),
        ('/api/events/new', 'post', dict(title='event123', group_id=142)),
    ]
)
def test_endpoint_requires_authentication(
        sample_backend: FlaskTestBackend,
        endpoint: str,
        method: str,
        payload: Optional[dict],
):
    # given an instance with one user
    test_client = sample_backend.test_client

    # when the endpoint is requested with no token and a valid payload
    meth = getattr(test_client, method)
    if payload is None:
        response = meth(endpoint)
    else:
        response = meth(endpoint, json=payload)

    # then error is raised
    assert response.status_code == 401  # Unauthorized


def test_authentication_parsing(test_client):
    # given a sample backend
    # when a resource limited to authenticated user is requested with no token
    response = test_client.get('/api/users/me')
    # then failure is indicated
    assert response.status_code == 401
    # and a relevant message is provided
    assert response.json['message'].startswith('found no Authorization header')

    # when a void token is provided to same resource
    response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=''),
    )
    # then failure is indicated
    assert response.status_code == 401
    # and a relevant message is provided
    assert response.json['message'].startswith(
        'found empty Authorization header')


@pytest.mark.parametrize(
    'auth_fmt',
    [
        'Bearer:{}',  # colons not expected
        'Bearer: {}',  # ... with or without space
        '{}',
        'bearer{}',  # uppercase-only expected
        'bearer {}',  # ... with or without space
        'Bearer{}'  # space expected
        # note: empty token is tested apart, as return message is different.
    ]
)
def test_auth_robustness_to_token_fmt(test_client_with_one_user, auth_fmt):
    # given a user exists
    test_client, user_data, user_id, token = test_client_with_one_user

    # when a different token is used to log in
    response = test_client.get(
        '/api/users/me',
        headers=dict(Authorization=auth_fmt.format(token))
    )

    # then an authentication error is returned
    assert response.status_code == 401
    # and a relevant message is provided
    assert response.json['message'] == \
        "Authorization header format error; shall be like 'Bearer <token>'"
