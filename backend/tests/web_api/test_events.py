from dataclasses import asdict

from jegere.domain.model import Event, EventData, Participation
from jegere.domain import ops
from jegere.domain.utils import hms_from_timedelta

from .conftest import FlaskTestBackend, request


def test_events_listing(sample_backend_with_privacy: FlaskTestBackend):
    # given a backend with some events and users
    test_client = sample_backend_with_privacy.test_client
    # and associated users
    token_in_public_group = sample_backend_with_privacy.tokens[2]
    token_in_public_group_with_private_event = \
        sample_backend_with_privacy.tokens[1]
    token_in_private_group = sample_backend_with_privacy.tokens[0]

    # when an anonymous request is performed to list the events
    response = request(test_client.get, '/api/events/all')
    # then only the public events in public groups are returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 2 events'
    assert {'event_id', 'link'} <= set(response.json['events'][0].keys())
    expected_ids = {event.id
                    for event in sample_backend_with_privacy.events[2:]}
    retrieved_ids = {event['event_id'] for event in response.json['events']}
    assert retrieved_ids == expected_ids

    # when an authenticated request is performed, out of private groups
    response = request(
        test_client.get,
        '/api/events/all',
        token=token_in_public_group)
    # then only the public events in public groups are retrieved
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 2 events'
    expected_ids = {event.id
                    for event in sample_backend_with_privacy.events[2:]}
    retrieved_ids = {event['event_id'] for event in response.json['events']}
    assert retrieved_ids == expected_ids

    # when an authenticated request is performed in a group with a private event
    response = request(
        test_client.get,
        '/api/events/all',
        token=token_in_public_group_with_private_event)
    # then public events and private event in member group are retrieved
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 3 events'
    expected_ids = {event.id
                    for event in sample_backend_with_privacy.events[1:]}
    retrieved_ids = {event['event_id'] for event in response.json['events']}
    assert retrieved_ids == expected_ids

    # when an authenticated request is performed, in a private group
    response = request(
        test_client.get,
        '/api/events/all',
        token=token_in_private_group)
    # then public events and events in private group are retrieved
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 3 events'
    expected_ids = {event.id
                    for event in sample_backend_with_privacy.events[2:]} \
        | {sample_backend_with_privacy.events[0].id}
    retrieved_ids = {event['event_id'] for event in response.json['events']}
    assert retrieved_ids == expected_ids


def test_get_event_details(sample_backend_with_privacy: FlaskTestBackend):
    # given a backend with some events and users and some privacy
    test_client = sample_backend_with_privacy.test_client
    token_in_public_group = sample_backend_with_privacy.tokens[2]
    token_in_public_group_with_private_event = \
        sample_backend_with_privacy.tokens[1]
    token_in_private_group = sample_backend_with_privacy.tokens[0]

    public_event = sample_backend_with_privacy.events[2]
    private_event_in_public_group = sample_backend_with_privacy.events[1]
    event_in_private_group = sample_backend_with_privacy.events[0]

    # when an anonymous request is performed to get details on public event
    response = request(test_client.get, f'/api/events/{public_event.id}')
    # then public details of the event are provided
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'event found'
    assert response.json['event']['event_id'] == public_event.id
    assert response.json['event']['title'] == public_event.title
    expected_fields = {
        'title', 'group_id', 'group_link', 'start', 'duration',
        'volunteering', 'is_participation_enough'}
    assert expected_fields <= set(response.json['event'].keys())
    assert len(response.json['event']['duration'].split(':')) == 2, \
        "format shall be HH:MM (no days, no seconds)"

    # when an anonymous request is performed to get details on private event
    response = request(
        test_client.get,
        f'/api/events/{private_event_in_public_group.id}')
    # then request fails
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to members'

    # when non-member requests private event details
    response = request(
        test_client.get,
        f'/api/events/{private_event_in_public_group.id}',
        token=token_in_public_group)
    # then request fails
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to members'

    # when a member requests details of private event in their public group
    response = request(
        test_client.get,
        f'/api/events/{private_event_in_public_group.id}',
        token=token_in_public_group_with_private_event)
    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'event found'
    assert response.json['event']['event_id'] == \
        private_event_in_public_group.id

    # when a non-member requests details of private event in a public group
    response = request(
        test_client.get,
        f'/api/events/{private_event_in_public_group.id}')
    # then request succeeds
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to members'

    # when a member requests details of event in their private group
    response = request(
        test_client.get,
        f'/api/events/{event_in_private_group.id}',
        token=token_in_private_group)
    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'event found'
    assert response.json['event']['event_id'] == event_in_private_group.id


def test_events_participation_display(sample_backend: FlaskTestBackend):
    # given a backend with some groups, events, and participation
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    event = sample_backend.events[0]
    user0 = sample_backend.users[0]
    user1 = sample_backend.users[1]
    user2 = sample_backend.users[2]
    user3 = sample_backend.users[3]
    # and some participation
    ops.add_member(group=group, user=user1)
    ops.add_member(group=group, user=user2)
    ops.add_member(group=group, user=user3)
    ops.set_participation(
        event=event, participant=user0, participation=Participation.VOLUNTEER)
    ops.set_participation(
        event=event, participant=user1, participation=Participation.BACK_UP)
    ops.set_participation(
        event=event, participant=user2, participation=Participation.ABSENT)

    # when the volunteers to an event are requested
    response = request(
        test_client.get,
        f'/api/events/{event.id}/volunteers')
    # then list of volunteers is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 volunteer'
    assert len(response.json['volunteers']) == 1
    assert response.json['volunteers'][0]['user_id'] == user0.id

    # when the back-ups to an event are requested
    response = request(
        test_client.get,
        f'/api/events/{event.id}/back-ups')
    # then list of back-ups is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 back-up'
    assert len(response.json['back-ups']) == 1
    assert response.json['back-ups'][0]['user_id'] == user1.id

    # when the absents to an event are requested
    response = request(
        test_client.get,
        f'/api/events/{event.id}/absents')
    # then list of absents is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 absent'
    assert len(response.json['absents']) == 1
    assert response.json['absents'][0]['user_id'] == user2.id

    # when a member requests members that did not set their participation
    response = request(
        test_client.get,
        f'/api/events/{event.id}/no-answers')
    # then list of users with no answer is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 no-answer'
    assert response.json['no-answers'][0]['user_id'] == user3.id

    # when some other status is requested
    response = request(
        test_client.get,
        f'/api/events/{event.id}/some-status')
    # then request fails
    assert response.status_code == 400


def test_event_creation(
        sample_backend: FlaskTestBackend,
        events_dataset: list[EventData],
):
    # given a sample backend
    test_client = sample_backend.test_client
    group = sample_backend.groups[3]
    token = sample_backend.tokens[3]
    # and necessary data to create an event, taking care only to send primitives
    event_data = events_dataset[3]
    event_data_prim = {
        **asdict(event_data),  # do a copy, then take care of format
        'start': event_data.start.isoformat(),
        'duration': hms_from_timedelta(event_data.duration),
        'is_public':  False,
        'status': event_data.status.value,
    }

    # when a group member requests creation of a new event
    response = request(
        test_client.post,
        '/api/events/new',
        json=dict(group_id=group.id, **event_data_prim),
        token=token)

    # then request is accepted
    assert response.status_code == 201  # created
    # and a new event is created
    assert response.json is not None
    assert response.json['message'] == 'new event created'
    assert set(response.json['event'].keys()) >= {'event_id', 'link'}
    # and returned identifier matches the stored one
    returned_id = response.json['event']['event_id']
    # and datetime and duration have correctly been parsed
    event = sample_backend.uow.store.get_event(event_id=returned_id)
    for field in ('title', 'start', 'duration', 'status'):
        assert getattr(event, field) == getattr(event_data, field)
    assert not event.is_public  # not in event_data


def test_event_creation_robustness_with_no_group_id(
        sample_backend: FlaskTestBackend):
    # given a sample backend
    test_client = sample_backend.test_client
    token = sample_backend.tokens[3]

    # when request to create an event is provided with fields missing
    response = request(
        test_client.post,
        '/api/events/new',
        json=dict(),
        token=token)

    # then request fails, with appropriate message
    assert response.status_code == 400  # Bad Request
    assert response.json is not None
    assert response.json['message'] == \
        "missing field(s) in request for event creation: group_id"


def test_event_creation_robustness_with_no_data(
        sample_backend: FlaskTestBackend):
    # given a sample backend
    test_client = sample_backend.test_client
    token = sample_backend.tokens[3]
    group = sample_backend.groups[3]

    # when request to create an event is provided with fields missing
    response = request(
        test_client.post,
        '/api/events/new',
        json=dict(group_id=group.id),
        token=token)

    # then request fails, with appropriate message
    assert response.status_code == 400  # Bad Request
    assert response.json is not None
    assert response.json['message'] == \
        ("missing field(s) in request: __init__() missing 3 required "
         "positional arguments: 'title', 'start', and 'duration'")


def test_event_creation_robustness_with_wrong_date_format(
        sample_backend: FlaskTestBackend,
        events_dataset: list[EventData],
):
    # given a sample backend
    test_client = sample_backend.test_client
    group = sample_backend.groups[3]
    event_data = events_dataset[3]
    token = sample_backend.tokens[3]

    # when request to create an event provides unexpected date format
    event_data_prim = {
        **asdict(event_data),  # do a copy, then take care of format
        # 'start': event_data.start.isoformat(),
        'duration': hms_from_timedelta(event_data.duration),
        'is_public':  False,
        'status': event_data.status.value,
    }
    response = request(
        test_client.post,
        '/api/events/new',
        json=dict(group_id=group.id, **event_data_prim),
        token=token,
    )
    # then request fails, with appropriate message
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == \
        ("cannot parse input(s): start shall comply with ISO-8601, "
         "but got 'Wed, 23 Dec 2020 20:00:00 GMT'")


def test_event_creation_robustness_with_wrong_duration_format(
        sample_backend: FlaskTestBackend,
        events_dataset: list[EventData],
):
    # given a sample backend
    test_client = sample_backend.test_client
    group = sample_backend.groups[3]
    event_data = events_dataset[3]
    token = sample_backend.tokens[3]

    # when request to create an event provides unexpected duration format
    wrong_duration = '8h 34minutes'
    event_data_prim = {
        **asdict(event_data),  # do a copy, then take care of format
        'start': event_data.start.isoformat(),
        'duration': wrong_duration,
        'is_public':  False,
        'status': event_data.status.value,
    }
    response = request(
        test_client.post,
        '/api/events/new',
        json=dict(group_id=group.id, **event_data_prim),
        token=token,
    )
    # then request fails, with appropriate message
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == \
        ("cannot parse input(s): duration shall be like "
         f"HH:MM or HH:MM:SS, but got '{wrong_duration}'")


def test_event_update(sample_backend: FlaskTestBackend):
    # given a backend with some user, group, event
    test_client = sample_backend.test_client
    event = sample_backend.events[0]
    member_token = sample_backend.tokens[0]

    # and some new data
    new_title = event.title + ' - updated'
    is_public = not event.is_public
    new_data = dict(
        title=new_title,
        start=str(event.start),  # unchanged
        duration=hms_from_timedelta(event.duration),  # unchanged
        is_public=is_public)

    # when a member requests update of event title
    response = request(
        test_client.put,
        f'/api/events/{event.id}',
        json=new_data,
        token=member_token)

    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == \
        f'event {event.id} updated: title, is_public'
    # and data is updated
    core_event: Event = sample_backend.uow.store.get_event(event_id=event.id)
    assert core_event.title == new_title
    assert core_event.is_public == is_public


def test_event_group_change(sample_backend: FlaskTestBackend):
    # given a user member of two groups, and an event
    test_client = sample_backend.test_client
    member = sample_backend.users[0]
    member_token = sample_backend.tokens[0]
    event = sample_backend.events[0]
    group_2 = sample_backend.groups[2]
    ops.add_member(group=group_2, user=member)

    # when member requests event group change
    old_group_id, new_group_id = event.group.id, group_2.id
    response = request(
        test_client.put,
        f'/api/events/{event.id}/group',
        json=dict(group_id=new_group_id),
        token=member_token)

    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == (
        f'event group changed, from group {old_group_id} '
        f'to group {new_group_id}')
    assert event.group.id == new_group_id


def test_event_group_change_robustness(sample_backend: FlaskTestBackend):
    # given a user member of only one groups, and an event
    test_client = sample_backend.test_client
    member_token = sample_backend.tokens[0]
    event = sample_backend.events[0]
    group_2 = sample_backend.groups[2]

    # when member requests event group change
    old_group_id, new_group_id = event.group.id, group_2.id
    response = request(
        test_client.put,
        f'/api/events/{event.id}/group',
        json=dict(group_id=new_group_id),
        token=member_token)

    # then request fails
    assert response.status_code == 403
    assert response.json is not None
    assert response.json['message'] == \
        'user shall be a member of destination group'
    assert event.group.id == old_group_id


def test_event_update_robustness_to_format(sample_backend: FlaskTestBackend):
    # given a backend with user, group, event
    test_client = sample_backend.test_client
    member_token = sample_backend.tokens[0]
    event = sample_backend.events[0]

    # when member tries to update event with invalid data
    new_data = dict(
        title='',
        start='Jeudi prochain',
        duration=hms_from_timedelta(event.duration),  # unchanged
    )
    response = request(
        test_client.put,
        f'/api/events/{event.id}',
        json=new_data,
        token=member_token)

    # then request fails with relevant message
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'].startswith(
        'cannot parse input(s): start shall comply with ISO-8601')
    # 'title' is not cached at input conversion level by the web_api, but at
    # a lower level. Therefore, there is no failure message expected here.


def test_event_update_robustness_to_content(sample_backend: FlaskTestBackend):
    # given a backend with user, group, event
    test_client = sample_backend.test_client
    member_token = sample_backend.tokens[0]
    event = sample_backend.events[0]

    # when member tries to update event with invalid data
    new_data = dict(
        title='',
        start=str(event.start),  # unchanged
        duration=hms_from_timedelta(event.duration),  # unchanged
    )
    response = request(
        test_client.put,
        f'/api/events/{event.id}',
        json=new_data,
        token=member_token)

    # then request fails with relevant message
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'].startswith(
        'invalid value(s): title shall be non-empty')


def test_event_participation_setting(sample_backend: FlaskTestBackend):
    # given a backend with some user, group, event
    test_client = sample_backend.test_client
    coordinator = sample_backend.users[0]
    coordinator_token = sample_backend.tokens[0]
    group = sample_backend.groups[0]
    event = sample_backend.events[0]
    # and a simple member in a group
    member = sample_backend.uow.store.get_user(
        user_id=sample_backend.users[1].id)
    ops.add_member(group=group, user=member)
    member_token = sample_backend.tokens[1]

    # when member request setting themself as volunteer to an event
    # (with no need of request payload)
    response = request(
        test_client.post,
        f'/api/events/{event.id}/volunteers',
        token=member_token)

    # then request succeeds
    assert response.status_code == 200
    # and event is updated
    assert event.participants[member] == Participation.VOLUNTEER

    # when coordinator sets another member participation
    response = request(
        test_client.post,
        f'/api/events/{event.id}/back-ups',
        json=dict(user_id=member.id),
        token=coordinator_token)

    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    # and event is updated
    assert event.participants[member] == Participation.BACK_UP

    # when a member requests to reset their participation
    response = request(
        test_client.post,
        f'/api/events/{event.id}/no-answers',
        token=member_token)
    # then request succeeds
    assert response.status_code == 200
    # and member is no longer in the participants
    assert member not in event.participants
    assert event not in member.events

    # when a member tries to change another member's participation
    response = request(
        test_client.post,
        f'/api/events/{event.id}/back-ups',
        json=dict(user_id=coordinator.id),
        token=member_token)

    # then request fails
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == \
        'shall act for self or be a coordinator'
    # and event is not updated
    assert coordinator not in event.participants

    # when trying to set another participation value
    response = request(
        test_client.post,
        f'/api/events/{event.id}/not-in-list',
        token=member_token)
    # then request fails
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == \
        "unknown participation query: 'not-in-list'"


def test_event_deletion(sample_backend: FlaskTestBackend):
    # given a backend with some user, group, event
    test_client = sample_backend.test_client
    coordinator_token = sample_backend.tokens[0]
    event = sample_backend.events[0]

    # when a coordinator requests group deletion
    response = request(
        test_client.delete,
        f'/api/events/{event.id}',
        token=coordinator_token)
    # then request is successful, and relevant message is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == f'event {event.id} deleted'
    # and event is removed from core
    assert sample_backend.uow.store.find_event(event_id=event.id) is None


def test_event_deletion_robustness(sample_backend: FlaskTestBackend):
    # given a backend with some user, group, event
    test_client = sample_backend.test_client
    member = sample_backend.users[1]
    member_token = sample_backend.tokens[1]
    group = sample_backend.groups[0]
    event = sample_backend.events[0]
    ops.add_member(group=group, user=member)

    # when a member requests group deletion
    response = request(
        test_client.delete,
        f'/api/events/{event.id}',
        token=member_token)
    # then request fails, and relevant message is returned
    assert response.status_code == 401  # Not Authorized
    assert response.json is not None
    assert response.json['message'] == 'accessible only to coordinators'
    # and event is not removed from core
    assert sample_backend.uow.store.find_event(event_id=event.id) is not None
