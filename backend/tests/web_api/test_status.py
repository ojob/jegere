def test_backend_healthy(test_client):
    # given the webserver is running
    # when healthy status is requested
    response = test_client.get('/api/status')

    # then answer is positive
    assert response.status_code == 200
    assert response.json['healthy'] is True


def test_frontend_serve_of_index(test_client):
    # given the test defaults
    # when root is requested
    response = test_client.get('/')

    # then front-end build is provided
    assert response.status_code == 200
    assert response.data.startswith(b'<html>\n  <h1>Fake Index Page</h1>')


def test_frontend_serve_of_static_files(test_client):
    # given the test defaults
    # when root is requested
    response = test_client.get('/css/style.css')

    # then front-end build is provided
    assert response.status_code == 200
    assert response.data.startswith(b'* {\n  font-family: sans;')
