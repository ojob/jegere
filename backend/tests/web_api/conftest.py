from typing import Any, Optional, NamedTuple

import pytest
from flask import Response
from flask.testing import FlaskClient

from jegere.bootstrap import build_app, WebRoot
from jegere.config import test_config
from jegere.domain.model import Event, Group, User
from jegere.domain.crypto import Token
from jegere.service_layer.unit_of_work import UnitOfWork

from ..conftest import UserData


# ---- structures, types, etc ----
class FlaskTestClient(NamedTuple):
    test_client: FlaskClient
    user_data: UserData
    user_id: int
    token: Token


class FlaskTestBackend(NamedTuple):
    test_client: FlaskClient
    users: list[User]
    groups: list[Group]
    events: list[Event]
    tokens: list[Token]
    uow: UnitOfWork


# ---- fixtures ----
@pytest.fixture
def web_root() -> WebRoot:
    return build_app(config=test_config)


@pytest.fixture
def test_client(web_root: WebRoot) -> FlaskClient:
    return web_root.app.test_client()


@pytest.fixture
def test_client_with_one_user(test_client: FlaskClient) -> FlaskTestClient:
    user_data = UserData(email='first.user@some.where', password='1234')
    response = test_client.post('/api/users/register', json=user_data._asdict())
    json: Optional[Any] = response.json
    if json is None:
        raise ValueError('response is not in JSON format')
    user_id = json['user_id']
    token = json['token']

    return FlaskTestClient(test_client, user_data, user_id, token)


@pytest.fixture
def sample_backend(sample_uow) -> FlaskTestBackend:
    app = build_app(uow=sample_uow.uow).app

    # dirty creation of the tokens, with no bothering to go through the storage:
    # as long as they are both valid and not blacklisted, they will be accepted.
    tokens = [
        sample_uow.uow.token_manager.encode(user.id)
        for user in sample_uow.users
    ]

    return FlaskTestBackend(
        test_client=app.test_client(),
        users=sample_uow.users,
        groups=sample_uow.groups,
        events=sample_uow.events,
        tokens=tokens,
        uow=sample_uow.uow,
    )


@pytest.fixture
def sample_backend_with_privacy(
        sample_backend: FlaskTestBackend) -> FlaskTestBackend:
    group0 = sample_backend.groups[0]
    group0.is_public = False
    event1 = sample_backend.events[1]
    event1.is_public = False
    return sample_backend


# ---- helpers ----
def request(
        method,
        endpoint: str,
        json: dict = None,
        token: Token = None,
) -> Response:
    """Take care to authenticate if *token* is provided."""
    kwargs = {}
    if json is not None:
        kwargs['json'] = json
    if token is not None:
        kwargs['headers'] = dict(Authorization=f'Bearer {token}')
    return method(endpoint, **kwargs)
