from jegere.domain.model import Event, Group
from jegere.domain.crypto import Token
from jegere.domain import ops

from .conftest import FlaskClient, FlaskTestBackend, FlaskTestClient, request


def test_group_creation(test_client_with_one_user: FlaskTestClient):
    # given a backend with one user
    test_client = test_client_with_one_user.test_client
    token = test_client_with_one_user.token

    # and group creation data
    group_data = dict(name='First Group', is_public=False)

    # when authenticated user requests group creation
    response = request(
        test_client.post, '/api/groups/new', json=group_data, token=token)

    # then the group is created, with expected success message
    assert response.status_code == 201
    assert response.json is not None
    assert 'group_id' in response.json['group']
    group_id = response.json['group']['group_id']
    assert response.json['message'] == 'new group created'
    assert response.json['group']['link'] == f'/api/groups/{group_id}'


def test_group_creation_robustness(test_client_with_one_user):
    # given a backend with one user
    test_client = test_client_with_one_user.test_client
    token = test_client_with_one_user.token
    # and invalid group creation data
    group_data = dict(title='unused field')

    # when authenticated user requests group creation
    response = request(
        test_client.post, '/api/groups/new', json=group_data, token=token)
    # then the group is not created, and expected message is returned
    assert response.status_code == 400
    assert response.json is not None
    assert 'group_id' not in response.json
    assert response.json['message'] == 'name missing in request'


def test_group_update(sample_backend: FlaskTestBackend):
    # given a backend with a group
    test_client = sample_backend.test_client
    group: Group = sample_backend.groups[0]
    old_name = group.name
    old_visibility = group.is_public
    group_id = group.id
    coordinator_token: Token = sample_backend.tokens[0]

    # when a coordinator requests group update
    payload = dict(name=old_name+'1', is_public=not old_visibility)
    response = request(
        test_client.put,
        f'/api/groups/{group_id}',
        json=payload,
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    # and relevant message is returned
    assert response.json is not None
    assert response.json['message'] == \
           f'group {group_id} updated: name, is_public'
    # and group characteristics are updated
    group = sample_backend.uow.store.get_group(group_id=group_id)
    assert group.name != old_name
    assert group.is_public != old_visibility


def test_group_deletion(sample_backend: FlaskTestBackend):
    # given a backend with a group with an event
    test_client = sample_backend.test_client
    group: Group = sample_backend.groups[0]
    coordinator_token: Token = sample_backend.tokens[0]
    event: Event = sample_backend.events[0]
    assert event.group == group

    # when a coordinator requests group deletion
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}',
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    # and relevant message is returned
    assert response.json is not None
    assert response.json['message'] == \
        f'group {group.id} deleted; 1 event dropped'
    # and group object does not exist anymore
    assert sample_backend.uow.store.find_group(group_id=group.id) is None
    # and group events are not there anymore as well
    assert sample_backend.uow.store.find_event(event_id=event.id) is None


def test_group_deletion_authorization(sample_backend: FlaskTestBackend):
    # given a backend with a group
    test_client = sample_backend.test_client
    group: Group = sample_backend.groups[0]
    # and some other user that is not a coordinator
    some_user_token: Token = sample_backend.tokens[2]

    # when this user requests deletion of the group
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}',
        token=some_user_token)

    # then request is refused, with relevant message
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to coordinators'


def test_group_update_then_deletion(sample_backend: FlaskTestBackend):
    # given a backend with a group
    test_client = sample_backend.test_client
    group: Group = sample_backend.groups[0]
    old_name = group.name
    old_visibility = group.is_public
    group_id = group.id
    coordinator_token: Token = sample_backend.tokens[0]

    # when a coordinator requests group update
    payload = dict(name=old_name+'1', is_public=not old_visibility)
    _ = request(
        test_client.put,
        f'/api/groups/{group_id}',
        json=payload,
        token=coordinator_token)
    # and a coordinator requests group deletion
    response = request(
        test_client.delete,
        f'/api/groups/{group_id}',
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    # and relevant message is returned
    assert response.json is not None
    assert response.json['message'] == \
        f'group {group_id} deleted; 1 event dropped'
    # and group object does not exist anymore
    assert sample_backend.uow.store.find_group(group_id=group_id) is None


def test_group_details_access_for_member(sample_backend: FlaskTestBackend):
    # given a backend with some clients and some groups
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    member_token = sample_backend.tokens[0]

    # when group details are requested with no authentication
    response = request(
        test_client.get,
        f'/api/groups/{group.id}',
        token=member_token)

    # then group details are returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'group found'
    assert response.json['group']['name'] == group.name
    assert response.json['group']['members_nb'] == len(group.members)
    assert {'is_member', 'is_coordinator'} <= set(response.json['group'].keys())


def test_group_details_access_for_non_members(sample_backend: FlaskTestBackend):
    # given a backend with some clients and some groups
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]

    # when group details are requested with no authentication
    response = test_client.get(f'/api/groups/{group.id}')

    # then group details are returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'group found'
    assert response.json['group']['name'] == group.name
    assert response.json['group']['members_nb'] == len(group.members)
    assert set(response.json['group'].keys()).isdisjoint(
        {'is_member', 'is_coordinator'})

    # when group details are requested with non-member authorization
    # then group details are still visible
    response = test_client.get(f'/api/groups/{group.id}')
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['group']['name'] == group.name
    assert response.json['group']['members_nb'] == len(group.members)


def test_group_details_when_not_existing(test_client: FlaskClient):
    # given a backend with no content and some idiot group id
    weird_id = 1384318852000

    # when details of a non-existing group are requested
    response = test_client.get(f'/api/groups/{weird_id}')

    # then request is not successful
    assert response.status_code == 404
    assert response.json is not None
    assert response.json['message'] == f'no group with group_id={weird_id}'


def test_group_details_hidden(sample_backend: FlaskTestBackend):
    # given a backend with some clients and some groups
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    member_token = sample_backend.tokens[0]
    other_token = sample_backend.tokens[1]

    # when a group is set as not public
    group.is_public = False  # with direct access!

    # then it is visible to members of the group
    response = request(
        test_client.get, f'/api/groups/{group.id}', token=member_token)
    assert response.status_code == 200

    # and it is not visible to unauthenticated user
    response = request(test_client.get, f'/api/groups/{group.id}', token=None)
    assert response.status_code == 401

    # and it is not visible to users that are not members of the group
    response = request(
        test_client.get, f'/api/groups/{group.id}', token=other_token)
    assert response.status_code == 401


def test_groups_listing(sample_backend: FlaskTestBackend):
    # given a backend with some groups
    test_client = sample_backend.test_client
    # and a user is member of several groups, among which some are private
    group0, group1 = sample_backend.groups[0:2]
    user = sample_backend.users[0]
    token = sample_backend.tokens[0]
    ops.add_member(group=group1, user=user)
    group1.is_public = False

    # when the user requests the whole list of groups
    response = request(test_client.get, '/api/groups/all', token=token)
    # then all groups are listed, including the privates ones for which they
    # are a member
    assert response.status_code == 200
    assert response.json is not None
    assert len(response.json['groups']) == 5

    # when the user requests the list of their groups
    response = request(test_client.get, '/api/groups/mine', token=token)
    # then user groups are listed, including the private ones, with private
    # details provided, but no other group
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 2 groups'
    assert len(response.json['groups']) == 2
    resp_groups = response.json['groups']
    exp_group_ids = {group0.id, group1.id}
    exp_group_names = {group0.name, group1.name}
    assert set(group['group_id'] for group in resp_groups) == exp_group_ids
    assert set(group['name'] for group in resp_groups) == exp_group_names

    # when another user requests a list of all groups
    other_token = sample_backend.tokens[2]
    response = request(test_client.get, '/api/groups/all', token=other_token)
    # then all public groups are returned
    assert response.json is not None
    assert len(response.json['groups']) == 4
    # and only public details are listed
    assert 'creator' not in response.json['groups'][0]

    # when no authentication is provided for request of all groups
    response = request(test_client.get, '/api/groups/all', token=None)
    # then all public groups are returned
    assert response.status_code == 200
    assert response.json is not None
    assert len(response.json['groups']) == 4
    # and only public details are listed
    assert 'creator' not in response.json['groups'][0]


def test_group_members_listing(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator = sample_backend.users[0]
    # and a group with two members
    some_user = sample_backend.users[1]
    ops.add_member(group=group, user=some_user)

    # when group members are requested by a member of the group
    some_token = sample_backend.tokens[1]
    response = request(
        test_client.get,
        f'/api/groups/{group.id}/members',
        token=some_token,
    )
    # then members are listed
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 2 members'
    assert 'members' in response.json
    assert set(response.json['members'][0].keys()) == \
        {'user_id', 'email', 'is_coordinator', 'link'}
    assert set(user['user_id'] for user in response.json['members']) == \
        {coordinator.id, some_user.id}


def test_group_membership_addition(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator_token = sample_backend.tokens[0]
    # and a group with two members
    some_user = sample_backend.users[1]
    ops.add_member(group=group, user=some_user)
    # and the token of a third user
    other_user = sample_backend.users[2]

    # when addition of a member is requested by a coordinator
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(user_id=other_user.id),
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'user added as member'

    # when the same request is performed again
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(user_id=other_user.id),
        token=coordinator_token)
    # then no failure, just a different message
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == "user is already a member"


def test_group_membership_authorization(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]

    # when members list is requested with no authentication
    response = request(test_client.get, f'/api/groups/{group.id}/members')
    # then request is refused
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'].startswith('found no Authorization header')

    # when members list is requested by a non-member
    dudes_token = sample_backend.tokens[3]
    response = request(
        test_client.get, f'/api/groups/{group.id}/members', token=dudes_token)
    # then request is refused
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to members'

    # when addition of user is requested by just a member of the group
    some_token = sample_backend.tokens[1]
    other_user = sample_backend.users[2]
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(user_id=other_user.id),
        token=some_token)
    # then it is refused
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'accessible only to coordinators'


def test_group_membership_addition_robustness(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator_token = sample_backend.tokens[0]

    # when addition of a member is requested with no id
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(),
        token=coordinator_token)

    # then error is returned
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == 'user_id missing in request'

    # when addition of a member is requested with wrongly formatted user_id
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(user_id=None),
        token=coordinator_token)

    # then error is returned
    assert response.status_code == 400
    assert response.json is not None
    assert response.json['message'] == "cannot use provided user_id: 'None'"

    # when addition of a member is requested with an unknown user id
    unk_user_id = 6814668400684
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/members',
        json=dict(user_id=unk_user_id),
        token=coordinator_token)

    # then error is returned
    assert response.status_code == 404
    assert response.json is not None
    assert response.json['message'] == f"no user with user_id={unk_user_id}"


def test_group_membership_removal(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    # and a group with two members
    some_user = sample_backend.users[1]
    some_token = sample_backend.tokens[1]
    ops.add_member(group=group, user=some_user)

    # when member requests to leave a group
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/members/{some_user.id}',
        token=some_token,
    )
    # then request is accepted and relevant message is provided
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'user removed from group members'
    assert some_user not in group.members

    # when a new request is performed
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/members/{some_user.id}',
        token=some_token,
    )
    # then request is accepted with no error (idempotence principle)
    assert response.status_code == 200
    # and relevant message is provided
    assert response.json is not None
    assert response.json['message'] == 'user is not a group member'


def test_group_membership_removal_robustness(sample_backend: FlaskTestBackend):
    # given a backend with some groups and coordinators
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator = sample_backend.users[0]
    coordinator_token = sample_backend.tokens[0]
    # and a group with two members
    some_user = sample_backend.users[1]
    some_token = sample_backend.tokens[1]
    ops.add_member(group=group, user=some_user)

    # when unexisting member is requested to be removed from a group
    unk_user_id = 6814668400684
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/members/{unk_user_id}',
        token=coordinator_token)

    # then error is returned
    assert response.status_code == 404
    assert response.json is not None
    assert response.json['message'] == f"no user with user_id={unk_user_id}"

    # when a member tries to remove another member from a group
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/members/{coordinator.id}',
        token=some_token)

    # then action is refused
    assert response.status_code == 401
    assert response.json is not None
    assert response.json['message'] == 'shall act for self or be a coordinator'

    # when sole coordinator tries to remove themself from the group
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/members/{coordinator.id}',
        token=coordinator_token)

    # then action is refused
    assert response.status_code == 403
    assert response.json is not None
    assert response.json['message'] == 'sole coordinator'


def test_group_coordinators_listing(sample_backend: FlaskTestBackend):
    # given a backend with some groups
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator = sample_backend.users[0]
    # and a group with two  members
    some_user = sample_backend.users[1]
    some_token = sample_backend.tokens[1]
    ops.add_member(group=group, user=some_user)

    # when a member requests the list of group coordinators
    response = request(
        test_client.get,
        f'/api/groups/{group.id}/coordinators',
        token=some_token)

    # then coordinator is returned
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 coordinator'
    assert len(response.json['coordinators']) == 1
    assert response.json['coordinators'][0]['user_id'] == coordinator.id


def test_group_coordinators_addition(sample_backend: FlaskTestBackend):
    # given a backend with a group and another user
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coordinator_token = sample_backend.tokens[0]
    other_user = sample_backend.users[1]

    # when a coordinator requests addition of a new coordinator
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/coordinators',
        json=dict(user_id=other_user.id),
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'user added as coordinator'
    # and user is now a coordinator of the group
    assert other_user in group.coordinators
    # and group information about number of coordinators is updated
    response = request(
        test_client.get,
        f'/api/groups/{group.id}',
        token=coordinator_token)
    assert response.json is not None
    assert response.json['group']['coordinators_nb'] == 2

    # when the same request is made again
    response = request(
        test_client.post,
        f'/api/groups/{group.id}/coordinators',
        json=dict(user_id=other_user.id),
        token=coordinator_token)

    # then request is accepted
    assert response.status_code == 200
    # and message is adapted
    assert response.json is not None
    assert response.json['message'] == 'user is already a coordinator'


def test_group_coordinator_removal(sample_backend: FlaskTestBackend):
    # given a backend with a group and another user
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    coord_token = sample_backend.tokens[0]
    other_user = sample_backend.users[1]
    # and a group with two coordinators
    ops.add_coordinator(group=group, user=other_user)

    # when a coordinator is requested to be removed
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/coordinators/{other_user.id}',
        token=coord_token)

    # then request is accepted
    assert response.status_code == 200
    # and message is adapted
    assert response.json is not None
    assert response.json['message'] == 'user removed from group coordinators'

    # when same coordinator is requested to be removed, again
    response = request(
        test_client.delete,
        f'/api/groups/{group.id}/coordinators/{other_user.id}',
        token=coord_token)

    # then request is accepted, i.e. no error is raised (idempotence)
    assert response.status_code == 200
    # and message is adapted
    assert response.json is not None
    assert response.json['message'] == 'user is not a group coordinator'


def test_group_events(sample_backend: FlaskTestBackend):
    # given a backend with a group and an event
    test_client = sample_backend.test_client
    group = sample_backend.groups[0]
    event = sample_backend.events[0]

    # when group events are requested
    response = request(
        test_client.get,
        f'/api/groups/{group.id}/events')

    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 event'
    assert response.json['events'][0]['event_id'] == event.id


def test_group_events_with_privacy(
        sample_backend_with_privacy: FlaskTestBackend):
    # given a backend with a group and another user
    test_client = sample_backend_with_privacy.test_client
    group = sample_backend_with_privacy.groups[0]
    event = sample_backend_with_privacy.events[0]

    # when group events are requested for an event in a private group
    response = request(
        test_client.get,
        f'/api/groups/{group.id}/events')
    # then request fails
    assert response.status_code == 401

    # when request is performed with adequate token
    token = sample_backend_with_privacy.tokens[0]
    response = request(
        test_client.get,
        f'/api/groups/{group.id}/events',
        token=token)
    # then request succeeds
    assert response.status_code == 200
    assert response.json is not None
    assert response.json['message'] == 'found 1 event'
    assert response.json['events'][0]['event_id'] == event.id
