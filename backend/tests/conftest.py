from datetime import datetime, timedelta
from typing import NamedTuple

import pytest

from jegere.config import test_config
from jegere.domain.crypto import BcryptPasswordManager, PasswordManager, \
    TokenManager
from jegere.domain.model import Event, EventData, EventStatus, \
    Group, GroupData, User, UserData
from jegere.domain import ops

from jegere.service_layer import commands
from jegere.service_layer.handlers import users as user_handlers
from jegere.service_layer.unit_of_work import UnitOfWork
from jegere.adapters.repository import InMemoryRepository, AbstractRepository


class SampleUow(NamedTuple):
    uow: UnitOfWork
    users: list[User] = []
    users_data: list[UserData] = []
    groups: list[Group] = []
    groups_data: list[GroupData] = []
    events: list[Event] = []
    events_data: list[EventData] = []


SAMPLE_USERS_DATA: list[UserData] = [
    UserData('Sarah@gmail.com', 'X-x-'),
    UserData('Joe@mamadoo.fr', '4321_a'),
    UserData('Mélina@gmx.DE', 'Oo__oéfc'),
    UserData('Maurice@mm.net', '-_rcoifUUéc#'),
    UserData('pétunia@some.where', 'odifoécOUBO'),
]

SAMPLE_GROUPS_DATA: list[GroupData] = [
    GroupData('First Group'),
    GroupData('Together FTW'),
    GroupData('Here and There'),
    GroupData('Tout ici'),
    GroupData('Là-bas'),
]

SAMPLE_EVENTS_DATA: list[EventData] = [
    EventData(
        title='First Event', volunteering=1,
        start=datetime(2020, 11, 24, hour=15, minute=20),
        duration=timedelta(hours=1, minutes=32),
    ),
    EventData(
        title='Some other Event', volunteering=2,
        start=datetime(2020, 11, 23, hour=9, minute=5),
        duration=timedelta(hours=48, minutes=5),
    ),
    EventData(
        title='Gathering #3', volunteering=3,
        start=datetime(2020, 11, 28, hour=0, minute=0),
        duration=timedelta(hours=24, minutes=0),
    ),
    EventData(
        title='Christmas 2020 prep', volunteering=12,
        start=datetime(2020, 12, 23, hour=20, minute=0),
        duration=timedelta(hours=3, minutes=0),
        status=EventStatus.IN_PREPARATION,
    ),
]

BCRYPT_PASSWORD_MANAGER = BcryptPasswordManager(
    bcrypt_rounds=test_config.PASSWORDS_BCRYPT_ROUNDS)

TOKEN_MANAGER = TokenManager(
        secret_key=test_config.TOKEN_SECRET_KEY,
        validity=test_config.TOKEN_VALIDITY,
)


# ---- fixtures ----
@pytest.fixture
def users_dataset() -> list[UserData]:
    return SAMPLE_USERS_DATA


@pytest.fixture
def groups_dataset() -> list[GroupData]:
    return SAMPLE_GROUPS_DATA


@pytest.fixture
def events_dataset() -> list[EventData]:
    return SAMPLE_EVENTS_DATA


@pytest.fixture
def user_data() -> UserData:
    return SAMPLE_USERS_DATA[0]


@pytest.fixture
def user2_data() -> UserData:
    return SAMPLE_USERS_DATA[1]


@pytest.fixture
def group1_data() -> GroupData:
    return SAMPLE_GROUPS_DATA[0]


@pytest.fixture
def group2_data() -> GroupData:
    return SAMPLE_GROUPS_DATA[1]


@pytest.fixture
def event_data() -> EventData:
    return SAMPLE_EVENTS_DATA[0]


@pytest.fixture
def event2_data() -> EventData:
    return SAMPLE_EVENTS_DATA[1]


@pytest.fixture
def test_pw_manager() -> PasswordManager:
    return BCRYPT_PASSWORD_MANAGER


@pytest.fixture
def test_store() -> AbstractRepository:
    return InMemoryRepository()


@pytest.fixture
def test_token_manager() -> TokenManager:
    return TOKEN_MANAGER


@pytest.fixture
def token_manager_with_negative_validity():
    return TokenManager(
        secret_key=test_config.TOKEN_SECRET_KEY,
        validity=-1,  # never valid
    )


@pytest.fixture
def void_uow(
        test_store,
        test_pw_manager: PasswordManager,
        test_token_manager: TokenManager,
) -> SampleUow:
    return SampleUow(
        uow=UnitOfWork(
            store=test_store,
            pw_manager=test_pw_manager,
            token_manager=test_token_manager,
        ),
    )


@pytest.fixture
def sample_uow(
        void_uow: SampleUow,
        users_dataset: list[UserData],
        groups_dataset: list[GroupData],
        events_dataset: list[EventData],
) -> SampleUow:
    uow = void_uow.uow

    users: list[User] = []
    for user_data in users_dataset:
        users.append(ops.create_user(user_data))
        uow.store.store_user(user=users[-1])

    groups: list[Group] = []
    for idx, group_data in enumerate(groups_dataset):
        creator: User = users[idx % len(users)]
        groups.append(ops.create_group(creator=creator, data=group_data))
        uow.store.store_group(group=groups[-1])

    events: list[Event] = []
    for idx, event_data in enumerate(events_dataset):
        creator = users[idx % len(users)]
        group = groups[idx % len(groups)]
        events.append(
            ops.create_event(creator=creator, group=group, data=event_data))
        group.events.add(events[-1])
        uow.store.store_event(events[-1])

    return SampleUow(
        uow=uow,
        users=users, users_data=users_dataset,
        groups=groups, groups_data=groups_dataset,
        events=events, events_data=events_dataset,
    )


@pytest.fixture
def uow_with_one_user(
        void_uow: SampleUow,
        users_dataset: list[UserData],
) -> SampleUow:
    uow = void_uow.uow
    user_dataset = users_dataset[0:1]
    users: list[User] = []

    for user_data in users_dataset:
        user_id = user_handlers.create_user(
            commands.CreateUser(data=user_data), uow=uow
        ).user_id
        users.append(uow.store.get_user(user_id=user_id))

    return SampleUow(
        uow=uow,
        users=users,
        users_data=user_dataset,
    )
