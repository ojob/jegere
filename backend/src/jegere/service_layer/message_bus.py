import logging
from typing import Any, NamedTuple, Iterable, Union

from . import app_events, commands
from .handlers import Message, COMMANDS_HANDLERS, EVENTS_HANDLERS
from .unit_of_work import UnitOfWork


class Outcome(NamedTuple):
    results: list[Any]
    trace: list[Message]


def handle(
        message: Union[Message, Iterable[Message]],
        uow: UnitOfWork,
) -> Outcome:
    """Execution loop of the message bus.

    Once the given *message* (or sequence of messages) is executed, this takes
    care to see if some events have been raised and shall be executed as well.

    """
    results = []
    if isinstance(message, (commands.Command, app_events.AppEvent)):
        queue: list[Message] = [message]
    else:
        queue = list(message)
    trace: list[Message] = []
    while queue:
        message = queue.pop(0)
        trace.append(message)
        if isinstance(message, commands.Command):
            results.append(handle_command(message, uow))
        elif isinstance(message, app_events.AppEvent):
            handle_event(message, uow)
        else:  # pragma: no cover
            raise TypeError(
                f'unknown message type: {type(message)} ({message})')
        queue.extend(uow.collect_new_events())
    return Outcome(results, trace)


def handle_command(cmd: commands.Command, uow: UnitOfWork):
    handler = COMMANDS_HANDLERS[type(cmd)]
    logging.info('command %s start, handler %s', cmd, handler)
    return handler(cmd, uow)


def handle_event(ev: app_events.AppEvent, uow: UnitOfWork) -> None:
    # Depending on the event, there may be zero or more handlers:
    # - zero when no action is needed;
    # - one or more when several actions are needed.
    for handler in EVENTS_HANDLERS.get(type(ev), []):
        logging.info('event %s start, handler %s', ev, handler)
        try:
            handler(ev, uow)
        except Exception as exc:
            # Do not allow app_events to raise blocking exceptions; just capture
            # the message in the trace.
            uow.app_events.append(app_events.EventHandlerFailure(
                actor_id=ev.actor_id,
                msg=f'app event {ev} handler {handler} failed: {exc}'))
