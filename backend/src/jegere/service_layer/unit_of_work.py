from __future__ import annotations

from typing import Iterable

from ..domain import crypto
from ..adapters.repository import AbstractRepository
from . import app_events


class UnitOfWork:
    """Context manager to take care of data storage persistence commands.

    This context is needed to ensure consistency of changes to be persisted.

    """

    def __init__(
            self,
            store: AbstractRepository,
            pw_manager: crypto.PasswordManager,
            token_manager: crypto.TokenManager,
    ):
        self.app_events: list[app_events.AppEvent] = []
        self.store = store
        self.pw_manager = pw_manager
        self.token_manager = token_manager

    def collect_new_events(self) -> Iterable[app_events.AppEvent]:
        # unstack the events, in chronological order
        while self.app_events:
            yield self.app_events.pop(0)

    def __enter__(self) -> UnitOfWork:
        return self

    def __exit__(self, *args):
        self.rollback()

    def commit(self):
        pass

    def rollback(self):
        pass
