from dataclasses import dataclass
from typing import Optional

from ..domain import model, crypto


@dataclass
class Command:
    """Class to hold commands."""


# ---- user commands ------------------------------------------------------
@dataclass
class CreateUser(Command):
    data: model.UserData


@dataclass
class LogUserIn(Command):
    email: str
    password: str


@dataclass
class LogUserOut(Command):
    token: crypto.Token


@dataclass
class GetUserPublicDetails(Command):
    user_id: Optional[int] = None
    email: Optional[str] = None


@dataclass
class GetUserPrivateDetails(Command):
    token: crypto.Token


@dataclass
class GetUsers(Command):
    actor_id: int


# ---- token commands -----------------------------------------------------
@dataclass
class ValidateToken(Command):
    token: crypto.Token


@dataclass
class DecodeToken(Command):
    token: crypto.Token


# ---- group commands -----------------------------------------------------
@dataclass
class CreateGroup(Command):
    actor_id: int
    data: model.GroupData


@dataclass
class GetGroupDetails(Command):
    actor_id: Optional[int] = None
    group_id: Optional[int] = None
    name: Optional[str] = None


@dataclass
class GetGroupsDetails(Command):
    actor_id: Optional[int] = None


@dataclass
class GetUserGroups(Command):
    actor_id: int


@dataclass
class FindGroup(Command):
    actor_id: Optional[int] = None
    group_id: Optional[int] = None
    name: Optional[str] = None


@dataclass
class UpdateGroup(Command):
    actor_id: int
    group_id: int
    data: model.GroupData


@dataclass
class RemoveGroup(Command):
    actor_id: int
    group_id: int


@dataclass
class GetGroupPrivateDetails(Command):
    group_id: int
    actor_id: Optional[int] = None


@dataclass
class SetGroupVisibility(Command):
    actor_id: int
    group_id: int
    public: bool


@dataclass
class AddMember(Command):
    actor_id: int
    group_id: int
    user_id: int


@dataclass
class GetMembers(Command):
    actor_id: int
    group_id: int


@dataclass
class RemoveMember(Command):
    actor_id: int
    group_id: int
    user_id: int


@dataclass
class AddCoordinator(Command):
    actor_id: int
    group_id: int
    user_id: int


@dataclass
class GetCoordinators(Command):
    actor_id: int
    group_id: int


@dataclass
class RemoveCoordinator(Command):
    actor_id: int
    group_id: int
    user_id: int


@dataclass
class GetGroupEvents(Command):
    group_id: int
    actor_id: Optional[int] = None


# ---- event commands -----------------------------------------------------
@dataclass
class CreateEvent(Command):
    actor_id: int
    group_id: int
    data: model.EventData


@dataclass
class GetEventDetails(Command):
    event_id: int
    actor_id: Optional[int] = None


@dataclass
class GetAllEvents(Command):
    actor_id: Optional[int] = None


@dataclass
class UpdateEvent(Command):
    actor_id: int
    event_id: int
    data: model.EventData


@dataclass
class DeleteEvent(Command):
    actor_id: int
    event_id: int


@dataclass
class ChangeEventGroup(Command):
    actor_id: int
    event_id: int
    new_group_id: int


@dataclass
class SetEventParticipation(Command):
    actor_id: int
    event_id: int
    participation: str
    participant_id: Optional[int] = None


@dataclass
class GetEventParticipants(Command):
    event_id: int
    actor_id: int
    participation: str
