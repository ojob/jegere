"""This module provides the mapping from commands and events to handlers.

"""
from typing import Union, Type, Callable

from ...service_layer import app_events, commands
from . import events, groups, users

Message = Union[commands.Command, app_events.AppEvent]
Handler = Callable

# Note that the keys are class objects, not instances
COMMANDS_HANDLERS: dict[Type[commands.Command], Handler] = {
    # user handlers
    commands.CreateUser: users.create_user,
    commands.GetUserPublicDetails: users.get_user_public_details,
    commands.GetUserPrivateDetails: users.get_user_private_details,
    commands.LogUserIn: users.log_user_in,
    commands.LogUserOut: users.log_user_out,
    commands.GetUsers: users.get_users,

    # token handlers
    commands.ValidateToken: users.validate_token,
    commands.DecodeToken: users.decode_token,

    # group handlers
    commands.CreateGroup: groups.create_group,
    commands.GetGroupDetails: groups.get_group_public_details,
    commands.GetGroupsDetails: groups.get_groups_public_details,
    commands.GetUserGroups: groups.get_user_groups_details,
    commands.FindGroup: groups.find_group,
    commands.UpdateGroup: groups.update_group,
    commands.RemoveGroup: groups.remove_group,
    commands.GetGroupPrivateDetails: groups.get_group_private_details,
    commands.SetGroupVisibility: groups.set_group_visibility,

    # group memberships and users rights handlers
    commands.AddMember: groups.add_member,
    commands.GetMembers: groups.get_members,
    commands.RemoveMember: groups.remove_member,
    commands.AddCoordinator: groups.add_coordinator,
    commands.GetCoordinators: groups.get_coordinators,
    commands.RemoveCoordinator: groups.remove_coordinator,

    # group events handlers
    commands.GetGroupEvents: groups.get_group_events,

    # event handlers
    commands.CreateEvent: events.create_event,
    commands.GetEventDetails: events.get_event_details,
    commands.GetAllEvents: events.get_all_events,
    commands.UpdateEvent: events.update_event,
    commands.DeleteEvent: events.remove_event,
    commands.ChangeEventGroup: events.change_event_group,

    # event participants handlers
    commands.SetEventParticipation: events.set_event_participation,
    commands.GetEventParticipants: events.get_event_participants,
}

# Note that the keys are class objects, not instances
EVENTS_HANDLERS: dict[Type[app_events.AppEvent], list[Handler]] = {
    app_events.GroupRemoved: [
        groups.unlink_members,
        groups.drop_linked_events],
}
