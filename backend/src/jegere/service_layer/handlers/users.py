from typing import NamedTuple


from ...domain import crypto, exc, model, ops
from .. import app_events, commands
from ..unit_of_work import UnitOfWork


class UserLoggedInDetails(NamedTuple):
    user_id: int
    token: crypto.Token


def create_user(
        cmd: commands.CreateUser, uow: UnitOfWork,
) -> app_events.UserCreated:
    """Handle user creation.

    Event raised: UserCreated.

    """
    model.check_credentials(email=cmd.data.email, password=cmd.data.password)
    with uow:
        if uow.store.find_user(email=cmd.data.email) is not None:
            raise exc.EmailAlreadyRegistered(
                f"email already registered: '{cmd.data.email}'")

        user: model.User = ops.create_user(model.UserData(
            email=cmd.data.email,
            password=uow.pw_manager.hash(cmd.data.password),
        ))
        token: crypto.Token = uow.token_manager.encode(user.id)

        uow.store.store_user(user=user)
        uow.store.store_token(user_id=user.id, token=token)
        uow.commit()

        ev = app_events.UserCreated(
            actor_id=user.id, user_id=user.id, token=token, msg=None)
        uow.app_events.append(ev)
        return ev


def log_user_in(
        cmd: commands.LogUserIn, uow: UnitOfWork,
) -> app_events.TokenCreated:
    model.check_credentials(email=cmd.email, password=cmd.password)
    with uow:
        try:
            user: model.User = uow.store.get_user(email=cmd.email)
            uow.pw_manager.check(
                candidate_password=cmd.password, password_hash=user.password)
        except (exc.UserNotFound, exc.WrongPassword):
            raise exc.AuthenticationFailed("unknown user or wrong password")

        token: crypto.Token = uow.token_manager.encode(user.id)
        uow.store.store_token(user_id=user.id, token=token)
        uow.commit()

        ev = app_events.TokenCreated(
            actor_id=user.id, user_id=user.id, token=token, msg=None)
        uow.app_events.append(ev)
        return ev


def log_user_out(cmd: commands.LogUserOut, uow: UnitOfWork) -> None:
    with uow:
        uow.store.blacklist(token=cmd.token)
        uow.commit()
        uow.app_events.append(app_events.TokenBlacklisted(
            actor_id=-1, token=cmd.token, msg=None))


def get_user_public_details(
        cmd: commands.GetUserPublicDetails, uow: UnitOfWork,
) -> dict:
    with uow:
        user = uow.store.get_user(user_id=cmd.user_id, email=cmd.email)
        return user.public_details()


def get_user_private_details(
        cmd: commands.GetUserPrivateDetails, uow: UnitOfWork,
) -> dict:
    actor_id: int = uow.token_manager.decode(cmd.token)
    with uow:
        try:
            actor = uow.store.get_user(user_id=actor_id)
        except exc.UserNotFound:
            raise exc.NotAuthorized('no user matching provided token')
        return actor.private_details()


def get_users(cmd: commands.GetUsers, uow: UnitOfWork) -> list[dict]:
    with uow:
        # check that actor_id matches a real user
        if uow.store.find_user(user_id=cmd.actor_id) is None:
            raise exc.NotAuthorized("accessible only to users")
        return [user.public_details() for user in uow.store.users]


def validate_token(cmd: commands.ValidateToken, uow: UnitOfWork) -> None:
    with uow:
        if uow.store.is_blacklisted(cmd.token):
            raise exc.BlacklistedToken("blacklisted token, log in again")
        uow.token_manager.decode(token=cmd.token)


def decode_token(cmd: commands.DecodeToken, uow: UnitOfWork) -> int:
    return uow.token_manager.decode(token=cmd.token)
