from typing import Optional

from ...domain import exc, model, ops
from .. import app_events, commands
from ..unit_of_work import UnitOfWork
from .events import remove_event


def create_group(
        cmd: commands.CreateGroup, uow: UnitOfWork,
) -> app_events.GroupCreated:
    with uow:
        if uow.store.find_group(name=cmd.data.name) is not None:
            raise exc.GroupNameAlreadyTaken(
                f"group name already taken: '{cmd.data.name}")
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group = ops.create_group(creator=actor, data=cmd.data)
        uow.store.store_group(group)
        uow.commit()

        ev = app_events.GroupCreated(
            actor_id=cmd.actor_id, group_id=group.id, msg=None)
        uow.app_events.append(ev)
        return ev


def find_group(
        cmd: commands.FindGroup, uow: UnitOfWork,
) -> Optional[model.Group]:
    with uow:
        group: Optional[model.Group] = \
            uow.store.find_group(name=cmd.name, group_id=cmd.group_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)
        if group is not None:
            model.assert_is_visible(group=group, user=actor)
        return group


def update_group(
        cmd: commands.UpdateGroup, uow: UnitOfWork,
) -> app_events.GroupUpdated:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)

        model.assert_is_coordinator(user=actor, group=group)
        updates: list[ops.FieldUpdate] = \
            ops.update_group(group=group, data=cmd.data)
        ev = app_events.GroupUpdated(
            actor_id=cmd.actor_id, group_id=group.id, updates=updates, msg=None)
        uow.store.update_group(group=group, ev=ev)
        uow.commit()

        uow.app_events.append(ev)
        return ev


def set_group_visibility(
        cmd: commands.SetGroupVisibility, uow: UnitOfWork,
) -> None:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_coordinator(group=group, user=actor)
        group.is_public = cmd.public
        uow.commit()


def remove_group(cmd: commands.RemoveGroup, uow: UnitOfWork) -> None:
    """

    Event raised: GroupDeleted

    """
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)

        model.assert_is_coordinator(user=actor, group=group)
        uow.store.remove_group(group=group)
        uow.commit()

        uow.app_events.append(app_events.GroupRemoved(
            group=group, actor_id=cmd.actor_id, msg=None))


def unlink_members(ev: app_events.GroupRemoved, uow: UnitOfWork) -> None:
    """React to group deletion by removal of links to users.

    Event raised: MembershipRemoved

    """
    with uow:
        for user in ev.group.members:
            user.groups.remove(ev.group)
        uow.commit()


def drop_linked_events(ev: app_events.GroupRemoved, uow: UnitOfWork) -> None:
    """React to group deletion by deletion of linked events.

    Event raised: EventDeleted

    """
    for event in list(ev.group.events):
        remove_event(commands.DeleteEvent(
            event_id=event.id, actor_id=ev.actor_id),
            uow=uow)


def get_group_public_details(
        cmd: commands.GetGroupDetails, uow: UnitOfWork,
) -> dict:
    with uow:
        group: model.Group = uow.store.get_group(
            group_id=cmd.group_id, name=cmd.name)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_visible(group=group, user=actor)
        return group.public_details()


def get_group_private_details(
        cmd: commands.GetGroupPrivateDetails, uow: UnitOfWork,
) -> dict:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_visible(user=actor, group=group)
        auth_info: dict[str, bool] = {}
        if actor is not None:
            auth_info = dict(
                is_member=actor in group.members,
                is_coordinator=actor in group.coordinators,
            )
        return {**group.private_details(), **auth_info}


def get_groups_public_details(
        cmd: commands.GetGroupsDetails, uow: UnitOfWork,
) -> dict[int, dict]:
    with uow:
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)
        groups = (group for group in uow.store.groups
                  if group.is_visible(user=actor))
        return {group.id: group.public_details() for group in groups}


def get_user_groups_details(
        cmd: commands.GetUserGroups, uow: UnitOfWork,
) -> dict[int, dict]:
    with uow:
        actor = uow.store.get_user(user_id=cmd.actor_id)
        # FIXME: actor.groups seems not correctly updated, as following line
        #  does not return all user groups
        # return {group.id: group.private_details() for group in actor.groups}
        return {group.id: group.private_details()
                for group in uow.store.groups
                if actor in group.members}


def get_group_events(
        cmd: commands.GetGroupEvents, uow: UnitOfWork,
) -> dict[int, dict]:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_visible(group=group, user=actor)
        return {event.id: {'title': event.title}
                for event in group.events
                if event.is_visible(user=actor)}


def add_member(cmd: commands.AddMember, uow: UnitOfWork) -> str:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        user: model.User = uow.store.get_user(user_id=cmd.user_id)
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_coordinator(user=actor, group=group)
        if user not in group.members:
            msg: str = ops.add_member(group=group, user=user)
            uow.commit()
            uow.app_events.append(app_events.MemberAdded(
                actor_id=cmd.actor_id, group=group, user=user, msg=msg))
        else:
            msg = 'user is already a member'
        return msg


def get_members(cmd: commands.GetMembers, uow: UnitOfWork) -> dict[int, dict]:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_member(group=group, user=actor)
        return {
            member.id: {
                'is_coordinator': member in group.coordinators,
                **member.public_details()}
            for member in group.members}


def remove_member(cmd: commands.RemoveMember, uow: UnitOfWork) -> str:
    with uow:
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        user: model.User = uow.store.get_user(user_id=cmd.user_id)
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_self_or_coordinator(actor=actor, group=group, user=user)

        was_coord = user in group.coordinators
        if was_coord:
            msg0: str = ops.remove_coordinator(group=group, user=user)
            uow.app_events.append(app_events.CoordinatorRemoved(
                actor_id=cmd.actor_id, group=group, user=user, msg=msg0))

        msg1: str = ops.remove_member(group=group, user=user)
        ev = app_events.MemberRemoved(
            actor_id=cmd.actor_id, group=group, user=user, msg=msg1,
            was_coord=was_coord)
        uow.app_events.append(ev)

        uow.commit()
        return msg1


def add_coordinator(cmd: commands.AddCoordinator, uow: UnitOfWork) -> str:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        new_coord: model.User = uow.store.get_user(user_id=cmd.user_id)

        model.assert_is_coordinator(group=group, user=actor)

        was_member = new_coord in group.members
        if not was_member:
            msg0: str = ops.add_member(group=group, user=new_coord)
            uow.commit()
            uow.app_events.append(app_events.MemberAdded(
                actor_id=cmd.actor_id, group=group, user=new_coord, msg=msg0))

        if new_coord not in group.coordinators:
            msg1: str = ops.add_coordinator(group=group, user=new_coord)
            uow.commit()
            uow.app_events.append(app_events.CoordinatorAdded(
                actor_id=cmd.actor_id, group=group, user=actor,
                was_member=was_member, msg=msg1))
        else:
            msg1 = 'user is already a coordinator'
        return msg1


def get_coordinators(
        cmd: commands.GetCoordinators, uow: UnitOfWork,
) -> dict[int, dict]:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)

        model.assert_is_member(user=actor, group=group)
        return {coord.id: coord.public_details()
                for coord in group.coordinators}


def remove_coordinator(
        cmd: commands.RemoveCoordinator, uow: UnitOfWork,
) -> str:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)
        user: model.User = uow.store.get_user(user_id=cmd.user_id)

        model.assert_is_coordinator(user=actor, group=group)
        if user in group.coordinators:
            msg = ops.remove_coordinator(user=user, group=group)
            uow.app_events.append(app_events.CoordinatorRemoved(
                actor_id=cmd.actor_id, group=group, user=user, msg=msg))
            uow.commit()
        else:
            msg = 'user is not a group coordinator'
        return msg
