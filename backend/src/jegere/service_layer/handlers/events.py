from typing import Optional

from ...domain import exc, model, ops
from .. import app_events, commands
from ..unit_of_work import UnitOfWork


def create_event(cmd: commands.CreateEvent, uow: UnitOfWork) -> int:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        group: model.Group = uow.store.get_group(group_id=cmd.group_id)

        model.assert_is_member(user=actor, group=group)
        event = ops.create_event(creator=actor, group=group, data=cmd.data)
        uow.store.store_event(event=event)
        uow.commit()

        ev = app_events.EventCreated(
            actor_id=cmd.actor_id, event_id=event.id, msg=None)
        uow.app_events.append(ev)
        return event.id


def get_event_details(cmd: commands.GetEventDetails, uow: UnitOfWork) -> dict:
    with uow:
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)

        model.assert_is_visible(user=actor, event=event)
        event_details = event.private_details()
        return event_details


def get_all_events(cmd: commands.GetAllEvents, uow: UnitOfWork) -> list[dict]:
    with uow:
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)
        return [event.public_details()
                for event in uow.store.events
                if event.is_visible(user=actor)]


def update_event(
        cmd: commands.UpdateEvent, uow: UnitOfWork,
) -> app_events.EventUpdated:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)

        model.assert_is_member(user=actor, event=event)
        updates: list[ops.FieldUpdate] = \
            ops.update_event(event=event, data=cmd.data)
        ev = app_events.EventUpdated(
            actor_id=cmd.actor_id, event_id=event.id, updates=updates, msg=None)
        uow.store.update_event(event=event, ev=ev)
        uow.commit()

        uow.app_events.append(ev)
        return ev


def remove_event(
        cmd: commands.DeleteEvent, uow: UnitOfWork,
) -> app_events.EventRemoved:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)

        model.assert_is_coordinator(user=actor, group=event.group)
        ops.delete_event(event=event)
        uow.store.remove_event(event=event)
        uow.commit()

        ev = app_events.EventRemoved(
            actor_id=cmd.actor_id, event=event, msg=None)
        uow.app_events.append(ev)
        return ev


def change_event_group(
        cmd: commands.ChangeEventGroup, uow: UnitOfWork,
) -> app_events.EventGroupChanged:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)
        old_group: model.Group = event.group
        new_group: model.Group = uow.store.get_group(group_id=cmd.new_group_id)

        model.assert_is_member(user=actor, group=old_group)
        if actor not in new_group.members:
            raise exc.NotAllowed('user shall be a member of destination group')
        ops.change_event_group(event=event, new_group=new_group)
        uow.commit()

        ev = app_events.EventGroupChanged(
            actor_id=cmd.actor_id, item_id=event.id, old_ref=old_group.id,
            new_ref=new_group.id, msg=None)
        uow.app_events.append(ev)
        return ev


def set_event_participation(
        cmd: commands.SetEventParticipation, uow: UnitOfWork,
) -> str:
    with uow:
        actor: model.User = uow.store.get_user(user_id=cmd.actor_id)
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)
        if cmd.participant_id is None:
            participant: model.User = actor
        else:
            participant = uow.store.get_user(user_id=cmd.participant_id)

        if cmd.participation == 'no-answers':
            participation: Optional[model.Participation] = None
        else:
            p = cmd.participation[:-1]  # harmonizing plurals
            try:
                participation = model.participation_from_str(p)
            except ValueError:
                raise exc.InvalidData(
                    f"unknown participation query: '{cmd.participation}'")

        model.assert_is_member(user=actor, group=event.group)
        model.assert_is_self_or_coordinator(
            actor=actor, user=participant, group=event.group)
        msg: str = ops.set_participation(
            event=event, participant=participant, participation=participation)

        uow.commit()
        uow.app_events.append(app_events.ParticipationUpdated(
            actor_id=cmd.actor_id, event=event, user=participant,
            participation=participation, msg=msg))
        return msg


def get_event_participants(
        cmd: commands.GetEventParticipants, uow: UnitOfWork,
) -> list[model.User]:
    with uow:
        event: model.Event = uow.store.get_event(event_id=cmd.event_id)
        actor: Optional[model.User] = None
        if cmd.actor_id is not None:
            actor = uow.store.get_user(user_id=cmd.actor_id)
        participation: Optional[str] = None
        if cmd.participation != 'no-answers':
            participation = cmd.participation.replace('-', '')

        model.assert_is_visible(event=event, user=actor)
        users: list[model.User] = ops.get_participants(
            event=event, participation=participation)
        return users
