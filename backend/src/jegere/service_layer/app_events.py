from dataclasses import dataclass
from typing import Optional

from ..domain import crypto, model, ops


@dataclass
class AppEvent:
    """For events raised by the app."""
    actor_id: int
    msg: Optional[str]


# ---- creation events ----------------------------------------------------
class ItemCreated(AppEvent):
    """For item storage."""


@dataclass
class UserCreated(ItemCreated):
    user_id: int
    token: crypto.Token


@dataclass
class TokenCreated(ItemCreated):
    user_id: int
    token: crypto.Token


@dataclass
class GroupCreated(ItemCreated):
    group_id: int


@dataclass
class EventCreated(ItemCreated):
    event_id: int


# ---- update events ------------------------------------------------------
@dataclass
class ItemUpdated(AppEvent):
    updates: list[ops.FieldUpdate]


@dataclass
class GroupUpdated(ItemUpdated):
    """For group updates."""
    group_id: int


@dataclass
class EventUpdated(ItemUpdated):
    """For event updates."""
    event_id: int


@dataclass
class MemberAdded(AppEvent):
    group: model.Group
    user: model.User


@dataclass
class CoordinatorAdded(AppEvent):
    group: model.Group
    user: model.User
    was_member: bool


@dataclass
class TokenBlacklisted(AppEvent):
    token: crypto.Token


@dataclass
class ParticipationUpdated(AppEvent):
    user: model.User
    event: model.Event
    participation: Optional[model.Participation]


# ---- references changes -------------------------------------------------
@dataclass
class ReferenceChanged(AppEvent):
    item_id: int
    old_ref: int
    new_ref: int


@dataclass
class EventGroupChanged(ReferenceChanged):
    """For change of event group."""


# ---- deletion events ----------------------------------------------------
@dataclass
class ItemRemoved(AppEvent):
    """For item removal from storage."""


@dataclass
class UserRemoved(ItemRemoved):
    user: model.User


@dataclass
class GroupRemoved(ItemRemoved):
    """For group removal from storage."""
    group: model.Group


@dataclass
class EventRemoved(ItemRemoved):
    """For event removal from storage."""
    event: model.Event


@dataclass
class MemberRemoved(ItemRemoved):
    group: model.Group
    user: model.User
    was_coord: bool


@dataclass
class CoordinatorRemoved(ItemRemoved):
    group: model.Group
    user: model.User


# ---- other type of events -----------------------------------------------
class EventHandlerFailure(AppEvent):
    """When an event handler failed."""
