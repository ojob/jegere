"""Jegere back-end package.

Everything resides in :py:mod:`bootstrap.py` module: creation of the backend
stuff and configuration happens there.

You may launch the backend by calling :py:func:`bootstrap.run()`.

Configuration is defined in :py:mode:`config.py`. Importing this module will
interrogate environment variables though: this is a side-effect to think about.

"""
