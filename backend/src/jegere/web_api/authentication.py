"""This module handles the HTTP authentication concerns."""
import functools
from typing import Optional, overload

from flask import jsonify, request

from ..domain.crypto import Token
from ..domain.exc import NotAuthorized, TokenError
from ..service_layer import commands, message_bus as bus
from ..service_layer.unit_of_work import UnitOfWork
from . import exc as web_exc


class AuthorizationError(Exception):
    """To indicate a missing or wrongly formatted header"""


def extract_token() -> Token:
    """Extract the token from current HTTP request.

    Raises:
        AuthorizationError: if token cannot be extracted.

    """
    try:
        auth_header = request.headers['Authorization']
    except KeyError:
        raise AuthorizationError(
            "found no Authorization header, while required for this resource")
    if not auth_header:
        raise AuthorizationError(
            "found empty Authorization header; shall be like 'Bearer <token>'")
    if not auth_header.startswith('Bearer '):
        raise AuthorizationError(
            "Authorization header format error; shall be like 'Bearer <token>'")
    token: Token = auth_header[len('Bearer '):]
    return token


class AuthService:
    """Class to handle tokens management with regards to web API."""

    def __init__(self, uow: UnitOfWork):
        self.uow = uow

    def _handle(self, cmd: commands.Command):
        return bus.handle(cmd, uow=self.uow)

    def inject_token(self, func, required: bool = True):
        """Decorate *func* and injects the token from current request.

        Args:
            func: Flask view method to decorate
            required: if `True` (default), on token extraction failure *func* is
                not called and Error `401` is returned;
                if `False`,  *func* is called with `token=None`.

        Returns:
            decorated function.

        """
        def wrapped_func(*args, **kwargs):
            token: Optional[Token] = None
            payload: Optional[dict] = None
            try:
                token = extract_token()
                # and check that the token is valid and not blacklisted
                self._handle(cmd=commands.ValidateToken(token=token))
            except AuthorizationError as exc:
                payload = dict(message=str(exc))
            except TokenError as exc:
                payload = dict(
                    message=str(exc),
                    link='/api/users/login',
                )
            if payload and required:  # failure encountered
                return jsonify(payload), 401  # Unauthorized
            return func(*args, token=token, **kwargs)

        functools.update_wrapper(wrapped_func, func)
        return wrapped_func

    def inject_actor_id(self, func, required: bool = True):
        """Decorate *func* and injects the acting user id from current request.

        The decorated function will raise appropriate exceptions when the
        authentication fails.

        Args:
            func: Flask view method to decorate
            required: if `True` (default), if token decoding does not work,
                *func* is not called and Error `401` is returned;
                if `False`, *func* is  called with `actor_id=None`.

        Returns:
            decorated function.

        """
        def wrapped_func(token: Optional[Token], *args, **kwargs):
            if token is None and required:
                raise NotAuthorized(
                    'accessible to authenticated users only')

            actor_id: Optional[int] = None
            if token is not None:
                cmd = commands.DecodeToken(token=token)
                try:
                    actor_id = self._handle(cmd).results[0]
                except TokenError as exc:
                    payload = dict(
                        message=str(exc),
                        link='/api/users/login',
                    )
                    if required:
                        return jsonify(payload), 400

            return func(*args, actor_id=actor_id, **kwargs)

        # taking care to inject the actor_id, using relevant decorator
        wrapped_func = self.inject_token(func=wrapped_func, required=required)

        functools.update_wrapper(wrapped_func, func)
        return wrapped_func


@overload
def extract_user_id(required: bool) -> Optional[int]:  # pragma: no cover
    ...


@overload
def extract_user_id() -> int:  # pragma: no cover
    ...


def extract_user_id(required=True, _request=request):
    """Extract user id from Flask request.

    Args:
        required: if `True`, raise RequestParseError if `user_id` is not in
            request body; otherwise, just return `None`.
        _request: request to parse. This points by default to Flask `request`
            and shall be changed only for testing purpose.

    >>> from dataclasses import dataclass
    >>> @dataclass
    ... class MyReq:
    ...     json: dict = None
    ...
    >>> extract_user_id(_request=MyReq())  # with no `json` content
    Traceback (most recent call last):
    ...
    jegere.web_api.exc.RequestParseError: user_id missing in request

    >>> extract_user_id(_request=MyReq(json={'user_id': 'prem'}))  # not an int
    Traceback (most recent call last):
    ...
    jegere.web_api.exc.RequestParseError: cannot use provided user_id: 'prem'

    """
    try:
        raw_user_id: str = _request.json['user_id']
    except (KeyError, TypeError):
        # TypeError is raised if `request.json` is None, i.e. no request body.
        # KeyError is raised if `'user_id'` is not in `request.json`.
        if required:
            raise web_exc.RequestParseError("user_id missing in request")
        return None
    try:
        return int(raw_user_id)
    except (TypeError, ValueError):
        raise web_exc.RequestParseError(
            f"cannot use provided user_id: '{raw_user_id}'")
