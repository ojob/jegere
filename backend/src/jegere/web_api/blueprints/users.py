from typing import Any, Optional

from flask import Blueprint, jsonify, request

from ...domain.crypto import Token
from ...domain.model import UserData
from ...service_layer.unit_of_work import UnitOfWork
from ...service_layer import app_events, commands, message_bus as bus
from .. import FlaskAnswer
from .. import exc as web_exc
from ..authentication import AuthService
from ..utils import counts


class UsersAPI:
    """Class to handle lifecycle of :py:class:`User` and related tokens.

    The tokens handling is performed by :py:class:`TokenManager` instance,
    managed by the same repository.

    """

    def __init__(self, uow: UnitOfWork):
        self.uow = uow

    def register(self) -> FlaskAnswer:
        cmd = commands.CreateUser(data=extract_user_data())
        ev: app_events.UserCreated = bus.handle(cmd, uow=self.uow).results[0]
        user_id, token = ev.user_id, ev.token
        payload = dict(
            message="new user created",
            user_id=user_id,
            token=token,
            link=f'/api/users/{user_id}',  # TODO: need to use url_for
        )
        return jsonify(payload), 201

    def login(self) -> FlaskAnswer:
        cmd = commands.LogUserIn(**extract_credentials())
        ev: app_events.TokenCreated = bus.handle(cmd, uow=self.uow).results[0]
        user_id, token = ev.user_id, ev.token
        payload = dict(
            message="user logged in",
            user=dict(
                user_id=user_id,
                link=f'/api/users/{user_id}',
            ),
            token=token,
        )
        return jsonify(payload), 200

    def logout(self, token: Token) -> FlaskAnswer:
        cmd = commands.LogUserOut(token=token)
        bus.handle(cmd, uow=self.uow)
        payload = dict(
            message="user logged out for this token",
        )
        return jsonify(payload), 200

    def user_details(self, user_id: int) -> FlaskAnswer:
        """Return user public details, from their ID."""
        cmd = commands.GetUserPublicDetails(user_id=user_id)
        public_details = bus.handle(cmd, uow=self.uow).results[0]
        payload = dict(
            message=f"details of user with id: {public_details['user_id']}",
            user=public_details,
        )
        return jsonify(payload), 200

    def current_user_details(self, token: Token) -> FlaskAnswer:
        """Return user private details."""
        cmd = commands.GetUserPrivateDetails(token=token)
        private_details = bus.handle(cmd, uow=self.uow).results[0]
        payload = dict(
            message=f"details of user with id: {private_details['user_id']}",
            user=private_details,
        )
        return jsonify(payload), 200

    def users(self, actor_id: int) -> FlaskAnswer:
        cmd = commands.GetUsers(actor_id=actor_id)
        users_details: list[dict] = bus.handle(cmd, uow=self.uow).results[0]
        payload = dict(
            message=counts(len(users_details), 'user', start='found'),
            users=[{'link': f'/api/users/{details["user_id"]}', **details}
                   for details in users_details],
        )
        return jsonify(payload), 200


def create_users_blueprint(auth: AuthService, uow: UnitOfWork) -> Blueprint:
    """Link the :py:class:`UsersAPI` to Flask endpoints."""
    users_blp = Blueprint('users', __name__)
    users_api = UsersAPI(uow=uow)

    users_blp.add_url_rule(
        '/register',
        view_func=users_api.register,
        methods=['POST'],
    )
    users_blp.add_url_rule(
        '/login',
        view_func=users_api.login,
        methods=['POST'],
    )
    users_blp.add_url_rule(
        '/logout',
        view_func=auth.inject_token(users_api.logout),
        methods=['POST'],
    )
    users_blp.add_url_rule(
        '/<int:user_id>',
        view_func=users_api.user_details,
    )
    users_blp.add_url_rule(
        '/me',
        view_func=auth.inject_token(users_api.current_user_details),
    )
    users_blp.add_url_rule(
        '/all',
        view_func=auth.inject_actor_id(users_api.users),
    )

    return users_blp


def extract_user_data() -> UserData:
    """Extract user data from Flask request."""
    json: Optional[Any] = request.json
    if json is None:
        raise web_exc.RequestParseError('request is not in JSON format')
    try:
        return UserData(
            email=json['email'],
            password=json['password'],
        )
    except (KeyError, TypeError) as exc:
        raise web_exc.RequestParseError(f'missing field in request: {exc}')


def extract_credentials() -> dict[str, str]:
    """Extract user credentials from Flask request."""
    json: Optional[Any] = request.json
    if json is None:
        raise web_exc.RequestParseError('request is not in JSON format')
    try:
        return dict(
            email=json['email'],
            password=json['password'],
        )
    except (KeyError, TypeError):
        raise web_exc.CredentialsParseError(
            "email and/or password missing in request")
