from datetime import datetime
from collections.abc import Callable
from typing import Any, Optional

from flask import Blueprint, jsonify, request

from ...domain.model import EventData, User, event_status_from_str
from ...domain.utils import hm_from_timedelta, timedelta_from_hms
from ...service_layer import app_events, commands, message_bus as bus
from ...service_layer.unit_of_work import UnitOfWork
from .. import FlaskAnswer
from .. import exc as web_exc
from ..authentication import AuthService, extract_user_id
from ..utils import counts


class EventsAPI:
    """Class to handle Flask requests and responses for :py:class:`Events`."""

    def __init__(self, uow: UnitOfWork):
        self.uow = uow

    def _handle(self, cmd: commands.Command):
        return bus.handle(cmd, uow=self.uow)

    def create(self, actor_id: int) -> FlaskAnswer:
        cmd = commands.CreateEvent(
            actor_id=actor_id,
            group_id=extract_group_id(),
            data=extract_event_data())
        event_id: int = self._handle(cmd).results[0]
        payload = dict(
            message='new event created',
            event=dict(event_id=event_id, link=f'/api/events/{event_id}'),
        )
        return jsonify(payload), 201  # created

    def details(self, event_id: int, actor_id: int) -> FlaskAnswer:
        cmd = commands.GetEventDetails(event_id=event_id, actor_id=actor_id)
        event_details = self._handle(cmd).results[0]
        payload = dict(
            message='event found',
            event={
                'group_link': f"/api/groups/{event_details['group_id']}",
                'creator_link': f"/api/users/{event_details['creator_id']}",
                **serialize(event_details),
            },
        )
        return jsonify(payload), 200

    def all_events(self, actor_id: int = None) -> FlaskAnswer:
        cmd = commands.GetAllEvents(actor_id=actor_id)
        events_details = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(events_details), 'event', start='found'),
            events=[
                {'link': f"/api/events/{details['event_id']}",
                 'group_link': f"/api/groups/{details['group_id']}",
                 **serialize(details)}
                for details in events_details],
        )
        return jsonify(payload), 200

    def update(self, actor_id: int, event_id: int) -> FlaskAnswer:
        cmd = commands.UpdateEvent(
            actor_id=actor_id, event_id=event_id, data=extract_event_data())
        app_ev: app_events.EventUpdated = self._handle(cmd).results[0]
        payload = dict(
            message=f'event {event_id} updated: '
                    + ', '.join(update.field for update in app_ev.updates),
        )
        return jsonify(payload), 200

    def change_group(self, actor_id: int, event_id: int) -> FlaskAnswer:
        cmd = commands.ChangeEventGroup(
            actor_id=actor_id, event_id=event_id,
            new_group_id=extract_group_id())
        app_ev: app_events.EventGroupChanged = self._handle(cmd).results[0]
        payload = dict(
            message=f'event group changed, from group {app_ev.old_ref} '
                    f'to group {app_ev.new_ref}',
        )
        return jsonify(payload), 200

    def delete(self, actor_id: int, event_id: int) -> FlaskAnswer:
        cmd = commands.DeleteEvent(actor_id=actor_id, event_id=event_id)
        app_ev: app_events.EventRemoved = self._handle(cmd).results[0]
        payload = dict(
            message=f'event {app_ev.event.id} deleted',
        )
        return jsonify(payload), 200

    def participants(
            self, event_id: int, participation: str, actor_id: int,
    ) -> FlaskAnswer:
        cmd = commands.GetEventParticipants(
            event_id=event_id, actor_id=actor_id,
            participation=participation)
        users: list[User] = self._handle(cmd).results[0]
        payload = {
            'message': counts(len(users), participation[:-1], start='found'),
            participation: [
                {'user_id': user.id, 'link': f'/api/users/{user.id}'}
                for user in users],
        }
        return jsonify(payload), 200

    def set_participation(
            self,
            actor_id: int,
            event_id: int,
            participation: str,
    ) -> FlaskAnswer:
        cmd = commands.SetEventParticipation(
            actor_id=actor_id, event_id=event_id, participation=participation,
            participant_id=extract_user_id(required=False))
        message: str = self._handle(cmd).results[0]
        return jsonify(dict(message=message)), 200


def create_events_blueprint(auth: AuthService, uow: UnitOfWork) -> Blueprint:
    """Build Flask sub-app ('blueprint') for events handling."""
    events_blp = Blueprint('events', __name__)
    events_api = EventsAPI(uow=uow)

    events_blp.add_url_rule(
        '/new',
        methods=['POST'],
        view_func=auth.inject_actor_id(events_api.create),
    )
    events_blp.add_url_rule(
        '/<int:event_id>',
        methods=['GET'],
        view_func=auth.inject_actor_id(events_api.details, required=False),
    )
    events_blp.add_url_rule(
        '/<int:event_id>',
        methods=['PUT'],
        view_func=auth.inject_actor_id(events_api.update),
    )
    events_blp.add_url_rule(
        '/<int:event_id>/group',
        methods=['PUT'],
        view_func=auth.inject_actor_id(events_api.change_group),
    )
    events_blp.add_url_rule(
        '/<int:event_id>',
        methods=['DELETE'],
        view_func=auth.inject_actor_id(events_api.delete),
    )
    events_blp.add_url_rule(
        '/all',
        methods=['GET'],
        view_func=auth.inject_actor_id(events_api.all_events, required=False),
    )
    events_blp.add_url_rule(
        '/<int:event_id>/<participation>',
        methods=['GET'],
        view_func=auth.inject_actor_id(events_api.participants, required=False),
    )
    events_blp.add_url_rule(
        '/<int:event_id>/<participation>',
        methods=['POST'],
        view_func=auth.inject_actor_id(events_api.set_participation),
    )

    return events_blp


def noop(x):
    return x


#: mapping of functions to transform from JSON data to Python objects.
#:
#: This mapping defines the fields that will be loaded from a request; fields
#: not defined here will be ignored.
DESERIALIZERS: dict[str, tuple[Callable, str]] = dict(
    title=(noop, ''),
    start=(datetime.fromisoformat, "shall comply with ISO-8601"),
    duration=(timedelta_from_hms, "shall be like HH:MM or HH:MM:SS"),
    status=(
        event_status_from_str,
        "status field shall be one of [active, in-preparation, cancelled]"),
    is_public=(noop, "shall be boolean"),
)


def extract_group_id() -> int:
    """Extract group_id from request."""
    json: Optional[Any] = request.json
    if json is None:
        raise web_exc.RequestParseError('request is not in JSON format')
    try:
        return json['group_id']
    except KeyError:
        raise web_exc.RequestParseError(
            'missing field(s) in request for event creation: group_id')


def extract_event_data() -> EventData:
    """Extract event data, for creation or update, from request.

    This function makes the minimum assumption on what data shall be provided,
    as this is up to the model -- this is done at the end, when creating a
    :py:class:EventData instance.

    """
    event_data = {}
    errs = []
    json: Optional[Any] = request.json
    if json is None:
        raise web_exc.RequestParseError('request is not in JSON format')
    for field_name, value in json.items():
        if field_name in DESERIALIZERS:
            converter, err_msg_fmt = DESERIALIZERS[field_name]
            try:
                event_data[field_name] = converter(value)
            except (ValueError, TypeError):
                errs.append(
                    f"{field_name} {err_msg_fmt.format(value)}, "
                    f"but got '{value}'")
        # only the fields listed in the SERIALIZERS are stored.
    if errs:
        raise web_exc.RequestParseError(
            f"cannot parse input(s): {'; '.join(errs)}")
    try:
        return EventData(**event_data)
    except (KeyError, TypeError) as exc:
        raise web_exc.RequestParseError(f"missing field(s) in request: {exc}")


#: mapping of functions to apply from Python objects to prepare for JSON.
SERIALIZERS: dict[str, Callable] = dict(
    start=str,
    duration=hm_from_timedelta,
)


def serialize(data: dict[str, Any]) -> dict[str, Any]:
    return {field_name: SERIALIZERS.get(field_name, noop)(value)
            for field_name, value in data.items()}
