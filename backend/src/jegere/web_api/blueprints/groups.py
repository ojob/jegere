from typing import Any, Optional

from flask import Blueprint, jsonify, request

from ...domain import model
from ...service_layer.unit_of_work import UnitOfWork
from ...service_layer import app_events, commands, message_bus as bus
from .. import FlaskAnswer
from .. import exc as web_exc
from ..authentication import AuthService, extract_user_id
from ..utils import counts


class GroupsAPI:
    """Class to handle Flask requests and responses for :py:class:`Groups`."""
    def __init__(self, uow: UnitOfWork):
        self.uow = uow

    def _handle(self, cmd: commands.Command):
        return bus.handle(cmd, uow=self.uow)

    # --- endpoints ---
    def new(self, actor_id: int) -> FlaskAnswer:
        cmd = commands.CreateGroup(actor_id=actor_id, data=extract_group_data())
        ev: app_events.GroupCreated = self._handle(cmd).results[0]
        payload = dict(
            message='new group created',
            group=dict(
                group_id=ev.group_id,
                link=f'/api/groups/{ev.group_id}',  # TODO: need to use url_for
            ),
        )
        return jsonify(payload), 201  # created

    def update(self, actor_id: int, group_id: int) -> FlaskAnswer:
        cmd = commands.UpdateGroup(
            actor_id=actor_id, group_id=group_id, data=extract_group_data())
        ev: app_events.GroupUpdated = self._handle(cmd).results[0]
        payload = dict(
            message=f"group {group_id} updated: "
                    + ', '.join(update.field for update in ev.updates)
        )
        return jsonify(payload), 200

    def delete(self, actor_id: int, group_id: int) -> FlaskAnswer:
        cmd = commands.RemoveGroup(actor_id=actor_id, group_id=group_id)
        trace = self._handle(cmd).trace
        n_events_dropped = \
            len([ev for ev in trace if type(ev) == app_events.EventRemoved])
        payload = dict(
            message=f'group {group_id} deleted; '
                    f'{counts(n_events_dropped, "event")} dropped',
        )
        return jsonify(payload), 200

    def details(
            self, group_id: int, actor_id: int = None) -> FlaskAnswer:
        cmd = commands.GetGroupPrivateDetails(
            actor_id=actor_id, group_id=group_id)
        group_details = self._handle(cmd).results[0]
        payload = dict(
            message='group found',
            group=group_details,
        )
        return jsonify(payload), 200

    def groups(self, actor_id: int = None) -> FlaskAnswer:
        cmd = commands.GetGroupsDetails(actor_id=actor_id)
        groups_details: dict[str, dict] = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(groups_details), "group", start="found"),
            groups=[
                {'link': f'/api/groups/{group_id}', **public_details}
                for group_id, public_details in groups_details.items()],
        )
        return jsonify(payload), 200

    def user_groups(self, actor_id: int) -> FlaskAnswer:
        cmd = commands.GetUserGroups(actor_id=actor_id)
        groups_details: dict[str, dict] = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(groups_details), "group", start="found"),
            groups=[
                {'link': f'/api/groups/{group_id}', **private_details}
                for group_id, private_details in groups_details.items()]
        )
        return jsonify(payload), 200

    def members(self, actor_id: int, group_id: int) -> FlaskAnswer:
        cmd = commands.GetMembers(actor_id=actor_id, group_id=group_id)
        members_details: dict[str, dict] = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(members_details), "member", start="found"),
            members=[{'link': f'/api/users/{member_id}', **member_details}
                     for member_id, member_details in members_details.items()],
        )
        return jsonify(payload), 200

    def add_member(self, actor_id: int, group_id: int) -> FlaskAnswer:
        """Add a member to *group*, identified by Flask *request*."""
        new_member_id: int = extract_user_id()
        cmd = commands.AddMember(
            actor_id=actor_id, group_id=group_id, user_id=new_member_id)
        message = bus.handle(cmd, uow=self.uow).results[0]
        return jsonify(dict(message=message)), 200

    def remove_member(
            self, actor_id: int, group_id: int, user_id: int) -> FlaskAnswer:
        """Remove a member from *group*, identified by URI *user_id*."""
        cmd = commands.RemoveMember(
            actor_id=actor_id, group_id=group_id, user_id=user_id)
        message = self._handle(cmd).results[0]
        return jsonify(dict(message=message)), 200

    def coordinators(self, actor_id: int, group_id: int) -> FlaskAnswer:
        cmd = commands.GetCoordinators(actor_id=actor_id, group_id=group_id)
        coords_details: dict[int, dict] = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(coords_details), "coordinator", start="found"),
            coordinators=[
                {'link': f'/api/users/{coord_id}', **coord_details}
                for coord_id, coord_details in coords_details.items()],
        )
        return jsonify(payload), 200

    def add_coordinator(self, actor_id: int, group_id: int) -> FlaskAnswer:
        """Add a member to *group*, identified by Flask *request*."""
        new_coordinator_id: int = extract_user_id()
        cmd = commands.AddCoordinator(
            actor_id=actor_id, group_id=group_id, user_id=new_coordinator_id)
        message = self._handle(cmd).results[0]
        return jsonify(dict(message=message)), 200

    def remove_coordinator(
            self, actor_id: int, group_id: int, user_id: int) -> FlaskAnswer:
        cmd = commands.RemoveCoordinator(
            actor_id=actor_id, group_id=group_id, user_id=user_id)
        message = bus.handle(cmd, uow=self.uow).results[0]
        return jsonify(dict(message=message)), 200

    def events(self, group_id: int, actor_id: int = None) -> FlaskAnswer:
        cmd = commands.GetGroupEvents(group_id=group_id, actor_id=actor_id)
        events_details: dict[int, dict] = self._handle(cmd).results[0]
        payload = dict(
            message=counts(len(events_details), 'event', start='found'),
            events=[
                {'event_id': event_id, 'link': f'/api/events/{event_id}',
                 **event_details}
                for event_id, event_details in events_details.items()],
        )
        return jsonify(payload), 200


def create_groups_blueprint(auth: AuthService, uow: UnitOfWork) -> Blueprint:
    """Build Flask sub-app ('blueprint') for groups handling."""
    groups_blp = Blueprint('groups', __name__)
    groups_api = GroupsAPI(uow=uow)

    groups_blp.add_url_rule(
        '/new',
        view_func=auth.inject_actor_id(groups_api.new),
        methods=['POST'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>',
        view_func=auth.inject_actor_id(groups_api.details, required=False),
        methods=['GET'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>',
        view_func=auth.inject_actor_id(groups_api.update),
        methods=['PUT'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>',
        view_func=auth.inject_actor_id(groups_api.delete),
        methods=['DELETE'],
    )
    groups_blp.add_url_rule(
        '/all',
        view_func=auth.inject_actor_id(groups_api.groups, required=False),
        methods=['GET'],
    )
    groups_blp.add_url_rule(
        '/mine',
        view_func=auth.inject_actor_id(groups_api.user_groups),
        methods=['GET'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/members',
        view_func=auth.inject_actor_id(groups_api.members),
        methods=['GET'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/members',
        view_func=auth.inject_actor_id(groups_api.add_member),
        methods=['POST'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/members/<int:user_id>',
        view_func=auth.inject_actor_id(groups_api.remove_member),
        methods=['DELETE'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/coordinators',
        view_func=auth.inject_actor_id(groups_api.coordinators),
        methods=['GET'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/coordinators',
        view_func=auth.inject_actor_id(groups_api.add_coordinator),
        methods=['POST'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/coordinators/<int:user_id>',
        view_func=auth.inject_actor_id(groups_api.remove_coordinator),
        methods=['DELETE'],
    )
    groups_blp.add_url_rule(
        '/<int:group_id>/events',
        view_func=auth.inject_actor_id(groups_api.events, required=False),
        methods=['GET'],
    )
    return groups_blp


def extract_group_data() -> model.GroupData:
    """Extract group data from Flask request."""
    json: Optional[Any] = request.json
    if json is None:
        raise web_exc.RequestParseError('request is not in JSON format')
    try:
        return model.GroupData(
            name=json['name'],
            is_public=json.get('is_public', True),
        )
    except KeyError:
        raise web_exc.RequestParseError("name missing in request")
