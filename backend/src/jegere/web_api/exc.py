"""This module provides the exceptions from the web API.

The hierarchy is as follows, and intentionally matches HTTP status codes:

    Exception (Python built-in)
    |
    +-- BadRequest
        +-- RequestParseError
            +-- CredentialsParseError

"""


class BadRequest(Exception):
    """To signal a request format error."""


class RequestParseError(BadRequest):
    """When request does not provide the expected fields."""


class CredentialsParseError(RequestParseError):
    """When request does not provide the expected fields."""
