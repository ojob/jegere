"""This module provides the root of the Flask instance.

The overall catching of exceptions is performed here, providing a centralized
management of return codes, and removing the need for every endpoint method to
deal will all possible cases.

This is made possible thanks to the exceptions hierarchy, that limits the top-
level possibilities while being easily mappable to HTTP status codes.

For testing purpose, it requires several inputs that are not trivial
to implement; see `./bootstrap.py`, or test file `web_api/conftest.py`.

"""
from dataclasses import dataclass
from typing import Any

from flask import Flask, Response, jsonify, send_from_directory
from flask_cors import CORS

from ..config import Config, RunLevel
from ..domain import exc as core_exc
from ..service_layer.unit_of_work import UnitOfWork
from . import FlaskAnswer
from . import exc as web_exc
from .authentication import AuthService
from .blueprints.users import create_users_blueprint
from .blueprints.groups import create_groups_blueprint
from .blueprints.events import create_events_blueprint


@dataclass
class WebRoot:
    app: Flask
    auth: AuthService
    uow: UnitOfWork


def link_app(
        config: Config,
        uow: UnitOfWork,
) -> WebRoot:
    app = Flask(__name__)
    app.config.from_object(config)

    auth: AuthService = AuthService(uow=uow)

    if app.config['RUN_LEVEL'] > RunLevel.PROD:
        CORS(app, resources={'/api/*': {'origins': '*'}})

    if app.config['BACK_SERVE_FRONT']:
        @app.route('/')
        @app.route('/<path:filename>')
        def serve_static(filename: str = None) -> Response:
            if filename is None:
                filename = 'index.html'
            return send_from_directory(app.config['FRONT_BUILD_DIR'], filename)

    @app.route('/api/status')
    def status() -> FlaskAnswer:
        return jsonify(dict(healthy=True)), 200

    users_blp = create_users_blueprint(auth=auth, uow=uow)
    app.register_blueprint(users_blp, url_prefix='/api/users')

    groups_blp = create_groups_blueprint(auth=auth, uow=uow)
    app.register_blueprint(groups_blp, url_prefix='/api/groups')

    events_blp = create_events_blueprint(auth=auth, uow=uow)
    app.register_blueprint(events_blp, url_prefix='/api/events')

    # ---- adding custom top-level errors handlers
    def to_json(stuff: Any, **kwargs) -> Response:
        """Jsonify *stuf* as part of a dictionary, at *message* entry."""
        return jsonify(dict(message=str(stuff), **kwargs))

    @app.errorhandler(web_exc.RequestParseError)
    def handle_bad_request(exc):
        return to_json(exc), 400

    @app.errorhandler(core_exc.InvalidData)
    def handle_invalid_data(exc):
        return to_json(exc), 400

    @app.errorhandler(core_exc.AuthenticationFailed)
    def handle_authentication_failure(exc):
        return to_json(exc), 401

    @app.errorhandler(core_exc.NotAuthorized)
    def handle_not_authorized(exc):
        return to_json(exc, link='/api/users/login'), 401

    @app.errorhandler(core_exc.NotAllowed)
    def handle_not_allowed(exc):
        return to_json(exc), 403

    @app.errorhandler(core_exc.NotFound)
    def handle_not_found(exc):
        return to_json(exc), 404

    @app.errorhandler(core_exc.CreationDuplicate)
    def handle_creation_duplicate(exc):
        return to_json(exc), 409  # Conflict

    return WebRoot(app, auth=auth, uow=uow)
