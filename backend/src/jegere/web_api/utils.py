"""This module provides utility functions for web api perimeter."""


def counts(
        count: int,
        label: str,
        *,
        none_mark: str = "no",
        start: str = None,
        end: str = None,
        plural_mark: str = "s",
        plural: str = None,
) -> str:
    """Build a nice string with some counts.

    It provides only a simple way to handle plurality.

    >>> counts(2, 'guy')
    '2 guys'
    >>> counts(2, 'man', plural='men')
    '2 men'
    >>> counts(1, 'woman', plural='women')
    '1 woman'
    >>> counts(3, 'lias', plural_mark='es', start='found', end='today')
    'found 3 liases today'
    >>> counts(0, 'boy', none_mark='ooo')
    'ooo boy'

    """
    res: list[str] = []
    if start is not None:
        res.append(start)
    if count == 0:
        res.append(none_mark)
        res.append(label)
    elif count >= 1:
        res.append(str(count))
        if count == 1:
            res.append(label)
        else:
            if plural is not None:
                res.append(plural)
            else:
                res.append(label + plural_mark)
    if end is not None:
        res.append(end)

    return ' '.join(res)
