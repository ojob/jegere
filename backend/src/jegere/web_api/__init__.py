from typing import Tuple

from flask import Response

FlaskAnswer = Tuple[Response, int]
