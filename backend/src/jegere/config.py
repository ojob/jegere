import os
from collections.abc import Callable
from dataclasses import dataclass
from enum import IntEnum
from pathlib import Path
from typing import Any, Optional

HERE: Path = Path(__file__).parent
PROJECT_ROOT: Path = HERE.parent.parent.parent


def get_env(
        key: str, *,
        converter: Callable = None,
        default: Any = None,
) -> Any:
    """Get environment value, taking care to format and existence."""
    try:
        val = os.environ[key]
    except KeyError:
        if default is not None:
            return default
        raise
    else:
        if converter is not None:
            return converter(val)
        return val


# default values
# TODO: there are defaults for DEV, so a clear warning is needed at start-up
FRONTEND_DIST = get_env(
    key='FRONTEND_DIST',
    converter=Path,
    default=PROJECT_ROOT / 'frontend' / 'dist' / 'jegere',
)
HOST = get_env('HOST', default='0.0.0.0')
PORT = get_env('PORT', converter=int, default=5050)


class RunLevel(IntEnum):
    PROD = 0
    DEV = 1
    TEST = 2


@dataclass
class Config:
    TOKEN_SECRET_KEY: str

    HOST: str = HOST
    PORT: int = PORT

    RUN_LEVEL: RunLevel = RunLevel.PROD
    BACK_SERVE_FRONT: bool = False
    FRONT_BUILD_DIR: Optional[Path] = None
    DEBUG: bool = None  # type: ignore  # solved in __post_init__ method

    PASSWORDS_BCRYPT_ROUNDS: int = 12
    TOKEN_VALIDITY: float = 86400 * 30  # in seconds; valid one month

    def __post_init__(self):
        if self.DEBUG is None:
            self.DEBUG = (self.RUN_LEVEL > RunLevel.PROD)


#: Configuration for backend testing purpose
test_config = Config(
    TOKEN_SECRET_KEY='VE33ry Sekkreeet key',

    HOST='localhost',
    PORT=8080,

    RUN_LEVEL=RunLevel.TEST,
    BACK_SERVE_FRONT=True,
    FRONT_BUILD_DIR=PROJECT_ROOT / 'backend' / 'tests' / 'assets',

    PASSWORDS_BCRYPT_ROUNDS=4,
    TOKEN_VALIDITY=5,  # in seconds; valid 5 seconds
)


#: Configuration for development purpose
dev_config = Config(
    TOKEN_SECRET_KEY='od-"_d-)[_sicfiiökowéd%%0#sd-fio 3"-ifncéROicnoI',

    RUN_LEVEL=RunLevel.DEV,
    BACK_SERVE_FRONT=True,
    FRONT_BUILD_DIR=FRONTEND_DIST,

    PASSWORDS_BCRYPT_ROUNDS=10,
    TOKEN_VALIDITY=3600  # in seconds; valid one hour
)
