"""This is where everything is glued together, to get a web backend running.

This module provides two interesting functions:

- :py:func:`build_app` returns a WSGI-compatible object, ready to be ran in
  production.

- :py:func:`run` runs the web app on a test server. DO NOT USE IN PRODUCTION!!!

Both can be passed a configuration object.


"""
from flask import Flask

from .config import Config, dev_config
from .domain.crypto import BcryptPasswordManager, TokenManager
from .adapters.repository import InMemoryRepository
from .service_layer.unit_of_work import UnitOfWork
from .web_api.root import link_app, WebRoot


def build_uow(config: Config = dev_config) -> UnitOfWork:
    """Create the app core from the configuration."""
    pw_manager = BcryptPasswordManager(
        bcrypt_rounds=config.PASSWORDS_BCRYPT_ROUNDS,
    )
    token_manager = TokenManager(
        secret_key=config.TOKEN_SECRET_KEY,
        validity=config.TOKEN_VALIDITY,
    )
    store = InMemoryRepository()
    uow = UnitOfWork(
        pw_manager=pw_manager,
        token_manager=token_manager,
        store=store,
    )
    return uow


def build_app(
        uow: UnitOfWork = None,
        config: Config = dev_config,
) -> WebRoot:
    """Create the app components, and glue them together."""
    if uow is None:
        uow = build_uow(config=config)
    web_root = link_app(config=config, uow=uow)
    if config.DEBUG:
        web_root.app.testing = True

    return web_root


def run(app: Flask = None):  # pragma: no cover
    """Launch the server, for testing purpose.

    For production, use a decent WSGI server with :py:attr:`web_api.app`.

    """
    if app is None:
        app = build_app().app

    app.run(
        host=app.config['HOST'],
        port=app.config['PORT'],
        debug=app.config['DEBUG'],
    )
