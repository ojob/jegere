from __future__ import annotations

import abc
from collections import defaultdict
from typing import Optional

from ..domain import exc
from ..domain.crypto import Token
from ..domain.model import Event, Group, User
from ..service_layer import app_events


class AbstractRepository(abc.ABC):
    """Interface for data persistence."""

    # ---- user stuff ----
    @abc.abstractmethod
    def store_user(self, user: User) -> None:
        raise NotImplementedError()  # pragma: no cover

    def get_user(self, *, user_id: int = None, email: str = None) -> User:
        if user_id is not None:
            user: User = self.get_user_by_id(user_id=user_id)
        elif email is not None:
            user = self.get_user_by_email(email=email)
        else:
            raise TypeError("one of user_id, email or token shall be provided")
        return user

    def find_user(self, **kwargs) -> Optional[User]:
        """Get user, but catch error and return `None` if not found."""
        try:
            return self.get_user(**kwargs)
        except exc.UserNotFound:
            return None

    @abc.abstractmethod
    def get_user_by_id(self, user_id: int) -> User:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def get_user_by_email(self, email: str) -> User:
        """Get the user by email, case-insensitive!"""
        raise NotImplementedError()  # pragma: no cover

    @property
    @abc.abstractmethod
    def users(self) -> list[User]:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def del_user(self, user_id: int) -> None:
        """Delete user; mainly useful for testing purpose."""
        raise NotImplementedError()  # pragma: no cover

    # ---- token stuff ----
    @abc.abstractmethod
    def store_token(self, user_id: int, token: Token) -> None:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def blacklist(self, token: Token) -> None:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def is_blacklisted(self, token: Token) -> bool:
        raise NotImplementedError()  # pragma: no cover

    # ---- group stuff ----
    @abc.abstractmethod
    def store_group(self, group: Group) -> None:
        raise NotImplementedError()  # pragma: no cover

    def get_group(self, *, group_id: int = None, name: str = None) -> Group:
        """Get the group matching the criterion provided."""
        if group_id is not None:
            group = self.get_group_by_id(group_id=group_id)
        elif name is not None:
            group = self.get_group_by_name(name=name)
        else:
            raise TypeError("one of group_id, name shall be provided")
        return group

    def find_group(self, **kwargs) -> Optional[Group]:
        """Return the group if it exists, `None` otherwise."""
        try:
            group = self.get_group(**kwargs)
        except exc.GroupNotFound:
            return None
        return group

    @abc.abstractmethod
    def get_group_by_id(self, group_id: int) -> Group:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def get_group_by_name(self, name: str) -> Group:
        raise NotImplementedError()  # pragma: no cover

    @property
    @abc.abstractmethod
    def groups(self) -> list[Group]:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def update_group(self, group: Group, ev: app_events.GroupUpdated) -> None:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def remove_group(self, group: Group) -> None:
        raise NotImplementedError()  # pragma: no cover

    # ---- event stuff ----
    @abc.abstractmethod
    def store_event(self, event: Event) -> None:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def get_event(self, event_id: int) -> Event:
        raise NotImplementedError()  # pragma: no cover

    def find_event(self, event_id: int) -> Optional[Event]:
        try:
            return self.get_event(event_id=event_id)
        except exc.EventNotFound:
            return None

    @property
    @abc.abstractmethod
    def events(self) -> list[Event]:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def update_event(self, event: Event, ev: app_events.EventUpdated) -> None:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def remove_event(self, event: Event) -> None:
        raise NotImplementedError()  # pragma: no cover


class InMemoryRepository(AbstractRepository):
    """Implementation with storage in process memory, as dictionaries."""

    def __init__(self):
        super().__init__()

        self._users_by_id: dict[int, User] = {}
        self._users_by_email: dict[str, User] = {}
        self._tokens_by_user_id: dict[int, list[Token]] = defaultdict(list)
        self._tokens_blacklist: set[Token] = set()
        self._groups_by_id: dict[int, Group] = {}
        self._groups_by_name: dict[str, Group] = {}
        self._events_by_id: dict[int, Event] = {}

    # ---- user stuff ----
    def store_user(self, user: User) -> None:
        self._users_by_id[user.id] = user
        # emails are considered case-insensitive, to avoid subtle conflicts
        self._users_by_email[user.email.lower()] = user

    def get_user_by_id(self, user_id: int) -> User:
        try:
            return self._users_by_id[user_id]
        except KeyError:
            raise exc.UserNotFound(f"no user with user_id={user_id}")

    def get_user_by_email(self, email: str) -> User:
        try:
            return self._users_by_email[email.lower()]
        except KeyError:
            raise exc.UserNotFound(f"no user with email={email}")

    @property
    def users(self) -> list[User]:
        return list(self._users_by_id.values())

    def del_user(self, user_id: int) -> None:
        try:
            email = self.get_user_by_id(user_id=user_id).email
            del self._users_by_id[user_id]
            del self._tokens_by_user_id[user_id]
            del self._users_by_email[email]
        except KeyError:
            raise exc.UserNotFound(f"no user with user_id={user_id}")

    def store_token(self, user_id: int, token: Token) -> None:
        self._tokens_by_user_id[user_id].append(token)

    def blacklist(self, token: Token) -> None:
        self._tokens_blacklist.add(token)

    def is_blacklisted(self, token: Token) -> bool:
        return token in self._tokens_blacklist

    # ---- group stuff ----
    def store_group(self, group: Group) -> None:
        self._groups_by_id[group.id] = group
        self._groups_by_name[group.name] = group

    def update_group(self, group: Group, ev: app_events.GroupUpdated) -> None:
        for field, old_val, new_val in ev.updates:
            if field == 'name':
                del self._groups_by_name[old_val]
                self._groups_by_name[new_val] = group

    @property
    def groups(self) -> list[Group]:
        return list(self._groups_by_id.values())

    def get_group_by_id(self, group_id: int) -> Group:
        try:
            return self._groups_by_id[group_id]
        except KeyError:
            raise exc.GroupNotFound(f"no group with group_id={group_id}")

    def get_group_by_name(self, name: str) -> Group:
        try:
            return self._groups_by_name[name]
        except KeyError:
            raise exc.GroupNotFound(f"no group with name={name}")

    def remove_group(self, group: Group) -> None:
        try:
            del self._groups_by_id[group.id]
            del self._groups_by_name[group.name]
        except KeyError:
            raise exc.GroupNotFound(f"no group with group_id={group.id}")

    # ---- event stuff ----
    def store_event(self, event: Event) -> None:
        self._events_by_id[event.id] = event

    def update_event(self, event: Event, ev: app_events.EventUpdated) -> None:
        pass  # no storage by title yet

    def get_event(self, event_id: int) -> Event:
        try:
            return self._events_by_id[event_id]
        except KeyError:
            raise exc.EventNotFound(f"no event with event_id={event_id}")

    @property
    def events(self) -> list[Event]:
        return list(self._events_by_id.values())

    def remove_event(self, event: Event) -> None:
        try:
            del self._events_by_id[event.id]
        except KeyError:
            raise exc.EventNotFound(f"no event with event_id={event.id}")
