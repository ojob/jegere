"""This module provides the exception for the domain core.

The hierarchy is as follows:

    Exception (Python built-in)
    |
    +-- AuthenticationFailed
    |   +-- CredentialsCheckError
    |   +-- TokenError
    |   |   +-- BlacklistedToken
    |   |   +-- ExpiredToken
    |   |   +-- InvalidToken
    |   +-- WrongPassword
    |
    +-- CreationDuplicate
    |   +-- EmailAlreadyRegistered
    |   +-- GroupNameAlreadyTaken
    |
    +-- InvalidData
    |
    +-- NotAllowed
    |
    +-- NotAuthorized
    |
    +-- NotFound
        +- EventNotFound
        +- UserNotFound
        +- GroupNotFound

"""


# --- authentication exceptions ----
class AuthenticationFailed(Exception):
    """For user authentication failed due to wrong credentials."""


class CredentialsCheckError(AuthenticationFailed):
    """When request does not contain the expected fields, i.e. format error."""


class TokenError(AuthenticationFailed):
    """For errors related to tokens."""


class BlacklistedToken(TokenError):
    """For trying to use a blacklisted token."""


class ExpiredToken(TokenError):
    """For tokens that are expired."""


class InvalidToken(TokenError):
    """For tokens that cannot be decoded."""


class WrongPassword(AuthenticationFailed):
    """When candidate password does not match."""


# ---- data manipulation failure exceptions ----
class CreationDuplicate(Exception):
    """For trying to create duplicate record."""


class EmailAlreadyRegistered(CreationDuplicate):
    """For user creation, but with already existing email."""


class GroupNameAlreadyTaken(CreationDuplicate):
    """For group name already taken."""


# ---- invalid data exceptions ----
class InvalidData(Exception):
    """For an action that fails because of invalid data."""


# ---- authorization failure exceptions ----
class NotAllowed(Exception):
    """For an action refused, because not consistent."""


class NotAuthorized(Exception):
    """For an action refused, because user misses rights."""


# ---- data access failure exceptions ----
class NotFound(Exception):
    """For items not found."""


class EventNotFound(NotFound):
    """For events not found"""


class UserNotFound(NotFound):
    """For users not found"""


class GroupNotFound(NotFound):
    """For groups not found"""
