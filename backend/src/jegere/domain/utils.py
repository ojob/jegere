"""Provides utility functions for domain perimeter."""

from datetime import timedelta


def hms_from_timedelta(dt: timedelta) -> str:
    """Format a timedelta to string as `HH:MM:SS`.

    In case the timedelta is negative, a leading dash is present.

    >>> hms_from_timedelta(timedelta(seconds=63.22))
    '00:01:03'
    >>> hms_from_timedelta(timedelta(hours=-26, seconds=22))
    '-26:00:22'
    >>> hms_from_timedelta(timedelta(hours=48))
    '48:00:00'

    """
    seconds = int(dt.total_seconds())
    hours, seconds = divmod(seconds, 3600)
    minutes, seconds = divmod(seconds, 60)
    return f'{hours:02}:{minutes:02}:{seconds:02}'


def hm_from_timedelta(dt: timedelta) -> str:
    """Format a timedelta to string as `HH:MM`, ignoring seconds.

    >>> hm_from_timedelta(timedelta(seconds=61))
    '00:01'
    >>> hm_from_timedelta(timedelta(hours=2, seconds=59))
    '02:00'

    """
    return hms_from_timedelta(dt)[:5]


def timedelta_from_hms(hms: str) -> timedelta:
    """Build a timedelta from a string as `HH:MM:SS` or `HH:MM`.

    If a leading dash is present, the returned timedelta is negative.

    >>> timedelta_from_hms('01:59:22').total_seconds()
    7162.0
    >>> timedelta_from_hms('-28:00:00').total_seconds() == -28 * 3600
    True
    >>> timedelta_from_hms('01:30').total_seconds() == 1.5 * 3600
    True

    """
    sign: int = -1 if hms.startswith('-') else +1
    parts = [int(part) for part in hms[1:].split(':')]
    hours, minutes = parts[:2]
    if len(parts) == 3:
        seconds = parts[2]
    else:
        seconds = 0
    return timedelta(hours=sign * hours, minutes=minutes, seconds=seconds)
