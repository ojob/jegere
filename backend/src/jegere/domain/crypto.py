import abc
from datetime import datetime, timedelta

import bcrypt
import jwt

from .exc import ExpiredToken, InvalidToken, WrongPassword

Token = str  #: type for tokens

ALGORITHM = 'HS256'


class PasswordManager(abc.ABC):
    """Interface for managing passwords."""

    @abc.abstractmethod
    def hash(self, clear_password: str) -> str:
        raise NotImplementedError()  # pragma: no cover

    @abc.abstractmethod
    def check(self, candidate_password: str, password_hash: str) -> None:
        raise NotImplementedError()  # pragma: no cover


class BcryptPasswordManager(PasswordManager):
    """Implementation using Bcrypt."""

    def __init__(self, bcrypt_rounds: int = 14):
        self.rounds = bcrypt_rounds

    def hash(self, clear_password: str) -> str:
        """Hash *clear_password*, so that it can be stored properly."""
        # `bcrypt.hashpw` only handles bytes, so we need to encode the clear
        # password in input, and decode towards unicode in output.
        return bcrypt.hashpw(
            clear_password.encode('utf-8'),
            bcrypt.gensalt(self.rounds)
        ).decode('utf-8')

    def check(self, candidate_password: str, password_hash: str) -> None:
        """Check that *candidate_password* matches *password_hash*.

        Raises:
            WrongPassword: if check failed

        """
        if not bcrypt.checkpw(
                candidate_password.encode('utf-8'),
                password_hash.encode('utf-8'),
        ):
            raise WrongPassword()


class TokenManager:
    """Class to handle Java Web Tokens (JWT)."""

    def __init__(self, secret_key: str, validity: float):
        """Initialize.

        Args:
            secret_key: the key for token signature.
            validity: the validity duration of the token.

        """
        self._secret_key = secret_key
        self.validity = validity

    def encode(self, user_id: int) -> Token:
        """Encode *user_id* in a JWT token.

        Args:
            user_id: identifier of the user to encode.

        Returns:
            Token, as a unicode string (not as bytes).

        This token can then be used as a proof that the user was correctly
        authenticated in the past.

        """
        now = datetime.utcnow()
        payload = dict(sub=user_id, iat=now, exp=now + timedelta(self.validity))
        return jwt.encode(payload, self._secret_key, algorithm=ALGORITHM)

    def decode(self, token: Token) -> int:
        """Decode *token*, using the initial secret key, to retrieve user_id.

        Args:
            token: JSON Web Token, as a unicode string (not as bytes).

        Returns:
            The user_id that was encoded.

        Raises:
            TokenSignatureError: if the token is either expired or invalid.

        """
        try:
            return jwt.decode(
                token, key=self._secret_key, algorithms=[ALGORITHM]
            )['sub']
        except (jwt.DecodeError, jwt.InvalidSignatureError):
            raise InvalidToken("Token is invalid and cannot be used")
        except jwt.ExpiredSignatureError:
            raise ExpiredToken("Token is expired and cannot be used anymore")
