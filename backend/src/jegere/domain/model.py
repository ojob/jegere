"""Model classes.

Note: there is no mutation function here; these shall all be in the dedicated
:py:mod:`ops` module. The idea behind is that this module does only provide
the characteristics, while the other module takes care of consistency.

"""
from __future__ import annotations

from collections.abc import Callable
from dataclasses import dataclass, field
from datetime import datetime, timedelta
from enum import Enum
from functools import partial
from typing import Any, Iterable, NamedTuple

from . import exc

#: unique ids generator, used if no ID is passed at object creation.
_ids = (n for n in range(10**30))


class UserData(NamedTuple):
    """Class to simply hold User creation data."""
    email: str
    password: str


def check_credentials(email: str, password: str) -> None:
    """Check the provided user credentials"""
    if not email or not password:
        raise exc.CredentialsCheckError("email and password shall be non-empty")


@dataclass
class User:
    """Value object to represent a user, and gather its public methods."""
    email: str
    password: str = field(repr=False)  #: hash of user password

    id: int = field(default_factory=lambda: next(_ids))
    groups: set[Group] = field(default_factory=set, repr=False)
    events: dict[Event, Participation] = field(default_factory=dict, repr=False)

    app_events: list = field(default_factory=list, repr=False)

    def public_details(self) -> dict[str, Any]:
        return dict(
            user_id=self.id,
            email=self.email,
        )

    def private_details(self) -> dict[str, Any]:
        return self.public_details()

    # ---- magic methods ----
    def __eq__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return self.id == other.id \
                and self.email == other.email
        return NotImplemented

    def __gt__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return self.email > other.email
        return NotImplemented

    def __hash__(self) -> int:
        return hash(
            ''.join(str(attr)
                    for attr in [self.__class__.__name__, self.id]))


class GroupData(NamedTuple):
    name: str
    is_public: bool = True


@dataclass
class Group:
    name: str
    creator: User = field(repr=False)
    id: int = field(default_factory=lambda: next(_ids))
    coordinators: set[User] = field(default_factory=set, repr=False)
    members: set[User] = field(default_factory=set, repr=False)
    events: set[Event] = field(default_factory=set, repr=False)
    is_public: bool = True

    app_events: list = field(default_factory=list, repr=False)

    def public_details(self) -> dict[str, Any]:
        return dict(
            group_id=self.id,
            name=self.name,
            is_public=self.is_public,
        )

    def private_details(self) -> dict[str, Any]:
        return {
            **self.public_details(),
            'creator_id': self.creator.id,
            'members_nb': len(self.members),
            'coordinators_nb': len(self.coordinators),
        }

    # ---- model methods ----
    def is_visible(self, user: User = None) -> bool:
        """Indicate if the group is visible.

        Args:
            user: if provided, answers the question from the point of view of
                this user (i.e. is the user a member of *group*).
                Otherwise, indicates if the group is visible publicly.

        """
        return self.is_public or (user is not None and user in self.members)

    # ---- magic methods ----
    def __eq__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return self.id == other.id \
                and self.name == other.name
        return NotImplemented

    def __gt__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return self.name > other.name
        return NotImplemented

    def __hash__(self) -> int:
        return hash(
            ''.join(str(attr)
                    for attr in [self.__class__.__name__, self.id]))


class EventStatus(Enum):
    ACTIVE = 'active'
    IN_PREPARATION = 'in-preparation'
    CANCELLED = 'cancelled'


class Participation(Enum):
    VOLUNTEER = 'volunteer'
    BACK_UP = 'back-up'
    ABSENT = 'absent'


def from_str(s: str, cls: Iterable):
    """Retrieve entry from an iterable (*cls*) that matches *s*.

    This is aimed at finding Enum entries.

    >>> from_str('cancelled', EventStatus)
    <EventStatus.CANCELLED: 'cancelled'>

    >>> from_str('not there :-)', EventStatus)
    Traceback (most recent call last):
    ...
    ValueError: no entry 'not there :-)' in <enum 'EventStatus'>

    """
    entry: Enum  # for mypy
    for entry in cls:
        if entry.value == s:
            return entry
    else:
        raise ValueError(f"no entry '{s}' in {cls}")


event_status_from_str = partial(from_str, cls=EventStatus)

participation_from_str = partial(from_str, cls=Participation)


def not_empty_str(val: Any) -> bool:
    return len(val) > 0


EVENT_DATA_RULES: dict[str, tuple[Callable, str]] = dict(
    # field_name, rule, msg
    title=(not_empty_str, 'shall be non-empty'),
    start=(
        lambda obj: isinstance(obj, datetime), 'shall be a datetime object'),
    duration=(
        lambda obj: isinstance(obj, timedelta), 'shall be a timedelta object'),
    # TODO: prevent negative durations
)


@dataclass(frozen=True)
class EventData:
    title: str
    start: datetime
    duration: timedelta
    volunteering: int = 1
    status: EventStatus = EventStatus.ACTIVE
    is_public: bool = True

    def __post_init__(self):
        """Execute post-init checks.

        Raises:
            InvalidData: if some data are not valid.

        """
        errs = []
        for field_name, (is_valid, err_msg_fmt) in EVENT_DATA_RULES.items():
            value = getattr(self, field_name)
            if not is_valid(value):
                errs.append(
                    f"{field_name} {err_msg_fmt.format(value)}, "
                    f"but got '{value}'")
        if errs:
            raise exc.InvalidData('invalid value(s): ' + '; '.join(errs))


@dataclass
class Event:
    title: str
    creator: User = field(repr=False)
    group: Group = field(repr=False)
    start: datetime = field(repr=False)
    duration: timedelta = field(repr=False)

    id: int = field(default_factory=lambda: next(_ids))
    volunteering: int = field(default=1, repr=False)
    status: EventStatus = EventStatus.ACTIVE
    is_public: bool = True

    app_events: list = field(default_factory=list, repr=False)

    participants: dict[User, Participation] = field(
        default_factory=dict, repr=False)

    def public_details(self) -> dict[str, Any]:
        return dict(
            event_id=self.id,
            title=self.title,
            group_id=self.group.id,
            is_public=self.is_public,
            start=self.start,
            duration=self.duration,
        )

    def private_details(self) -> dict[str, Any]:
        return {
            **self.public_details(),
            'creator_id': self.creator.id,
            'volunteering': self.volunteering,
            'is_participation_enough': self.is_participation_enough,
        }

    # ---- model methods ----
    def is_visible(self, user: User = None) -> bool:
        """Indicates if the event is visible.

        Args:
            user: if provided, answers the question from the point of view of
                this user (i.e. is the user a member of *event*).
                Otherwise, indicates if the event is visible publicly.

        """
        return self.group.is_public and self.is_public \
            or (user is not None and user in self.group.members)

    @property
    def is_participation_enough(self):
        volunteers = sum(1 if part == Participation.VOLUNTEER else 0
                         for part in self.participants.values())
        return volunteers >= self.volunteering

    @property
    def volunteers(self) -> Iterable[User]:
        return self._participation(part=Participation.VOLUNTEER)

    @property
    def backups(self) -> Iterable[User]:
        return self._participation(part=Participation.BACK_UP)

    @property
    def absents(self) -> Iterable[User]:
        return self._participation(part=Participation.ABSENT)

    def _participation(self, part: Participation) -> Iterable[User]:
        """Provide the list of users for a given participation."""
        for user, participation in self.participants.items():
            if participation == part:
                yield user

    # ---- magic methods ----
    def __eq__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return all([
                self.id == other.id,
                self.title == other.title,
                self.start == other.start,
            ])
        return NotImplemented

    def __gt__(self, other: Any) -> bool:
        if self.__class__ is other.__class__:
            return self.start > other.start
        return NotImplemented

    def __hash__(self) -> int:
        return hash(
            ''.join(str(attr)
                    for attr in [self.__class__.__name__, self.id]))


# ---- assertions ----
def assert_is_visible(
        user: User = None, group: Group = None, event: Event = None) -> None:
    if group is None and event is None:
        raise ValueError('one of group or event must be set')

    if event is not None and not event.is_visible(user=user):
        raise exc.NotAuthorized('accessible only to members')
    if group is not None and not group.is_visible(user=user):
        raise exc.NotAuthorized('accessible only to members')


def assert_is_member(
        user: User = None, group: Group = None, event: Event = None) -> None:
    if user is None:
        raise exc.NotAuthorized('accessible only to members')
    if group is None:
        if event is not None:
            group = event.group
        else:
            raise ValueError('one of group or event must be set')
    if user not in group.members:
        raise exc.NotAuthorized('accessible only to members')


def assert_is_coordinator(user: User, group: Group) -> None:
    if user not in group.coordinators:
        raise exc.NotAuthorized("accessible only to coordinators")


def assert_is_self_or_coordinator(
        actor: User, group: Group, user: User) -> None:
    if actor != user and actor not in group.coordinators:
        raise exc.NotAuthorized('shall act for self or be a coordinator')
