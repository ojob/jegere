"""Functions that straddle two object, hence in their own module.

Note: this also permits to import :py:mod:`app_events` without circular import
difficulties. In fact, all mutating functions on model base classes should be
brought in this module.

"""
from dataclasses import asdict
from typing import NamedTuple, Any

from . import exc
from .model import (
    User, UserData,
    Event, EventData, Participation,
    Group, GroupData,
)


class FieldUpdate(NamedTuple):
    field: str
    old: Any
    new: Any


# ---- user stuff ----
def create_user(data: UserData) -> User:
    user = User(**data._asdict())
    # TODO: catch User instances creations, instead of just storage
    # user.app_events.append(app_events.UserCreated(user_id=user.id))
    return user


# ---- group stuff ----
def create_group(creator: User, data: GroupData) -> Group:
    group = Group(creator=creator, **data._asdict())
    group.coordinators.add(creator)
    group.members.add(creator)
    creator.groups.add(group)
    # TODO: catch Group instances creations, instead of just storage
    # group.app_events.append(app_events.GroupCreated(group_id=group.id))
    return group


def update_group(group: Group, data: GroupData) -> list[FieldUpdate]:
    updates = []
    for field in data.__annotations__:
        new_val = getattr(data, field)
        old_val = getattr(group, field)
        if new_val != old_val:
            updates.append(FieldUpdate(field, old_val, new_val))
            setattr(group, field, new_val)
    return updates


# ---- membership stuff ----
def add_member(group: Group, user: User) -> str:
    group.members.add(user)
    user.groups.add(group)
    return "user added as member"


def remove_member(group: Group, user: User) -> str:
    if user not in group.members:
        return "user is not a group member"

    if user in group.coordinators:
        remove_coordinator(group=group, user=user)

    group.members.remove(user)
    user.groups.remove(group)
    return 'user removed from group members'


def add_coordinator(group: Group, user: User) -> str:
    was_member: bool = user in group.members
    if not was_member:
        add_member(group=group, user=user)

    group.coordinators.add(user)
    return "user added as coordinator"


def remove_coordinator(group: Group, user: User) -> str:
    if user not in group.coordinators:
        return "user is not a group coordinator"

    if len(group.coordinators) == 1:
        raise exc.NotAllowed("sole coordinator")

    group.coordinators.remove(user)
    return "user removed from group coordinators"


# ---- events stuff ----
def create_event(creator: User, group: Group, data: EventData) -> Event:
    event = Event(creator=creator, group=group, **asdict(data))
    group.events.add(event)
    # TODO: catch Event instances creations, instead of just in storage
    # event.app_events.append(app_events.EventCreated(event_id=event.id))
    return event


def update_event(event: Event, data: EventData) -> list[FieldUpdate]:
    updates = []
    for field in data.__annotations__:
        old_val = getattr(event, field)
        new_val = getattr(data, field)
        if new_val != old_val:
            updates.append(
                FieldUpdate(field, old_val, new_val))
            setattr(event, field, new_val)
    return updates


def change_event_group(event: Event, new_group: Group) -> None:
    # following actions need to be done in this order, so that references
    # are swapped correctly
    old_group, event.group = event.group, new_group
    old_group.events.remove(event)
    new_group.events.add(event)


def delete_event(event: Event) -> None:
    event.group.events.remove(event)


# ---- participation stuff ----
def get_participants(event: Event, participation: str = None) -> list[User]:
    if participation is None:
        # return the members of the event group, that are not part of the
        # event participants
        users: list[User] = list(
            event.group.members - set(event.participants.keys()))
    else:
        try:
            users = list(getattr(event, participation))
        except AttributeError:
            raise exc.InvalidData(
                f"unknown participation query: '{participation}'")
    return users


def set_participation(
        event: Event, participant: User, participation: Participation = None,
) -> str:
    if participation is None:
        del event.participants[participant]
        del participant.events[event]
        return "participation removed"
    else:
        event.participants[participant] = participation
        participant.events[event] = participation
        return "participation updated"
