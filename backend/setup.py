from setuptools import setup, find_packages

setup(
    name="jegere",
    version="0.0.0",

    description="Back-end server for jegere.net web application",

    packages=find_packages('src'),
    package_dir={'': 'src'},

    install_requires=[
        'bcrypt',
        'flask',
        'flask-cors',
        'pyjwt',
    ],
)
