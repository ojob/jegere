"""This is the entry point to start the backend of `je gére`.

`app` is a :py:mod:`Flask` instance.

* To launch the app using a production server:

    `$ gunicorn wsgi.py:app --bind 0.0.0.0:5050` or any other port;

* To launch a development server:

    `$ python wsgi.py`

"""
from jegere import bootstrap, config


if __name__ == '__main__':
    # fire a development server
    app = bootstrap.build_app().app
    bootstrap.run(app=app)

else:
    # return a WSGI instance, ready for production
    app = bootstrap.build_app(
        config=config.dev_config,  # FIXME: provide the prod configuration
    ).app
