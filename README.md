# Je gère

This project provides the source code of [jegere.net], a web application
aiming at automatizing the gathering of volunteers to hold events.

> The title of this project means "I'm in charge" in French.

My motivation on this project is to avoid me and others the hassle of
the gathering volunteers to hold an event, i.e. sending a first email, then
sending reminders until the necessary number of volunteers is reached, then
taking care to send reminders just before the event in case someone forgot.

[jegere.net]: http://jegere.net

## Features of the web application

This project provides the following features (in progress!):

-   Users management:

    -   [x] Visitors may create an account, to become Users.
    -   [x] Users may log in and log out, using JWT.
    -   [ ] Users may revoke their account.
    -   [ ] Site admin may select to manually or automatically accept registrations
    -   [ ] Site admin may revoke User accounts.

-   Groups management:

    -   [x] Users may create new Groups, for which they are then Coordinators.
    -   [x] Coordinators may select a Users to become a Member.
    -   [x] Users may quit a Group.
    -   [x] Coordinators may revoke membership.
    -   [x] Coordinators may give Coordinator role to any Member.

-   Events management:

    -   [x] Members may create Events in the Group, define its requirements
            (date, timeslot, minimum number of volunteers needed)

        - [ ] and add some more information (description, location...)

    -   [ ] Members may volunteer for an Event

        -   [ ] then the App sends a calendar invitation with appropriate
                status: active, in preparation, cancelled
        -   [ ] then the App sends a reminder shortly before the Event start

    -   [ ] As long as more volunteers are needed, the Event is marked as such

        -   [ ] then the App sends a reminder to Members regularly

    -   [ ] When no (or not enough) member(s) volunteered at event start,
            the App marks the event as cancelled.

        -   [ ] then the App sends a notice to all Members.

-   Languages:

    -   [x] French, as it is what will suit my initial audience (though the backend
            API is in English)
    -   [ ] Support for translations
    -   [ ] English translation

-   Monitoring:

    -   [x] The website status is available at `/api/status` endpoint, and is
            displayed on front-end (currently as a string)
    -   [ ] Consistent execution logging

-   Deployment:

    -   [x] Deployment using [Docker] (see below).

[docker]: https://www.docker.com
[ojob/jegere]: https://hub.docker.com/ojob/jegere

## Internals

This project is a web application, made of (currently) two sub-projects held
in the same git repo:

-   `backend/` folder provides the back-end server, that holds the data, and take
    care of storing and retrieven it;
-   `frontend/` folder provides the front-end source code, that manages the web
    presentation to the users.

In the future, there may be some sort of smartphone app, using the same
backend server.

The API is described in the `API` document.

### Back-end

The `backend/` folder provides the back-end server, as a [Python] [Flask]
instance. Flask itself can be used to start a development server, even if
a more robust WSGI server (like [Gunicorn]) shall be used for production.

It provides a RESTful API to the front-end, and takes care of handling the
good stuff.

[python]: https://www.python.org/
[flask]: https://flask.palletsprojects.com/
[gunicorn]: https://

### Front-end

The `frontend/` folder provides the front-end source code, as an [Angular]
project.

During testsing, the back-end is configured to serve the front-end build when
requesting `/` (i.e. if the `BACK_SERVE_FRONT` variable is set to `True`).

> For production, it is better to serve these files directly by setting up a web
> server like Apache or nginx.

[angular]: https://angular.io/

## Deploying

### Using Docker (the easy way)

The app is packed as two [Docker] images, which you can retrieve from Docker Hub.

This uses Docker-compose, therefore maybe you need to install it beforehand:
`sudo aptitude install docker-compose`.

Then, it's a simple command: `docker-compose up` when at the root of the
project. This reads in the `docker-compose.yml` file, get the images and
setup the network at once.

Now you should be able to see the webapp at http://localhost/8080.

Note: at this point, the website is not yet reachable from your local network
or even internet. To enable this, you need to tune your computer to forward
the connections (so that Docker can grab them), then maybe also to configure
your internet modem/router to route internet port `80` to your computer
port `8080`.

Enjoy!

### Development Configurations

There are three configurations for the back-end, to allow for different
behaviors regarding the use cases.

In the Docker image, the **development configuration** is currently active.

-   **Production configuration** is intended to be secure, requiring the
    operator (you!) to:

    -   properly define some environment variables: `TOKEN_SECRET_KEY`.

    Note: `HOST` and `PORT` are defined for Docker execution, and currently set
    at `localhost:5050`.
    Additionally, password hashing is resource-consuming, and no CORS is
    allowed.

-   **Development configuration**, intended to be representative but allow for
    efficient development:
    -   backend listens at `http://0.0.0.0:5050`, and CORS is enabled,
    -   secret key is some hard-coded default,
    -   password hashing is alleviated to some low value,
    -   front-end build is served at `/`,
    -   CORS is allowed, so that the front-end development (served by `ng serve`)
        can access the back-end;
-   **Test configuration**, intended for quick automated tests:
    -   backend listens at `http://localhost:5050`, and CORS is enabled,
    -   secret key is some dumb hard-coded default,
    -   password hashing is set at the quickest possible value,
    -   front-end build is served at `/`.

The _development configuration_ is used when the development server is started
with `python -m jegere`.
