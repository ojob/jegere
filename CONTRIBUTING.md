# Helping Je Gère development

Thanks for your interest! Here are some hints to get you started in
contributing to the development of this project.

## What's next to be developed?

See the [board of open issues]: it is used as the development backlog, where
the first column is sorted (tickets with top-priority are at the top), and
the `ToDo` column identifies the next topics to work on.  

[board of open issues]: https://framagit.org/ojob/jegere/-/boards


## Setting up the stuff for development

The back-end and the front-end can be independently started in development,
as both Flask and Angular bring their own development servers that follows
the files changes.

For a global check though, the whole stuff should be packed with Docker
Compose (see below).


### Backend

The tests run with [Pytest] and some plugins, so you need to:

- clone the repo and `cd` into it
- `cd backend`, then:
  - create a virtual Python environment: `python3 -m venv .env`
  - activate it: `source .env/bin/activate`
- install the test dependencies: `pip install -r backend/requirements-dev.txt`
- install the package in editable mode: `pip install -e backend/`
- run the tests: `pytest`

These tests will soon be handled by [Tox], so as to expose different
versions of Python and a correct exposition of the installation.

[Pytest]: https://docs.pytest.org/en/latest/
[Tox]: https://tox.readthedocs.io/en/latest/


### Frontend

This project provides the front-end source code, so once cloned you will be able
to modify the source and build the static javascript files.

Some useful commands, to run in `frontend/` folder

* Install the needed tools for front-end development: 

  - `npm`: `sudo aptitude install npm`
  - Angular stuff: `sudo npm install -g @angular/cli` (The `-g` flag is here to
    install Angular globally, making it availble for other projects.)

* Run `ng serve` to launch a dev server, then navigate to
  `http://localhost:4200/`. The app will automatically reload if
  you change any of the source files.
  
  Note: do not use `ng serve`, as this would circumvent the proxy that is set
  for the front-end to connect to the backend.

* Run `ng generate component component-name` to generate a new component.
  You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

* Building the front-end: `ng build`. The build artifacts will be stored in the
`  frontend/dist/` directory. Use the `--prod` flag for a production build.

* Running unit tests: `ng test`, to execute the unit tests via [Karma].

* Running end-to-end tests: `ng e2e` to execute the end-to-end tests via
  [Protractor].

> These unit and end-to-end tests are not yet setup.

[Karma]: https://karma-runner.github.io
[Protractor]: http://www.protractortest.org/


### Starting the whole stuff using Docker Compose (aka. the simple way)

As for installation, there's a `docker-compose-build.yml` file that allows for
grouped build and run of the project: just type
`docker-compose -f docker-compose-build.yml up -d`.

This triggers local build of the images, that are then started. The `nginx`
logs are written to the _host_ `/var/log/nginx/jegere` folder.

Eventually, if the images are fine, then pushing to Docker Hub is as easy as
follows: `docker-compose push`.


### Using Docker independent images (aka. the hard way)

Internally to the Docker composition, there are two Docker images, linked with
a Docker network.

To start these two images independently, proceed as follows to build and run
these images:

1. Building the Docker images:

  - `cd` to the root of the project (i.e. `backend`, `frontend` and `production`
    folders are located there)

  - build the back-end image (mind the trailing `.`):
    `docker build -t ojob/jegere-backend:test -f production/Dockerfile-backend .`

  - build the front-end image (again, mind the trailing `.`):
    `docker build -t ojob/jegere-frondend:test -f production/Dockerfile-frontend .`
    
2. Running the images: 
    
  - create the docker network (as otherwise, the images cannot see each other):
    `docker network create jegere-network`

  - start the back-end, using `backend` as name as this is then used inside the
    Docker network as url, with network allocation:
     `docker run -d --name backend -it -p 5050:5050 --network jegere-network ojob/jegere-backend:test`.
    
    You may already want to have a look at http://localhost:5050 :-)
       
  - start the front-end image, with the same network:  
    `docker run -d --name frontend -it -p 80:80 --network jegere-network ojob/jegere-frontend:test`

  You may wish to define another port after `-p` if it is already taken.


### Others

* to update the favicon:
  
  - Install something to edit the `.svg` files, like [Inkscape],
  - Do the modifications on `favicon.svg` file, and save it,
  - Install [imagemagick], and use it to convert to an `.ico` file:
    `convert -background none -density 384 favicon.svg -define icon:auto-resize favicon.ico`
    
[Inkscape]: https://inkscape.org
[ImageMagick]: https://www.imagemagick.org/


## Proposing evolutions

Please fork the project, then submit a merge request.

If you really feel so, please send me an email with your motivation, and I may
well accept requests for being a developer member of this project.

